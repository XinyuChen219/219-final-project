/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import coursesitegenerator.GeneratorApp;
import coursesitegenerator.data.CourseSiteData;
import coursesitegenerator.file.CourseSiteFile;
import coursesitegenerator.test_bed.TestSave;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author xinyu
 */
public class LoadingTest {
    
    public LoadingTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    
    @Test
    public void testLoad(){
        GeneratorApp app = new GeneratorApp();
        CourseSiteData courseData = new CourseSiteData();
        CourseSiteFile file = new CourseSiteFile();
        try {
            file.testLoadData(courseData, "C:\\Users\\xinyu\\Desktop\\SiteSaveTest.json");
        } catch (IOException ex) {
            Logger.getLogger(LoadingTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        assertEquals(courseData.getCourseInfoSemester(),"Fall");
        assertEquals(courseData.getCourseInstructorName(), "Richard McKenna");
        assertEquals(((courseData.getHolidays()).get(0)).getDay(), 2);
        assertEquals(((courseData.getHolidays()).get(0)).getMonth(), 3);
        assertEquals(((courseData.getHolidays()).get(0)).getTitle(), "Some Holiday");
        assertEquals(((courseData.getHolidays()).get(0)).getLink(), "some link");
        assertEquals(courseData.getSemester(), "Spring 2017");
        assertEquals(courseData.getSubject(), "CSE");
        assertEquals(((courseData.getProjects()).get(0)).getName(),"atomic comics");
        assertEquals(((courseData.getTeams()).get(0)).getTextColor(), "black");
        assertEquals(((courseData.getStudents()).get(0)).getFirstName(), "Wei Bin");
        assertEquals(courseData.getStartingMondayDay(), 2);
        assertEquals(((courseData.getTeachingAssistants()).get(0)).getName(), "TAName");
        assertEquals(((courseData.getRecitations()).get(0)).getLocation(), "Frey Hall");
        assertEquals((courseData.getRecitations()).size(), 1);
        assertEquals(courseData.getStartHour(), 9);
    }
}
