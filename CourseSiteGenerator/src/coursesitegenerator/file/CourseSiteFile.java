/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coursesitegenerator.file;

import coursesitegenerator.GeneratorApp;
import coursesitegenerator.data.CourseSiteData;
import coursesitegenerator.data.HWs;
import coursesitegenerator.data.Holiday;
import coursesitegenerator.data.Lecture;
import coursesitegenerator.data.Project;
import coursesitegenerator.data.Recitation;
import coursesitegenerator.data.RecitationsSchedule;
import coursesitegenerator.data.Reference;
import coursesitegenerator.data.Student;
import coursesitegenerator.data.TeachingAssistant;
import coursesitegenerator.data.Team;
import coursesitegenerator.workspace.GeneratorWorkspace;
import djf.components.AppDataComponent;
import djf.components.AppFileComponent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.ObservableList;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonWriter;
import javax.json.JsonWriterFactory;
import javax.json.stream.JsonGenerator;
import org.apache.commons.io.FileUtils;

/**
 *
 * @author xinyu
 */
public class CourseSiteFile implements AppFileComponent {

    GeneratorApp app;

    // THESE ARE USED FOR IDENTIFYING JSON TYPES
    static final String JSON_START_HOUR = "startHour";
    static final String JSON_END_HOUR = "endHour";
    static final String JSON_OFFICE_HOURS = "officeHours";
    static final String JSON_DAY = "day";
    static final String JSON_TIME = "time";
    static final String JSON_NAME = "name";
    static final String JSON_UNDERGRAD_TAS = "undergrad_tas";
    static final String JSON_GRAD_TAS = "grad_tas";
    static final String JSON_EMAIL = "email";
    static final String JSON_TADATA = "TA Data";
    static final String JSON_SECTION = "section";
    static final String JSON_DAYTIME = "day_time";
    static final String JSON_LOCATION = "location";
    static final String JSON_TA1 = "ta_1";
    static final String JSON_TA2 = "ta_2";
    static final String JSON_MONTH = "month";
    static final String JSON_TITLE = "title";
    static final String JSON_LINK = "link";
    static final String JSON_TOPIC = "topic";
    static final String JSON_CRITERIA = "criteria";
    static final String JSON_RECITATION = "recitations";
    static final String JSON_WORK = "work";
    static final String JSON_STARTING_MONDAY_MONTH = "startingMondayMonth";
    static final String JSON_STARTING_MONDAY_DAY = "startingMondayDay";
    static final String JSON_ENDING_FRIDAY_MONTH = "endingFridayMonth";
    static final String JSON_ENDING_FRIDAY_DAY = "endingFridayDay";
    static final String JSON_HOLIDAYS = "holidays";
    static final String JSON_LECTURES = "lectures";
    static final String JSON_HWS = "hws";
    static final String JSON_REFERENCES = "references";
    static final String JSON_RECITATION_SCHEDULE = "recitation_schedule";
    static final String JSON_STUDENTS = "students";
    static final String JSON_SEMESTER = "semester";
    static final String JSON_PROJECTS = "projects";
    static final String JSON_RED = "red";
    static final String JSON_GREEN = "green";
    static final String JSON_BLUE = "blue";
    static final String JSON_TEXT_COLOR = "text_color";
    static final String JSON_LAST_NAME = "lastName";
    static final String JSON_FIRST_NAME = "firstName";
    static final String JSON_TEAM = "team";
    static final String JSON_ROLE = "role";
    static final String JSON_TEAMS = "teams";
    static final String JSON_INSTRUCTOR = "instructor";
    static final String JSON_SUBJECT = "subject";
    static final String JSON_NUMBER = "number";
    static final String JSON_YEAR = "year";
    static final String JSON_INSTRUCTOR_NAME = "instructor_name";
    static final String JSON_INSTRUCTOR_HOME = "instructor_home";
    static final String JSON_COURSE_INFO = "course_info";
    static final String JSON_TEAM_LINK = "team link:";
    static final String JSON_DATE = "date";
    static final String JSON_SCHOOL_BANNER = "school_banner";

    public CourseSiteFile(GeneratorApp initApp) {
        app = initApp;
    }

    public CourseSiteFile() {
    }

    @Override
    public void saveData(AppDataComponent data, String filePath) throws IOException {
        CourseSiteData courseData = (CourseSiteData) data;
        JsonArrayBuilder courseInfoBuilder = Json.createArrayBuilder();
        JsonObject course = Json.createObjectBuilder()
                .add(JSON_SUBJECT, courseData.getSubject())
                .add(JSON_SEMESTER, courseData.getCourseInfoSemester())
                .add(JSON_NUMBER, courseData.getNumber())
                .add(JSON_YEAR, courseData.getYear())
                .add(JSON_TITLE, courseData.getCourseTitle())
                .add(JSON_INSTRUCTOR_NAME, courseData.getCourseInstructorName())
                .add(JSON_INSTRUCTOR_HOME, courseData.getCourseInstructorHome())
                .build();

        courseInfoBuilder.add(course);
        JsonArray courseArray = courseInfoBuilder.build();

        //Build the TA json object
        JsonArrayBuilder taArrayBuilder = Json.createArrayBuilder();
        ObservableList<TeachingAssistant> tas = courseData.getUnderGradTa();
        for (TeachingAssistant ta : tas) {
            JsonObject taJson = Json.createObjectBuilder()
                    .add(JSON_NAME, ta.getName())
                    .add(JSON_EMAIL, ta.getEmail()).build();
            taArrayBuilder.add(taJson);
        }
        JsonArray undergradTAsArray = taArrayBuilder.build();

        JsonArrayBuilder gradTaArrayBuilder = Json.createArrayBuilder();
        ObservableList<TeachingAssistant> gradTAs = courseData.getGradTa();
        for (TeachingAssistant ta : gradTAs) {
            JsonObject gradTAJson = Json.createObjectBuilder()
                    .add(JSON_NAME, ta.getName())
                    .add(JSON_EMAIL, ta.getEmail()).build();
            gradTaArrayBuilder.add(gradTAJson);
        }
        JsonArray gradTAsArray = gradTaArrayBuilder.build();

        //Build the time slot
        JsonArrayBuilder timeSlotArrayBuilder = Json.createArrayBuilder();
        ArrayList<TimeSlot> officeHours = TimeSlot.buildOfficeHoursList(courseData);
        for (TimeSlot ts : officeHours) {
            JsonObject tsJson = Json.createObjectBuilder()
                    .add(JSON_DAY, ts.getDay())
                    .add(JSON_TIME, ts.getTime())
                    .add(JSON_NAME, ts.getName()).build();
            timeSlotArrayBuilder.add(tsJson);
        }
        JsonArray timeSlotArray = timeSlotArrayBuilder.build();

        //Build recitations
        JsonArrayBuilder recitationArrayBuilder = Json.createArrayBuilder();
        ObservableList<Recitation> recitations = courseData.getRecitations();
        for (Recitation re : recitations) {
            JsonObject reJson = Json.createObjectBuilder()
                    .add(JSON_SECTION, re.getSection())
                    .add(JSON_INSTRUCTOR, re.getInstructor())
                    .add(JSON_DAYTIME, re.getDayTime())
                    .add(JSON_LOCATION, re.getLocation())
                    .add(JSON_TA1, re.getTa1())
                    .add(JSON_TA2, re.getTa2()).build();
            recitationArrayBuilder.add(reJson);
        }
        JsonArray recitationArray = recitationArrayBuilder.build();
        //HOLIDAY ARRAY
        JsonArrayBuilder holidaysArrayBuilder = Json.createArrayBuilder();
        ObservableList<Holiday> holidays = courseData.getHolidays();
        for (Holiday ho : holidays) {
            JsonObject hoJson = Json.createObjectBuilder()
                    .add(JSON_MONTH, ho.getMonth())
                    .add(JSON_DAY, ho.getDay())
                    .add(JSON_DATE, ho.getDate())
                    .add(JSON_TITLE, ho.getTitle())
                    .add(JSON_LINK, ho.getLink()).build();
            holidaysArrayBuilder.add(hoJson);
        }
        JsonArray holidaysArray = holidaysArrayBuilder.build();

        //Lectures
        JsonArrayBuilder lecturesArrayBuilder = Json.createArrayBuilder();
        ObservableList<Lecture> lectures = courseData.getLectures();
        for (Lecture le : lectures) {
            JsonObject leJson = Json.createObjectBuilder()
                    .add(JSON_MONTH, le.getMonth())
                    .add(JSON_DAY, le.getDay())
                    .add(JSON_DATE, le.getDate())
                    .add(JSON_TITLE, le.getTitle())
                    .add(JSON_TOPIC, le.getTopic())
                    .add(JSON_LINK, le.getLink()).build();
            lecturesArrayBuilder.add(leJson);
        }
        JsonArray lectureArray = lecturesArrayBuilder.build();

        //references
        JsonArrayBuilder referenceArrayBuilder = Json.createArrayBuilder();
        ObservableList<Reference> references = courseData.getReferences();
        for (Reference re : references) {
            JsonObject reJson = Json.createObjectBuilder()
                    .add(JSON_MONTH, re.getMonth())
                    .add(JSON_DAY, re.getDay())
                    .add(JSON_DATE, re.getDate())
                    .add(JSON_TITLE, re.getTitle())
                    .add(JSON_TOPIC, re.getTopic())
                    .add(JSON_LINK, re.getLink()).build();
            referenceArrayBuilder.add(reJson);
        }
        JsonArray referenceArray = referenceArrayBuilder.build();
        //RecitationSchedule
        JsonArrayBuilder recitationScheduleArrayBuilder = Json.createArrayBuilder();
        ObservableList<RecitationsSchedule> reciSche = courseData.getRecitationsSchedule();
        for (RecitationsSchedule rs : reciSche) {
            JsonObject rsJson = Json.createObjectBuilder()
                    .add(JSON_MONTH, rs.getMonth())
                    .add(JSON_DAY, rs.getDay())
                    .add(JSON_DATE, rs.getDate())
                    .add(JSON_TITLE, rs.getTitle())
                    .add(JSON_TOPIC, rs.getTopic()).build();
            recitationScheduleArrayBuilder.add(rsJson);
        }
        JsonArray recitationScheduleArray = recitationScheduleArrayBuilder.build();
        //HWs
        JsonArrayBuilder hwsArrayBuilder = Json.createArrayBuilder();
        ObservableList<HWs> hws = courseData.getHws();
        for (HWs hw : hws) {
            JsonObject hwJson = Json.createObjectBuilder()
                    .add(JSON_MONTH, hw.getMonth())
                    .add(JSON_DAY, hw.getDay())
                    .add(JSON_DATE, hw.getDate())
                    .add(JSON_TITLE, hw.getTitle())
                    .add(JSON_TOPIC, hw.getTopic())
                    .add(JSON_LINK, hw.getLink())
                    .add(JSON_TIME, hw.getTime())
                    .add(JSON_CRITERIA, hw.getCriteria()).build();
            hwsArrayBuilder.add(hwJson);
        }
        JsonArray hwsArray = hwsArrayBuilder.build();
        //works
        JsonArrayBuilder workArrayBuilder = Json.createArrayBuilder();
        ObservableList<Project> projects = courseData.getProjects();
        for (Project project : projects) {
            String[] students = project.getStudents();
            JsonArrayBuilder studentsArrayBuilder = Json.createArrayBuilder();
            studentsArrayBuilder.add(students[0]);
            studentsArrayBuilder.add(students[1]);
            studentsArrayBuilder.add(students[2]);
            studentsArrayBuilder.add(students[3]);
            JsonObject proJson = Json.createObjectBuilder()
                    .add(JSON_NAME, project.getName())
                    .add(JSON_STUDENTS, studentsArrayBuilder)
                    .add(JSON_LINK, project.getLink()).build();
            workArrayBuilder.add(proJson);
        }
        JsonArrayBuilder projectArrayBuilder = Json.createArrayBuilder();
        JsonObject woJson = Json.createObjectBuilder()
                .add(JSON_SEMESTER, courseData.getSemester())
                .add(JSON_PROJECTS, workArrayBuilder).build();
        projectArrayBuilder.add(woJson);
        JsonArray workArray = projectArrayBuilder.build();

        //teams
        JsonArrayBuilder teamsArrayBuilder = Json.createArrayBuilder();
        ObservableList<Team> teams = courseData.getTeams();
        for (Team team : teams) {
            JsonObject teamJson = Json.createObjectBuilder()
                    .add(JSON_NAME, team.getName())
                    .add(JSON_RED, team.getRed())
                    .add(JSON_GREEN, team.getGreen())
                    .add(JSON_BLUE, team.getBlue())
                    .add(JSON_TEXT_COLOR, team.getTextColor())
                    .add(JSON_TEAM_LINK, team.getLink())
                    .build();
            teamsArrayBuilder.add(teamJson);
        }
        JsonArray teamsArray = teamsArrayBuilder.build();

        //students
        JsonArrayBuilder studentsArrayBuilder = Json.createArrayBuilder();
        ObservableList<Student> students = courseData.getStudents();
        for (Student student : students) {
            JsonObject stuJson = Json.createObjectBuilder()
                    .add(JSON_LAST_NAME, student.getLastName())
                    .add(JSON_FIRST_NAME, student.getFirstName())
                    .add(JSON_TEAM, student.getTeam())
                    .add(JSON_ROLE, student.getRole()).build();
            studentsArrayBuilder.add(stuJson);
        }
        JsonArray studentsArray = studentsArrayBuilder.build();

        JsonObject dataJSO = Json.createObjectBuilder()
                .add(JSON_COURSE_INFO, courseArray)
                .add(JSON_START_HOUR, "" + courseData.getStartHour())
                .add(JSON_END_HOUR, "" + courseData.getEndHour())
                .add(JSON_UNDERGRAD_TAS, undergradTAsArray)
                .add(JSON_GRAD_TAS, gradTAsArray)
                .add(JSON_OFFICE_HOURS, timeSlotArray)
                .add(JSON_RECITATION, recitationArray)
                .add(JSON_STARTING_MONDAY_MONTH, "" + courseData.getStartingMondayMonth())
                .add(JSON_STARTING_MONDAY_DAY, "" + courseData.getStartingMondayDay())
                .add(JSON_ENDING_FRIDAY_MONTH, "" + courseData.getEndingFridayMonth())
                .add(JSON_ENDING_FRIDAY_DAY, "" + courseData.getEndingFridayDay())
                .add(JSON_HOLIDAYS, holidaysArray)
                .add(JSON_LECTURES, lectureArray)
                .add(JSON_REFERENCES, referenceArray)
                .add(JSON_RECITATION_SCHEDULE, recitationScheduleArray)
                .add(JSON_HWS, hwsArray)
                .add(JSON_WORK, workArray)
                .add(JSON_TEAMS, teamsArray)
                .add(JSON_STUDENTS, studentsArray)
                .build();

        Map<String, Object> properties = new HashMap<>(1);
        properties.put(JsonGenerator.PRETTY_PRINTING, true);
        JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
        StringWriter sw = new StringWriter();
        JsonWriter jsonWriter = writerFactory.createWriter(sw);
        jsonWriter.writeObject(dataJSO);
        jsonWriter.close();

        OutputStream os = new FileOutputStream(filePath + ".json");
        JsonWriter jsonFileWriter = Json.createWriter(os);
        jsonFileWriter.writeObject(dataJSO);
        String prettyPrinted = sw.toString();
        PrintWriter pw = new PrintWriter(filePath + ".json");
        pw.write(prettyPrinted);
        pw.close();

    }

    @Override
    public void loadData(AppDataComponent data, String filePath) throws IOException {
        CourseSiteData dataManager = (CourseSiteData) data;

        JsonObject json = loadJSONFile(filePath);

        String startHour = json.getString(JSON_START_HOUR);
        String endHour = json.getString(JSON_END_HOUR);
        dataManager.initHours(startHour, endHour);

        app.getWorkspaceComponent().reloadWorkspace(app.getDataComponent());

        JsonArray courseInfoArray = json.getJsonArray(JSON_COURSE_INFO);
        JsonObject courseInfo = courseInfoArray.getJsonObject(0);
        String subject = courseInfo.getString(JSON_SUBJECT);
        String sem = courseInfo.getString(JSON_SEMESTER);
        String number = courseInfo.getString(JSON_NUMBER);
        String year = courseInfo.getString(JSON_YEAR);
        String courseT = courseInfo.getString(JSON_TITLE);
        String courseIH = courseInfo.getString(JSON_INSTRUCTOR_HOME);
        String courseIN = courseInfo.getString(JSON_INSTRUCTOR_NAME);
        dataManager.setSubject(subject);
        dataManager.setCourseInfoSemester(sem);
        dataManager.setSemester(sem);
        dataManager.setCourseTitle(courseT);
        dataManager.setNumber(number);
        dataManager.setYear(year);
        dataManager.setCourseInstructorHome(courseIH);
        dataManager.setCourseInstructorName(courseIN);

        JsonArray jsonTAArray = json.getJsonArray(JSON_UNDERGRAD_TAS);
        for (int i = 0; i < jsonTAArray.size(); i++) {
            JsonObject jsonTA = jsonTAArray.getJsonObject(i);
            String name = jsonTA.getString(JSON_NAME);
            String email = jsonTA.getString(JSON_EMAIL);
            dataManager.addTA(name, email, true);
        }
        JsonArray jsonGradTAArray = json.getJsonArray(JSON_GRAD_TAS);
        for (int i = 0; i < jsonGradTAArray.size(); i++) {
            JsonObject jsonTA = jsonGradTAArray.getJsonObject(i);
            String name = jsonTA.getString(JSON_NAME);
            String email = jsonTA.getString(JSON_EMAIL);
            dataManager.addTA(name, email, false);
        }
        JsonArray jsonOfficeHoursArray = json.getJsonArray(JSON_OFFICE_HOURS);
        for (int i = 0; i < jsonOfficeHoursArray.size(); i++) {
            JsonObject jsonOfficeHours = jsonOfficeHoursArray.getJsonObject(i);
            String day = jsonOfficeHours.getString(JSON_DAY);
            String time = jsonOfficeHours.getString(JSON_TIME);
            String name = jsonOfficeHours.getString(JSON_NAME);
            dataManager.addOfficeHoursReservation(day, time, name);
        }

        JsonArray recitationArray = json.getJsonArray(JSON_RECITATION);
        for (int i = 0; i < recitationArray.size(); i++) {
            JsonObject jsonRecitation = recitationArray.getJsonObject(i);
            String section = jsonRecitation.getString(JSON_SECTION);
            String dayTime = jsonRecitation.getString(JSON_DAYTIME);
            String location = jsonRecitation.getString(JSON_LOCATION);
            String instructor = jsonRecitation.getString(JSON_INSTRUCTOR);
            String ta1 = jsonRecitation.getString(JSON_TA1);
            String ta2 = jsonRecitation.getString(JSON_TA2);
            dataManager.addRecitation(section, instructor, dayTime, location, ta1, ta2);
        }

        String startingMondayMonth = json.getString(JSON_STARTING_MONDAY_MONTH);
        String startingMondayDay = json.getString(JSON_STARTING_MONDAY_DAY);
        String endingFridayMonth = json.getString(JSON_ENDING_FRIDAY_MONTH);
        String endingFridayDay = json.getString(JSON_ENDING_FRIDAY_DAY);
        dataManager.setStartingMondayMonth(Integer.parseInt(startingMondayMonth));
        dataManager.setStartingMondayDay(Integer.parseInt(startingMondayDay));
        dataManager.setEndingFridayDay(Integer.parseInt(endingFridayDay));
        dataManager.setEndingFridayMonth(Integer.parseInt(endingFridayMonth));

        JsonArray holidaysArray = json.getJsonArray(JSON_HOLIDAYS);
        for (int i = 0; i < holidaysArray.size(); i++) {
            JsonObject jsonHolidays = holidaysArray.getJsonObject(i);
            String date = jsonHolidays.getString(JSON_DATE);
            String title = jsonHolidays.getString(JSON_TITLE);
            String link = jsonHolidays.getString(JSON_LINK);
            dataManager.addHoliday(date, title, link);
        }

        JsonArray lectureArray = json.getJsonArray(JSON_LECTURES);
        for (int i = 0; i < lectureArray.size(); i++) {
            JsonObject jsonLecture = lectureArray.getJsonObject(i);
            String date = jsonLecture.getString(JSON_DATE);
            String title = jsonLecture.getString(JSON_TITLE);
            String topic = jsonLecture.getString(JSON_TOPIC);
            String link = jsonLecture.getString(JSON_LINK);
            dataManager.addLecture(date, title, topic, link);
        }

        JsonArray referenceArray = json.getJsonArray(JSON_REFERENCES);
        for (int i = 0; i < referenceArray.size(); i++) {
            JsonObject jsonReference = referenceArray.getJsonObject(i);
            String date = jsonReference.getString(JSON_DATE);
            String title = jsonReference.getString(JSON_TITLE);
            String topic = jsonReference.getString(JSON_TOPIC);
            String link = jsonReference.getString(JSON_LINK);
            dataManager.addReference(date, title, topic, link);
        }

        JsonArray recitationScheduleArray = json.getJsonArray(JSON_RECITATION_SCHEDULE);
        for (int i = 0; i < recitationScheduleArray.size(); i++) {
            JsonObject jsonRecSch = recitationScheduleArray.getJsonObject(i);
            String date = jsonRecSch.getString(JSON_DATE);
            String title = jsonRecSch.getString(JSON_TITLE);
            String topic = jsonRecSch.getString(JSON_TOPIC);
            dataManager.addRecitationSchedule(date, title, topic);
        }

        JsonArray hwsArray = json.getJsonArray(JSON_HWS);
        for (int i = 0; i < hwsArray.size(); i++) {
            JsonObject jsonHw = hwsArray.getJsonObject(i);
            String date = jsonHw.getString(JSON_DATE);
            String title = jsonHw.getString(JSON_TITLE);
            String time = jsonHw.getString(JSON_TIME);
            String topic = jsonHw.getString(JSON_TOPIC);
            String link = jsonHw.getString(JSON_LINK);
            String criteria = jsonHw.getString(JSON_CRITERIA);
            dataManager.addHW(date, title, topic, link, time, criteria);
        }

        JsonArray workArray = json.getJsonArray(JSON_WORK);
        JsonObject jsonWork = workArray.getJsonObject(0);
        JsonArray projectArray = jsonWork.getJsonArray(JSON_PROJECTS);
        for (int i = 0; i < projectArray.size(); i++) {
            JsonObject jsonProject = projectArray.getJsonObject(i);
            JsonArray jsonStu = jsonProject.getJsonArray(JSON_STUDENTS);
            String[] students = new String[4];
            for (int j = 0; j < jsonStu.size(); j++) {
                students[j] = jsonStu.getString(j);
            }
            String name = jsonProject.getString(JSON_NAME);
            String link = jsonProject.getString(JSON_LINK);
            dataManager.addProject(name, students, link);
        }

        JsonArray teamsArray = json.getJsonArray(JSON_TEAMS);
        for (int i = 0; i < teamsArray.size(); i++) {
            JsonObject jsonTeam = teamsArray.getJsonObject(i);
            String name = jsonTeam.getString(JSON_NAME);
            int red = jsonTeam.getInt(JSON_RED);
            int green = jsonTeam.getInt(JSON_GREEN);
            int blue = jsonTeam.getInt(JSON_BLUE);
            String textColor = jsonTeam.getString(JSON_TEXT_COLOR);
            String link = jsonTeam.getString(JSON_TEAM_LINK);
            dataManager.addTeam(name, red, green, blue, textColor, link);
        }

        JsonArray studentsArray = json.getJsonArray(JSON_STUDENTS);
        for (int i = 0; i < studentsArray.size(); i++) {
            JsonObject jsonStudent = studentsArray.getJsonObject(i);
            String lastName = jsonStudent.getString(JSON_LAST_NAME);
            String firstName = jsonStudent.getString(JSON_FIRST_NAME);
            String team = jsonStudent.getString(JSON_TEAM);
            String role = jsonStudent.getString(JSON_ROLE);
            dataManager.addStudent(lastName, firstName, team, role);
        }

    }

    @Override
    public void exportData(AppDataComponent dataComponent, String filePath) throws IOException {
        CourseSiteData data = (CourseSiteData) dataComponent;
        GeneratorWorkspace workspace = (GeneratorWorkspace) app.getWorkspaceComponent();

        File file = new File(workspace.getTemplateDirectory());
        File exFile = new File(workspace.getExportDirectory());
        FileUtils.copyDirectory(file, exFile);
    }

    @Override
    public void importData(AppDataComponent data, String filePath) throws IOException {

    }

    @Override
    public void saveInOriginalData(AppDataComponent dataComponent, String path) {
        OutputStream os = null;
        try {
            CourseSiteData courseData = (CourseSiteData) dataComponent;
            JsonArrayBuilder courseInfoBuilder = Json.createArrayBuilder();
            JsonObject course = Json.createObjectBuilder()
                    .add(JSON_SUBJECT, courseData.getSubject())
                    .add(JSON_SEMESTER, courseData.getCourseInfoSemester())
                    .add(JSON_NUMBER, courseData.getNumber())
                    .add(JSON_YEAR, courseData.getYear())
                    .add(JSON_TITLE, courseData.getCourseTitle())
                    .add(JSON_INSTRUCTOR_NAME, courseData.getCourseInstructorName())
                    .add(JSON_INSTRUCTOR_HOME, courseData.getCourseInstructorHome())
                    .build();
            courseInfoBuilder.add(course);
            JsonArray courseArray = courseInfoBuilder.build();

            //Build the TA json object
            JsonArrayBuilder taArrayBuilder = Json.createArrayBuilder();
            ObservableList<TeachingAssistant> tas = courseData.getUnderGradTa();
            for (TeachingAssistant ta : tas) {
                JsonObject taJson = Json.createObjectBuilder()
                        .add(JSON_NAME, ta.getName())
                        .add(JSON_EMAIL, ta.getEmail()).build();
                taArrayBuilder.add(taJson);
            }
            JsonArray undergradTAsArray = taArrayBuilder.build();

            JsonArrayBuilder gradTaArrayBuilder = Json.createArrayBuilder();
            ObservableList<TeachingAssistant> gradTAs = courseData.getGradTa();
            for (TeachingAssistant ta : gradTAs) {
                JsonObject gradTAJson = Json.createObjectBuilder()
                        .add(JSON_NAME, ta.getName())
                        .add(JSON_EMAIL, ta.getEmail()).build();
                gradTaArrayBuilder.add(gradTAJson);
            }
            JsonArray gradTAsArray = gradTaArrayBuilder.build();

            //Build the time slot
            JsonArrayBuilder timeSlotArrayBuilder = Json.createArrayBuilder();
            ArrayList<TimeSlot> officeHours = TimeSlot.buildOfficeHoursList(courseData);
            for (TimeSlot ts : officeHours) {
                JsonObject tsJson = Json.createObjectBuilder()
                        .add(JSON_DAY, ts.getDay())
                        .add(JSON_TIME, ts.getTime())
                        .add(JSON_NAME, ts.getName()).build();
                timeSlotArrayBuilder.add(tsJson);
            }
            JsonArray timeSlotArray = timeSlotArrayBuilder.build();

            //Build recitations
            JsonArrayBuilder recitationArrayBuilder = Json.createArrayBuilder();
            ObservableList<Recitation> recitations = courseData.getRecitations();
            for (Recitation re : recitations) {
                JsonObject reJson = Json.createObjectBuilder()
                        .add(JSON_SECTION, re.getSection())
                        .add(JSON_INSTRUCTOR, re.getInstructor())
                        .add(JSON_DAYTIME, re.getDayTime())
                        .add(JSON_LOCATION, re.getLocation())
                        .add(JSON_TA1, re.getTa1())
                        .add(JSON_TA2, re.getTa2()).build();
                recitationArrayBuilder.add(reJson);
            }
            JsonArray recitationArray = recitationArrayBuilder.build();
            //HOLIDAY ARRAY
            JsonArrayBuilder holidaysArrayBuilder = Json.createArrayBuilder();
            ObservableList<Holiday> holidays = courseData.getHolidays();
            for (Holiday ho : holidays) {
                JsonObject hoJson = Json.createObjectBuilder()
                        .add(JSON_MONTH, ho.getMonth())
                        .add(JSON_DAY, ho.getDay())
                        .add(JSON_DATE, ho.getDate())
                        .add(JSON_TITLE, ho.getTitle())
                        .add(JSON_LINK, ho.getLink()).build();
                holidaysArrayBuilder.add(hoJson);
            }
            JsonArray holidaysArray = holidaysArrayBuilder.build();

            //Lectures
            JsonArrayBuilder lecturesArrayBuilder = Json.createArrayBuilder();
            ObservableList<Lecture> lectures = courseData.getLectures();
            for (Lecture le : lectures) {
                JsonObject leJson = Json.createObjectBuilder()
                        .add(JSON_MONTH, le.getMonth())
                        .add(JSON_DAY, le.getDay())
                        .add(JSON_DATE, le.getDate())
                        .add(JSON_TITLE, le.getTitle())
                        .add(JSON_TOPIC, le.getTopic())
                        .add(JSON_LINK, le.getLink()).build();
                lecturesArrayBuilder.add(leJson);
            }
            JsonArray lectureArray = lecturesArrayBuilder.build();

            //references
            JsonArrayBuilder referenceArrayBuilder = Json.createArrayBuilder();
            ObservableList<Reference> references = courseData.getReferences();
            for (Reference re : references) {
                JsonObject reJson = Json.createObjectBuilder()
                        .add(JSON_MONTH, re.getMonth())
                        .add(JSON_DAY, re.getDay())
                        .add(JSON_DATE, re.getDate())
                        .add(JSON_TITLE, re.getTitle())
                        .add(JSON_TOPIC, re.getTopic())
                        .add(JSON_LINK, re.getLink()).build();
                referenceArrayBuilder.add(reJson);
            }
            JsonArray referenceArray = referenceArrayBuilder.build();
            //RecitationSchedule
            JsonArrayBuilder recitationScheduleArrayBuilder = Json.createArrayBuilder();
            ObservableList<RecitationsSchedule> reciSche = courseData.getRecitationsSchedule();
            for (RecitationsSchedule rs : reciSche) {
                JsonObject rsJson = Json.createObjectBuilder()
                        .add(JSON_MONTH, rs.getMonth())
                        .add(JSON_DAY, rs.getDay())
                        .add(JSON_DATE, rs.getDate())
                        .add(JSON_TITLE, rs.getTitle())
                        .add(JSON_TOPIC, rs.getTopic()).build();
                recitationScheduleArrayBuilder.add(rsJson);
            }
            JsonArray recitationScheduleArray = recitationScheduleArrayBuilder.build();
            //HWs
            JsonArrayBuilder hwsArrayBuilder = Json.createArrayBuilder();
            ObservableList<HWs> hws = courseData.getHws();
            for (HWs hw : hws) {
                JsonObject hwJson = Json.createObjectBuilder()
                        .add(JSON_MONTH, hw.getMonth())
                        .add(JSON_DAY, hw.getDay())
                        .add(JSON_DATE, hw.getDate())
                        .add(JSON_TITLE, hw.getTitle())
                        .add(JSON_TOPIC, hw.getTopic())
                        .add(JSON_LINK, hw.getLink())
                        .add(JSON_TIME, hw.getTime())
                        .add(JSON_CRITERIA, hw.getCriteria()).build();
                hwsArrayBuilder.add(hwJson);
            }
            JsonArray hwsArray = hwsArrayBuilder.build();
            //works
            JsonArrayBuilder workArrayBuilder = Json.createArrayBuilder();
            ObservableList<Project> projects = courseData.getProjects();
            for (Project project : projects) {
                String[] students = project.getStudents();
                JsonArrayBuilder studentsArrayBuilder = Json.createArrayBuilder();
                studentsArrayBuilder.add(students[0]);
                studentsArrayBuilder.add(students[1]);
                studentsArrayBuilder.add(students[2]);
                studentsArrayBuilder.add(students[3]);
                JsonObject proJson = Json.createObjectBuilder()
                        .add(JSON_NAME, project.getName())
                        .add(JSON_STUDENTS, studentsArrayBuilder)
                        .add(JSON_LINK, project.getLink()).build();
                workArrayBuilder.add(proJson);
            }
            JsonArrayBuilder projectArrayBuilder = Json.createArrayBuilder();
            JsonObject woJson = Json.createObjectBuilder()
                    .add(JSON_SEMESTER, courseData.getSemester())
                    .add(JSON_PROJECTS, workArrayBuilder).build();
            projectArrayBuilder.add(woJson);
            JsonArray workArray = projectArrayBuilder.build();

            //teams
            JsonArrayBuilder teamsArrayBuilder = Json.createArrayBuilder();
            ObservableList<Team> teams = courseData.getTeams();
            for (Team team : teams) {
                JsonObject teamJson = Json.createObjectBuilder()
                        .add(JSON_NAME, team.getName())
                        .add(JSON_RED, team.getRed())
                        .add(JSON_GREEN, team.getGreen())
                        .add(JSON_BLUE, team.getBlue())
                        .add(JSON_TEXT_COLOR, team.getTextColor())
                        .add(JSON_TEAM_LINK, team.getLink())
                        .build();
                teamsArrayBuilder.add(teamJson);
            }
            JsonArray teamsArray = teamsArrayBuilder.build();

            //students
            JsonArrayBuilder studentsArrayBuilder = Json.createArrayBuilder();
            ObservableList<Student> students = courseData.getStudents();
            for (Student student : students) {
                JsonObject stuJson = Json.createObjectBuilder()
                        .add(JSON_LAST_NAME, student.getLastName())
                        .add(JSON_FIRST_NAME, student.getFirstName())
                        .add(JSON_TEAM, student.getTeam())
                        .add(JSON_ROLE, student.getRole()).build();
                studentsArrayBuilder.add(stuJson);
            }
            JsonArray studentsArray = studentsArrayBuilder.build();

            JsonObject dataJSO = Json.createObjectBuilder()
                    .add(JSON_COURSE_INFO, courseArray)
                    .add(JSON_START_HOUR, "" + courseData.getStartHour())
                    .add(JSON_END_HOUR, "" + courseData.getEndHour())
                    .add(JSON_UNDERGRAD_TAS, undergradTAsArray)
                    .add(JSON_GRAD_TAS, gradTAsArray)
                    .add(JSON_OFFICE_HOURS, timeSlotArray)
                    .add(JSON_RECITATION, recitationArray)
                    .add(JSON_STARTING_MONDAY_MONTH, "" + courseData.getStartingMondayMonth())
                    .add(JSON_STARTING_MONDAY_DAY, "" + courseData.getStartingMondayDay())
                    .add(JSON_ENDING_FRIDAY_MONTH, "" + courseData.getEndingFridayMonth())
                    .add(JSON_ENDING_FRIDAY_DAY, "" + courseData.getEndingFridayDay())
                    .add(JSON_HOLIDAYS, holidaysArray)
                    .add(JSON_LECTURES, lectureArray)
                    .add(JSON_REFERENCES, referenceArray)
                    .add(JSON_RECITATION_SCHEDULE, recitationScheduleArray)
                    .add(JSON_HWS, hwsArray)
                    .add(JSON_WORK, workArray)
                    .add(JSON_TEAMS, teamsArray)
                    .add(JSON_STUDENTS, studentsArray)
                    .build();

            Map<String, Object> properties = new HashMap<>(1);
            properties.put(JsonGenerator.PRETTY_PRINTING, true);
            JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
            StringWriter sw = new StringWriter();
            JsonWriter jsonWriter = writerFactory.createWriter(sw);
            jsonWriter.writeObject(dataJSO);
            jsonWriter.close();

            os = new FileOutputStream(path);
            JsonWriter jsonFileWriter = Json.createWriter(os);
            jsonFileWriter.writeObject(dataJSO);
            String prettyPrinted = sw.toString();
            PrintWriter pw = new PrintWriter(path);
            pw.write(prettyPrinted);
            pw.close();

        } catch (FileNotFoundException ex) {
            Logger.getLogger(CourseSiteFile.class.getName()).log(Level.SEVERE, null, ex);

        } finally {
            try {
                os.close();
            } catch (IOException ex) {
                Logger.getLogger(CourseSiteFile.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    private JsonObject loadJSONFile(String jsonFilePath) throws FileNotFoundException, IOException {
        InputStream is = new FileInputStream(jsonFilePath);
        JsonReader jsonReader = Json.createReader(is);
        JsonObject json = jsonReader.readObject();
        jsonReader.close();
        is.close();
        return json;
    }
}
