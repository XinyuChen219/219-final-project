/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coursesitegenerator.data;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author xinyu
 */
public class Recitation <E extends Comparable<E>> implements Comparable<E> {
    private final StringProperty section;
    private final StringProperty instructor;
    private final StringProperty dayTime;
    private final StringProperty location;
    private final StringProperty ta1;
    private final StringProperty ta2;
    
    
    public Recitation(String section, String instructor, String dayTime, String location, String ta1, String ta2){
        this.section = new SimpleStringProperty(section);
        this.instructor = new SimpleStringProperty(instructor);
        this.dayTime = new SimpleStringProperty(instructor);
        this.location = new SimpleStringProperty(location);
        this.ta1 = new SimpleStringProperty(ta1);
        this.ta2 = new SimpleStringProperty(ta2);
    }

    public String getSection() {
        return section.get();
    }

    public String getInstructor() {
        return instructor.get();
    }

    public String getDayTime() {
        return dayTime.get();
    }

    public String getLocation() {
        return location.get();
    }

    public String getTa1() {
        return ta1.get();
    }
    
    public void setTA1(String newTA1){
        ta1.set(newTA1);
    }
    public void setTA2(String newTA2){
        ta2.set(newTA2);
    }

    public String getTa2() {
        return ta2.get();
    }
    
    
    @Override
    public int compareTo(E o) {
        return getSection().compareTo(((Recitation) o).getSection());
    }
    
    @Override
    public String toString(){
        return section.getValue();
    }
    
}
