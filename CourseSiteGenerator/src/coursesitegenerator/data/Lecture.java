/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coursesitegenerator.data;

/**
 *
 * @author xinyu
 */
public class Lecture extends ScheduleItem{
    private String link;
    /**public Lecture(int month, int day, String title, String topic, String link){
        this.month = month;
        this.day = day;
        this.title = title;
        this.link = link;
        this.topic = topic;
    }**/

    public Lecture(String type, String date, String title, String topic) {
        super(type, date, title, topic);
    }
    
    public void setLink(String link){
        this.link = link;
    }
    
    public String getLink(){
        return link;
    }
    
}
