/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coursesitegenerator.data;

/**
 *
 * @author xinyu
 */
public class HWs extends ScheduleItem{
    private String link;
    private String criteria;
    private String time;

    public HWs(String type, String date, String title, String topic) {
        super(type, date, title, topic);
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
    
    
    public void setLink(String link){
        this.link = link;
    }
    
    public String getLink(){
        return link;
    }
    
    public void setCriteria(String criteria){
        this.criteria = criteria;
    }
    public String getCriteria(){
        return criteria;
    }
    
}
