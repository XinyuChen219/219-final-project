/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coursesitegenerator.data;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author xinyu
 */
public class ScheduleItem <E extends Comparable<E>> implements Comparable<E>{
    private final StringProperty type;
    private final StringProperty date;
    private final StringProperty title;
    private final StringProperty topic;
    
    public ScheduleItem(String type, String date, String title, String topic){
        this.type = new SimpleStringProperty(type);
        this.title = new SimpleStringProperty(title);
        this.date = new SimpleStringProperty(date);
        this.topic = new SimpleStringProperty(topic);
        
    }
    
    public int getDay(){
        return Integer.parseInt((date.get()).substring(8));
    }
    public int getMonth(){
        return Integer.parseInt((date.get()).substring(5, 7));
    }
    
    public String getType(){
        return type.get();
    }
    public void setType(String Ntype){
        type.set(Ntype);
    }
    public String getDate(){
        return date.get();
    }
    public void setDate(String newDate){
        date.set(newDate);
    }
    public String getTitle(){
        return title.get();
    }
    public void setTitle(String newTitle){
        title.set(newTitle);
    }
    public String getTopic(){
        return topic.get();
    }
    public void setTopic(String newTopic){
        topic.set(newTopic);
    }

    @Override
    public int compareTo(E o) {
        return getDate().compareTo(((ScheduleItem) o).getDate());
    }
}
