/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coursesitegenerator.data;

/**
 *
 * @author xinyu
 */
public class Student {
    private String lastName;
    private String firstName;
    private String team;
    private String role;
    
    public Student(String lastName, String firstName, String team, String role){
        this.lastName = lastName;
        this.firstName = firstName;
        this.team = team;
        this.role = role;
    }

    public String getLastName() {
        return lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getTeam() {
        return team;
    }
    public void setTeam(String newTeam){
        team = newTeam;
    }

    public String getRole() {
        return role;
    }
    
   
}
