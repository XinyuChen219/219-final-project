/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coursesitegenerator.data;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.scene.paint.Color;

/**
 *
 * @author xinyu
 */
public class Team {
    private StringProperty name;
    private StringProperty link;
    private StringProperty color;
    private StringProperty textColor;
    private int red;
    private int green;
    private int blue;
    Color colorC;

    
    public Team(String name, String color, String textColor, String link){
        this.name = new SimpleStringProperty(name);
        this.color = new SimpleStringProperty(color);
        this.textColor = new SimpleStringProperty(textColor);
        this.link = new SimpleStringProperty(link);
        colorC = Color.web(color);
        red = (int)(colorC.getRed() * 255);
        green = (int) (colorC.getGreen() * 255);
        blue = (int) (colorC.getBlue() * 255);
    }

    public int getRed() {
        return red;
    }

    public void setRed(int red) {
        this.red = red;
    }

    public int getGreen() {
        return green;
    }

    public void setGreen(int green) {
        this.green = green;
    }

    public int getBlue() {
        return blue;
    }

    public void setBlue(int blue) {
        this.blue = blue;
    }

    
    
    public String getLink() {
        return link.get();
    }

    public void setLink(String newLink){
        link.set(newLink);
    }
    public String getName() {
        return name.get();
    }
    
    public void setName(String newName){
        name.set(newName);
    }
    public String getColor(){
        return color.get();
    }
    public void setColor(String newColor){
        color.set(newColor);
    }

    public String getTextColor() {
        return textColor.get();
    }
    public void setTextColor(String newTextColor){
        textColor.set(newTextColor);
    }
}
