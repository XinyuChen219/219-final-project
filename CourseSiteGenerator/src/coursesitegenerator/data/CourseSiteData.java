/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coursesitegenerator.data;

import coursesitegenerator.GeneratorApp;
import coursesitegenerator.GeneratorProp;
import static coursesitegenerator.data.TAData.MAX_END_HOUR;
import static coursesitegenerator.data.TAData.MIN_START_HOUR;
import coursesitegenerator.workspace.GeneratorWorkspace;
import djf.components.AppDataComponent;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.paint.Color;
import properties_manager.PropertiesManager;

/**
 *
 * @author xinyu
 */
public class CourseSiteData implements AppDataComponent {

    //Data for TA data
    GeneratorApp app;
    ObservableList<TeachingAssistant> teachingAssistants;
    ArrayList<String> gridHeaders;
    HashMap<String, StringProperty> officeHours;
    int startHour;
    int endHour;
    //For Recitation data
    ObservableList<Recitation> recitations;
    //For schedule data
    int startingMondayMonth;
    int startingMondayDay;
    int endingFridayMonth;
    int endingFridayDay;
    ObservableList<ScheduleItem> scheduleItems;
    ObservableList<HWs> hws;
    ObservableList<Holiday> holidays;
    ObservableList<Lecture> lectures;
    ObservableList<RecitationsSchedule> recitationsSchedules;
    ObservableList<Reference> references;
    //For work
    String semester;
    ObservableList<Project> projects;
    ObservableList<Team> teams;
    ObservableList<Student> students;
    String subject;
    String courseInfoSemester;
    String number;
    String year;
    String courseTitle;
    String courseInstructorName;
    String courseInstructorHome;
    String schoolBannerImage;
    String leftFooterImage;
    String rightFooterImage;

    public static final int MIN_START_HOUR = 9;
    public static final int MAX_END_HOUR = 20;

    public CourseSiteData() {
// KEEP THIS FOR LATER
        app = new GeneratorApp();

        // CONSTRUCT THE LIST OF TAs FOR THE TABLE
        teachingAssistants = FXCollections.observableArrayList();
        recitations = FXCollections.observableArrayList();
        hws = FXCollections.observableArrayList();
        holidays = FXCollections.observableArrayList();
        lectures = FXCollections.observableArrayList();
        recitationsSchedules = FXCollections.observableArrayList();
        references = FXCollections.observableArrayList();
        projects = FXCollections.observableArrayList();
        teams = FXCollections.observableArrayList();
        students = FXCollections.observableArrayList();
        scheduleItems = FXCollections.observableArrayList();

        // THESE ARE THE DEFAULT OFFICE HOURS
        startHour = MIN_START_HOUR;
        endHour = MAX_END_HOUR;

        //THIS WILL STORE OUR OFFICE HOURS
        officeHours = new HashMap();

        // THESE ARE THE LANGUAGE-DEPENDENT OFFICE HOURS GRID HEADERS
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        //ArrayList<String> timeHeaders = props.getPropertyOptionsList(GeneratorProp.OFFICE_HOURS_TABLE_HEADERS);
        //ArrayList<String> dowHeaders = props.getPropertyOptionsList(GeneratorProp.DAYS_OF_WEEK);

        ArrayList<String> timeHeaders = new ArrayList();
        ArrayList<String> dowHeaders = new ArrayList();
        timeHeaders.add("Start Time");
        timeHeaders.add("End Time");
        dowHeaders.add("MONDAY");

        gridHeaders = new ArrayList();
        gridHeaders.addAll(timeHeaders);
        gridHeaders.addAll(dowHeaders);
    }

    public CourseSiteData(GeneratorApp initApp) {
        // KEEP THIS FOR LATER
        app = initApp;

        // CONSTRUCT THE LIST OF TAs FOR THE TABLE
        teachingAssistants = FXCollections.observableArrayList();
        recitations = FXCollections.observableArrayList();
        hws = FXCollections.observableArrayList();
        holidays = FXCollections.observableArrayList();
        lectures = FXCollections.observableArrayList();
        recitationsSchedules = FXCollections.observableArrayList();
        references = FXCollections.observableArrayList();
        projects = FXCollections.observableArrayList();
        teams = FXCollections.observableArrayList();
        students = FXCollections.observableArrayList();
        scheduleItems = FXCollections.observableArrayList();

        // THESE ARE THE DEFAULT OFFICE HOURS
        startHour = MIN_START_HOUR;
        endHour = MAX_END_HOUR;

        //THIS WILL STORE OUR OFFICE HOURS
        officeHours = new HashMap();

        // THESE ARE THE LANGUAGE-DEPENDENT OFFICE HOURS GRID HEADERS
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        ArrayList<String> timeHeaders = props.getPropertyOptionsList(GeneratorProp.OFFICE_HOURS_TABLE_HEADERS);
        ArrayList<String> dowHeaders = props.getPropertyOptionsList(GeneratorProp.DAYS_OF_WEEK);

        gridHeaders = new ArrayList();
        gridHeaders.addAll(timeHeaders);
        gridHeaders.addAll(dowHeaders);
    }

    public void initHours(String startHourText, String endHourText) {
        int initStartHour = Integer.parseInt(startHourText);
        int initEndHour = Integer.parseInt(endHourText);
        //if ((initStartHour >= MIN_START_HOUR)
        //&& (initEndHour <= MAX_END_HOUR)
        //&& (initStartHour <= initEndHour)) {
        //THESE ARE VALID HOURS SO KEEP THEM
        initOfficeHours(initStartHour, initEndHour);
        //}
    }

    private void initOfficeHours(int initStartHour, int initEndHour) {
        // NOTE THAT THESE VALUES MUST BE PRE-VERIFIED
        startHour = initStartHour;
        endHour = initEndHour;

        // EMPTY THE CURRENT OFFICE HOURS VALUES
        officeHours.clear();

        // WE'LL BUILD THE USER INTERFACE COMPONENT FOR THE
        // OFFICE HOURS GRID AND FEED THEM TO OUR DATA
        // STRUCTURE AS WE GO
        GeneratorWorkspace workspaceComponent = (GeneratorWorkspace) app.getWorkspaceComponent();
        workspaceComponent.reloadOfficeHoursGrid(this);
    }

    @Override
    public void resetData() {
        startHour = MIN_START_HOUR;
        endHour = MAX_END_HOUR;
        teachingAssistants.clear();
        officeHours.clear();
        //For schedule data
        startingMondayMonth = 1;
        startingMondayDay = 1;
        endingFridayMonth = 12;
        endingFridayDay = 30;
        hws.clear();
        holidays.clear();
        lectures.clear();
        recitationsSchedules.clear();
        references.clear();
        scheduleItems.clear();
        teams.clear();
        students.clear();
        recitations.clear();
        //For work
        semester = "";
        projects.clear();
        teams.clear();
        students.clear();
        subject = "";
        courseInfoSemester = "";
        number = "";
        year = "";
        courseTitle = "";
        courseInstructorName = "";
        courseInstructorHome = "";

    }

    public String getCellKey(int col, int row) {
        return col + "_" + row;
    }

    public StringProperty getCellTextProperty(int col, int row) {
        String cellKey = getCellKey(col, row);
        return officeHours.get(cellKey);
    }

    public HashMap<String, StringProperty> getOfficeHours() {
        return officeHours;
    }

    public int getNumRows() {
        return ((endHour - startHour) * 2) + 1;
    }

    public String getTimeString(int militaryHour, boolean onHour) {
        String minutesText = "00";
        if (!onHour) {
            minutesText = "30";
        }

        // FIRST THE START AND END CELLS
        int hour = militaryHour;
        if (hour > 12) {
            hour -= 12;
        }
        String cellText = "" + hour + ":" + minutesText;
        if (militaryHour < 12) {
            cellText += "am";
        } else {
            cellText += "pm";
        }
        return cellText;
    }

    public String getCellKey(String day, String time) {
        int col = gridHeaders.indexOf(day);
        int row = 1;
        int hour = Integer.parseInt(time.substring(0, time.indexOf("_")));
        int milHour = hour;
        //if (hour < startHour)
        //milHour += 12;
        if (time.contains("pm")) {
            if (hour != 12) {
                milHour += 12;
            }
        }
        row += (milHour - startHour) * 2;
        if (time.contains("_30")) {
            row += 1;
        }
        return getCellKey(col, row);
    }

    public TeachingAssistant getTA(String testName) {
        for (TeachingAssistant ta : teachingAssistants) {
            if (ta.getName().equals(testName)) {
                return ta;
            }
        }
        return null;
    }

    public void addTA(String initName, String initEmail, Boolean underGrad) {
        // MAKE THE TA
        TeachingAssistant ta = new TeachingAssistant(initName, initEmail);
        ta.setUnderGrad(underGrad);
        // ADD THE TA
        if (!containsTA(initName, initEmail)) {
            teachingAssistants.add(ta);
        }

        // SORT THE TAS
        Collections.sort(teachingAssistants);
    }

    public void removeTA(String name) {
        for (TeachingAssistant ta : teachingAssistants) {
            if (name.equals(ta.getName())) {
                teachingAssistants.remove(ta);
                return;
            }
        }
    }

    public void addOfficeHoursReservation(String day, String time, String taName) {
        String cellKey = getCellKey(day, time);
        toggleTAOfficeHours(cellKey, taName);
    }

    /**
     * This method is for giving this data manager the string property for a
     * given cell.
     */
    public void setCellProperty(int col, int row, StringProperty prop) {
        String cellKey = getCellKey(col, row);
        officeHours.put(cellKey, prop);
    }

    /**
     * This method is for setting the string property for a given cell.
     */
    public void setGridProperty(ArrayList<ArrayList<StringProperty>> grid,
            int column, int row, StringProperty prop) {
        grid.get(row).set(column, prop);
    }

    /**
     * This function toggles the taName in the cell represented by cellKey.
     * Toggle means if it's there it removes it, if it's not there it adds it.
     */
    public void toggleTAOfficeHours(String cellKey, String taName) {
        StringProperty cellProp = officeHours.get(cellKey);
        String cellText = cellProp.getValue();

        // IF IT ALREADY HAS THE TA, REMOVE IT
        if (cellText.contains(taName)) {
            removeTAFromCell(cellProp, taName);
        } // OTHERWISE ADD IT
        else if (cellText.length() == 0) {
            cellProp.setValue(taName);
        } else {
            cellProp.setValue(cellText + "\n" + taName);
        }
    }

    /**
     * This method removes taName from the office grid cell represented by
     * cellProp.
     */
    public void removeTAFromCell(StringProperty cellProp, String taName) {
        // GET THE CELL TEXT
        String cellText = cellProp.getValue();
        // IS IT THE ONLY TA IN THE CELL?
        if (cellText.equals(taName)) {
            cellProp.setValue("");
        } // IS IT THE FIRST TA IN A CELL WITH MULTIPLE TA'S?
        else if (cellText.indexOf(taName) == 0) {
            int startIndex = cellText.indexOf("\n") + 1;
            cellText = cellText.substring(startIndex);
            cellProp.setValue(cellText);
        } // IS IT IN THE MIDDLE OF A LIST OF TAs
        else if (cellText.indexOf(taName) < cellText.indexOf("\n", cellText.indexOf(taName))) {
            int startIndex = cellText.indexOf("\n" + taName);
            int endIndex = startIndex + taName.length() + 1;
            cellText = cellText.substring(0, startIndex) + cellText.substring(endIndex);
            cellProp.setValue(cellText);
        } // IT MUST BE THE LAST TA
        else {
            int startIndex = cellText.indexOf("\n" + taName);
            cellText = cellText.substring(0, startIndex);
            cellProp.setValue(cellText);
        }
    }

    public void editTAFromCell(StringProperty cellProp, String oldName, String newName) {
        // GET THE CELL TEXT
        String cellText = cellProp.getValue();
        // IS IT THE ONLY TA IN THE CELL?
        if (cellText.equals(oldName)) {
            cellProp.setValue(newName);
        } // IS IT THE FIRST TA IN A CELL WITH MULTIPLE TA'S?
        else if (cellText.indexOf(oldName) == 0) {
            int startIndex = cellText.indexOf("\n") + 1;
            cellText = cellText.substring(startIndex);
            cellText = newName + "\n" + cellText;
            cellProp.setValue(cellText);
        } // IS IT IN THE MIDDLE OF A LIST OF TAs
        else if (cellText.indexOf(oldName) < cellText.indexOf("\n", cellText.indexOf(oldName))) {
            int startIndex = cellText.indexOf("\n" + oldName);
            int endIndex = startIndex + oldName.length() + 1;
            cellText = cellText.substring(0, startIndex) + "\n" + newName + cellText.substring(endIndex);
            cellProp.setValue(cellText);
        } // IT MUST BE THE LAST TA
        else {
            int startIndex = cellText.indexOf("\n" + oldName);
            cellText = cellText.substring(0, startIndex) + "\n" + newName;
            cellProp.setValue(cellText);
        }
    }

    public boolean containsTA(String testName, String testEmail) {
        for (TeachingAssistant ta : teachingAssistants) {
            if (ta.getName().equals(testName)) {
                return true;
            }
            if (ta.getEmail().equals(testEmail)) {
                return true;
            }
        }
        return false;
    }

    public void setStartHour(int newStartHour) {
        startHour = newStartHour;
    }

    public void setEndHour(int newEndHour) {
        endHour = newEndHour;
    }

    public GeneratorApp getApp() {
        return app;
    }

    public ObservableList<TeachingAssistant> getTeachingAssistants() {
        return teachingAssistants;
    }

    public ArrayList<String> getGridHeaders() {
        return gridHeaders;
    }

    public int getStartHour() {
        return startHour;
    }

    public int getEndHour() {
        return endHour;
    }

    public ObservableList<Recitation> getRecitations() {
        return recitations;
    }

    public int getStartingMondayMonth() {
        return startingMondayMonth;
    }

    public int getStartingMondayDay() {
        return startingMondayDay;
    }

    public int getEndingFridayMonth() {
        return endingFridayMonth;
    }

    public int getEndingFridayDay() {
        return endingFridayDay;
    }

    public ObservableList<HWs> getHws() {
        return hws;
    }

    public ObservableList<Holiday> getHolidays() {
        return holidays;
    }

    public ObservableList<Lecture> getLectures() {
        return lectures;
    }

    public ObservableList<RecitationsSchedule> getRecitationsSchedule() {
        return recitationsSchedules;
    }

    public ObservableList<Reference> getReferences() {
        return references;
    }

    public String getSemester() {
        return semester;
    }

    public ObservableList<Project> getProjects() {
        ObservableList<Project> pro = FXCollections.observableArrayList();
        
        for(Team team: teams){
            int i = 0;
            String[] names = {"none", "none","none","none"};
            for(Student student : students){
                if(student.getTeam().equals(team.getName())){
                    names[i] = student.getLastName() + " " + student.getFirstName();
                    i++;
                }
            }
            Project proj = new Project(team.getName(), names, team.getLink());
            pro.add(proj);
        }
        return pro;
    }

    public ObservableList<Team> getTeams() {
        return teams;
    }

    public ObservableList<Student> getStudents() {
        return students;
    }

    public void setRecitationsSchedules(ObservableList<RecitationsSchedule> recitationsSchedules) {
        this.recitationsSchedules = recitationsSchedules;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public void setCourseInfoSemester(String courseInfoSemester) {
        this.courseInfoSemester = courseInfoSemester;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public void setCourseTitle(String courseTitle) {
        this.courseTitle = courseTitle;
    }

    public void setCourseInstructorName(String courseInstructorName) {
        this.courseInstructorName = courseInstructorName;
    }

    public void setCourseInstructorHome(String courseInstructorHome) {
        this.courseInstructorHome = courseInstructorHome;
    }

    public ObservableList<RecitationsSchedule> getRecitationsSchedules() {
        return recitationsSchedules;
    }

    public String getSubject() {
        return subject;
    }

    public String getCourseInfoSemester() {
        return courseInfoSemester;
    }

    public String getNumber() {
        return number;
    }

    public String getYear() {
        return year;
    }

    public String getCourseTitle() {
        return courseTitle;
    }

    public String getCourseInstructorName() {
        return courseInstructorName;
    }

    public String getCourseInstructorHome() {
        return courseInstructorHome;
    }

    public static int getMIN_START_HOUR() {
        return MIN_START_HOUR;
    }

    public static int getMAX_END_HOUR() {
        return MAX_END_HOUR;
    }

    public void setApp(GeneratorApp app) {
        this.app = app;
    }

    public void setTeachingAssistants(ObservableList<TeachingAssistant> teachingAssistants) {
        this.teachingAssistants = teachingAssistants;
    }

    public void setGridHeaders(ArrayList<String> gridHeaders) {
        this.gridHeaders = gridHeaders;
    }

    public void setScheduleItems(ObservableList<ScheduleItem> scheduleItems) {
        this.scheduleItems = scheduleItems;
    }

    public ObservableList<ScheduleItem> getScheduleItems() {
        return scheduleItems;
    }

    public void setOfficeHours(HashMap<String, StringProperty> officeHours) {
        this.officeHours = officeHours;
    }

    public void setRecitations(ObservableList<Recitation> recitations) {
        this.recitations = recitations;
    }

    public void setStartingMondayMonth(int startingMondayMonth) {
        this.startingMondayMonth = startingMondayMonth;
    }

    public void setStartingMondayDay(int startingMondayDay) {
        this.startingMondayDay = startingMondayDay;
    }

    public void setEndingFridayMonth(int endingFridayMonth) {
        this.endingFridayMonth = endingFridayMonth;
    }

    public void setEndingFridayDay(int endingFridayDay) {
        this.endingFridayDay = endingFridayDay;
    }

    public void setHws(ObservableList<HWs> hws) {
        this.hws = hws;
    }

    public void setHolidays(ObservableList<Holiday> holidays) {
        this.holidays = holidays;
    }

    public void setLectures(ObservableList<Lecture> lectures) {
        this.lectures = lectures;
    }

    public void setRecitationsSchedule(ObservableList<RecitationsSchedule> recitationsSchedule) {
        this.recitationsSchedules = recitationsSchedule;
    }

    public void setReferences(ObservableList<Reference> references) {
        this.references = references;
    }

    public void setSemester(String semester) {
        this.semester = semester;
    }

    public void setProjects(ObservableList<Project> projects) {
        this.projects = projects;
    }

    public void setTeams(ObservableList<Team> teams) {
        this.teams = teams;
    }

    public void setStudents(ObservableList<Student> students) {
        this.students = students;
    }

    public void addRecitation(String section, String instructor, String dayTime, String location, String ta1, String ta2) {
        Recitation recitation = new Recitation(section, instructor, dayTime, location, ta1, ta2);
        if (!containRecitation(section)) {
            recitations.add(recitation);
        }
        Collections.sort(recitations);
    }

    public void addProject(String name, String[] students, String link) {
        Project pro = new Project(name, students, link);
        projects.add(pro);
    }

    public void addTeam(String name, int red, int green, int blue, String textColor, String link) {
        Color color = Color.rgb(red, green, blue);
        String colorS = color.toString();
        Team t = new Team(name, colorS, textColor, link);
        teams.add(t);
    }

    public void addTeam(String name, String color, String textColor, String link) {
        Team t = new Team(name, color, textColor, link);
        if (!containTeam(name)) {
            teams.add(t);
        }
    }

    public void removeTeam(String name) {
        for (Team team : teams) {
            if (team.getName().equals(name)) {
                teams.remove(team);
                return;
            }
        }
    }

    public boolean containTeam(String name) {
        for (Team team : teams) {
            if (team.getName().equals(name)) {
                return true;
            }
        }
        return false;
    }

    public void addStudent(String lastName, String firstName, String team, String role) {
        Student stu = new Student(lastName, firstName, team, role);
        if (!containStudent(lastName, firstName)) {
            students.add(stu);
        }
    }

    public boolean containStudent(String lastName, String firstName) {
        for (Student stu : students) {
            if (stu.getFirstName().equals(firstName) && stu.getLastName().equals(lastName)) {
                return true;
            }
        }
        return false;
    }

    public void removeStudent(String lastName, String firstName) {
        for (Student stu : students) {
            if (stu.getFirstName().equals(firstName) && stu.getLastName().equals(lastName)) {
                students.remove(stu);
                return;
            }
        }
    }

    public void addHoliday(String date, String title, String link) {
        Holiday holiday = new Holiday("Holiday", date, title, "");
        holiday.setLink(link);
        if (!containScheduleItem(title, date)) {
            holidays.add(holiday);
            scheduleItems.add(holiday);
        }
    }

    public void addLecture(String date, String title, String topic, String link) {
        Lecture lecture = new Lecture("Lecture", date, title, topic);
        lecture.setLink(link);
        if (!containScheduleItem(title, date)) {
            lectures.add(lecture);
            scheduleItems.add(lecture);
        }
    }

    public void addReference(String date, String title, String topic, String link) {
        Reference reference = new Reference("Reference", date, title, topic);
        reference.setLink(link);
        if (!containScheduleItem(title, date)) {
            references.add(reference);
            scheduleItems.add(reference);
        }
    }

    public void addRecitationSchedule(String date, String title, String topic) {
        RecitationsSchedule recSch = new RecitationsSchedule("Recitation", date, title, topic);
        if (!containScheduleItem(title, date)) {
            recitationsSchedules.add(recSch);
            scheduleItems.add(recSch);
        }
    }

    public void addHW(String date, String title, String topic, String link, String time, String criteria) {
        HWs hw = new HWs("HW", date, title, topic);
        hw.setLink(link);
        hw.setTime(time);
        hw.setCriteria(criteria);
        if (!containScheduleItem(title, date)) {
            hws.add(hw);
            scheduleItems.add(hw);
        }
    }

    

    public void removeRecitation(String section) {
        for (Recitation re : recitations) {
            if (section.equals(re.getSection())) {
                recitations.remove(re);
                return;
            }
        }
    }

    public void addScheduleItem(ScheduleItem schedule){
        if(!containScheduleItem(schedule.getTitle(), schedule.getDate())){
            if(schedule.getType().equals("Reference")){
                references.add((Reference) schedule);
                scheduleItems.add((Reference) schedule);
            }
            else if(schedule.getType().equals("Recitation")){
                recitationsSchedules.add((RecitationsSchedule) schedule);
                scheduleItems.add((RecitationsSchedule) schedule);
            }
            else if(schedule.getType().equals("HW")){
                hws.add((HWs)schedule);
                scheduleItems.add((HWs) schedule);
            }
            else if(schedule.getType().equals("Lecture")){
                Lecture lecture = (Lecture) schedule;
                lectures.add(lecture);
                scheduleItems.add(lecture);
            }
            else{
                Holiday holiday = (Holiday) schedule;
                holidays.add(holiday);
                scheduleItems.add(holiday);
            }
        }
    }
    public void removeScheduleItem(String title, String date) {
        for (ScheduleItem si : scheduleItems) {
            if (si.getTitle().equals(title) && si.getDate().equals(date)) {
                scheduleItems.remove(si);
                String type = si.getType();
                if (type == "Holiday") {
                    holidays.remove((Holiday) si);
                } else if (type == "Lecture") {
                    lectures.remove((Lecture) si);
                } else if (type == "Reference") {
                    references.remove((Reference) si);
                } else if (type == "Recitation") {
                    recitationsSchedules.remove((RecitationsSchedule) si);
                } else {
                    hws.remove((HWs) si);
                }
                return;
            }
        }
    }

    public boolean containRecitation(String section) {
        for (Recitation re : recitations) {
            if (section.equals(re.getSection())) {
                return true;
            }
        }
        return false;
    }

    public Holiday getHoliday(String title, String date) {
        for (Holiday ho : holidays) {
            if (ho.getTitle().equals(title) && ho.getDate().equals(date)) {
                return ho;
            }
        }
        return null;
    }

    public Lecture getLecture(String title, String date) {
        for (Lecture le : lectures) {
            if (le.getTitle().equals(title) && le.getDate().equals(date)) {
                return le;
            }
        }
        return null;
    }

    public HWs getHWs(String title, String date) {
        for (HWs hw : hws) {
            if (hw.getTitle().equals(title) && hw.getDate().equals(date)) {
                return hw;
            }
        }
        return null;
    }

    public Reference getReference(String title, String date) {
        for (Reference rf : references) {
            if (rf.getTitle().equals(title) && rf.getDate().equals(date)) {
                return rf;
            }
        }
        return null;
    }

    public RecitationsSchedule getRecitationScheduleItem(String title, String date) {
        for (RecitationsSchedule rs : recitationsSchedules) {
            if (rs.getTitle().equals(title)&& rs.getDate().equals(date)) {
                return rs;
            }
        }
        return null;
    }

    public boolean containScheduleItem(String title, String date) {
        for (ScheduleItem si : scheduleItems) {
            if (si.getTitle().equals(title) && si.getDate().equals(date)) {
                return true;
            }
        }
        return false;
    }

    public Team getTeam(String name) {
        for (Team team : teams) {
            if (team.getName().equals(name)) {
                return team;
            }
        }
        return null;
    }

    public ObservableList<TeachingAssistant> getUnderGradTa() {
        ObservableList<TeachingAssistant> underGrad = FXCollections.observableArrayList();
        for (int i = 0; i < teachingAssistants.size(); i++) {
            if (teachingAssistants.get(i).getUnderGrad()) {
                underGrad.add(teachingAssistants.get(i));
            }
        }

        return underGrad;

    }

    public ObservableList<TeachingAssistant> getGradTa() {
        ObservableList<TeachingAssistant> gradTA = FXCollections.observableArrayList();
        for (int i = 0; i < teachingAssistants.size(); i++) {
            if (!teachingAssistants.get(i).getUnderGrad()) {
                gradTA.add(teachingAssistants.get(i));
            }
        }

        return gradTA;
    }

    public String getSchoolBannerImage() {
        return schoolBannerImage;
    }

    public String getLeftFooterImage() {
        return leftFooterImage;
    }

    public String getRightFooterImage() {
        return rightFooterImage;
    }

    public void setSchoolBannerImage(String schoolBannerImage) {
        this.schoolBannerImage = schoolBannerImage;
    }

    public void setLeftFooterImage(String leftFooterImage) {
        this.leftFooterImage = leftFooterImage;
    }

    public void setRightFooterImage(String rightFooterImage) {
        this.rightFooterImage = rightFooterImage;
    }

}
