/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coursesitegenerator.workspace;

import coursesitegenerator.GeneratorApp;
import static coursesitegenerator.GeneratorProp.ADD_HOLIDAY_ERROR_MESSAGE;
import static coursesitegenerator.GeneratorProp.ADD_HOLIDAY_ERROR_TITLE;
import static coursesitegenerator.GeneratorProp.ADD_HW_ERROR_MESSAGE;
import static coursesitegenerator.GeneratorProp.ADD_HW_ERROR_TITLE;
import static coursesitegenerator.GeneratorProp.ADD_LECTURE_ERROR_MESSAGE;
import static coursesitegenerator.GeneratorProp.ADD_LECTURE_ERROR_TITLE;
import static coursesitegenerator.GeneratorProp.ADD_RECITATION_SCHEDULE_ERROR_MESSAGE;
import static coursesitegenerator.GeneratorProp.ADD_RECITATION_SCHEDULE_ERROR_TITLE;
import static coursesitegenerator.GeneratorProp.ADD_REFERENCE_ERROR_MESSAGE;
import static coursesitegenerator.GeneratorProp.ADD_REFERENCE_ERROR_TITLE;
import static coursesitegenerator.GeneratorProp.ADD_STUDENT_ERROR_MESSAGE;
import static coursesitegenerator.GeneratorProp.ADD_STUDENT_ERROR_TITLE;
import static coursesitegenerator.GeneratorProp.ADD_TEAM_ERROR_MESSAGE;
import static coursesitegenerator.GeneratorProp.ADD_TEAM_ERROR_TITLE;
import static coursesitegenerator.GeneratorProp.CHANGE_TIME_MESSAGE;
//import static coursesitegenerator.GeneratorProp.CHANGE_TIME_MESSAGE;
import static coursesitegenerator.GeneratorProp.CHANGE_TIME_TITLE;
import static coursesitegenerator.GeneratorProp.EMAIL_ADDRESS_ERROR;
import static coursesitegenerator.GeneratorProp.MISSING_RECITATION_DAYTIME_MESSAGE;
import static coursesitegenerator.GeneratorProp.MISSING_RECITATION_DAYTIME_TITLE;
import static coursesitegenerator.GeneratorProp.MISSING_RECITATION_INSTRUCTOR_MESSAGE;
import static coursesitegenerator.GeneratorProp.MISSING_RECITATION_INSTRUCTOR_TITLE;
import static coursesitegenerator.GeneratorProp.MISSING_RECITATION_LOCATION_MESSAGE;
import static coursesitegenerator.GeneratorProp.MISSING_RECITATION_LOCATION_TITLE;
import static coursesitegenerator.GeneratorProp.MISSING_RECITATION_SECTION_MESSAGE;
import static coursesitegenerator.GeneratorProp.MISSING_RECITATION_SECTION_TITLE;
import static coursesitegenerator.GeneratorProp.MISSING_TA_EMAIL_MESSAGE;
import static coursesitegenerator.GeneratorProp.MISSING_TA_EMAIL_TITLE;
import static coursesitegenerator.GeneratorProp.MISSING_TA_NAME_MESSAGE;
import static coursesitegenerator.GeneratorProp.MISSING_TA_NAME_TITLE;
import static coursesitegenerator.GeneratorProp.RECITATION_ALREADY_EXIST_MESSAGE;
import static coursesitegenerator.GeneratorProp.RECITATION_ALREADY_EXIST_TITLE;
import static coursesitegenerator.GeneratorProp.SCHEDULE_ITEM_ALREADY_EXIST_MESSAGE;
import static coursesitegenerator.GeneratorProp.SCHEDULE_ITEM_ALREADY_EXIST_TITLE;
import static coursesitegenerator.GeneratorProp.SCHEDULE_TYPE_ERROR_MESSAGE;
import static coursesitegenerator.GeneratorProp.SCHEDULE_TYPE_ERROR_TITLE;
import static coursesitegenerator.GeneratorProp.STARTING_MONDAY_ERROR_MESSAGE;
import static coursesitegenerator.GeneratorProp.STARTING_MONDAY_ERROR_TITLE;
import static coursesitegenerator.GeneratorProp.START_HOUR_ERROR;
import static coursesitegenerator.GeneratorProp.START_HOUR_TITLE;
import static coursesitegenerator.GeneratorProp.TA_NAME_AND_EMAIL_NOT_UNIQUE_MESSAGE;
import static coursesitegenerator.GeneratorProp.TA_NAME_AND_EMAIL_NOT_UNIQUE_TITLE;
import coursesitegenerator.data.CourseSiteData;
import coursesitegenerator.data.HWs;
import coursesitegenerator.data.Holiday;
import coursesitegenerator.data.Lecture;
import coursesitegenerator.data.Recitation;
import coursesitegenerator.data.RecitationsSchedule;
import coursesitegenerator.data.Reference;
import coursesitegenerator.data.ScheduleItem;
import coursesitegenerator.data.Student;
import coursesitegenerator.data.TAData;
import coursesitegenerator.data.TeachingAssistant;
import coursesitegenerator.data.Team;
import coursesitegenerator.file.CourseSiteFile;
import coursesitegenerator.file.TimeSlot;
import coursesitegenerator.jTPS.Add_Recitation_Transaction;
import coursesitegenerator.jTPS.Add_Schedule_Transaction;
import coursesitegenerator.jTPS.Add_Student_Transaction;
import static coursesitegenerator.style.style.CLASS_HIGHLIGHTED_GRID_CELL;
import static coursesitegenerator.style.style.CLASS_HIGHLIGHTED_GRID_ROW_OR_COLUMN;
import static coursesitegenerator.style.style.CLASS_OFFICE_HOURS_GRID_TA_CELL_PANE;
import djf.ui.AppGUI;
import djf.ui.AppMessageDialogSingleton;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javafx.scene.control.TextField;
import properties_manager.PropertiesManager;
import coursesitegenerator.workspace.GeneratorWorkspace;
import djf.ui.AppYesNoCancelDialogSingleton;
import java.time.LocalDate;
import java.time.Month;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import javafx.beans.property.StringProperty;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TableView;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import jtps.jTPS;
import coursesitegenerator.jTPS.Add_TA_Transaction;
import coursesitegenerator.jTPS.Add_Team_Transaction;
import coursesitegenerator.jTPS.Change_Office_End_Hour_Transaction;
import coursesitegenerator.jTPS.Change_Office_Start_Hour_Transaction;
import coursesitegenerator.jTPS.Delete_Recitation_Transaction;
import coursesitegenerator.jTPS.Delete_Schedule_Transaction;
import coursesitegenerator.jTPS.Delete_Student_Transaction;
import coursesitegenerator.jTPS.Delete_TA_Transaction;
import coursesitegenerator.jTPS.Delete_Team_Transaction;
import coursesitegenerator.jTPS.Edit_Recitation_Transaction;
import coursesitegenerator.jTPS.Edit_Schedule_Transaction;
import coursesitegenerator.jTPS.Edit_Student_Transaction;
import coursesitegenerator.jTPS.Edit_TA_Transaction;
import coursesitegenerator.jTPS.Edit_Team_Transaction;
import coursesitegenerator.jTPS.Toggle_TA_Transaction;
import static djf.settings.AppPropertyType.LOAD_ERROR_MESSAGE;
import static djf.settings.AppPropertyType.LOAD_ERROR_TITLE;
import static djf.settings.AppStartupConstants.PATH_IMAGES;
import static djf.settings.AppStartupConstants.PATH_WORK;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.image.Image;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javax.imageio.ImageIO;
import org.apache.commons.io.FileUtils;

/**
 *
 * @author xinyu
 */
public class GeneratorController {

    static jTPS jtps = new jTPS();
    GeneratorApp app;
    private Pattern pattern;
    private Matcher matcher;
    private static final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
            + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

    public GeneratorController(GeneratorApp initApp) {
        app = initApp;
        pattern = Pattern.compile(EMAIL_PATTERN);
    }

    public GeneratorApp getApp() {
        return app;
    }

    public void handleUndoTransaction() {
        jtps.undoTransaction();
        markWorkAsEdited();
    }

    public void handleRedoTransaction() {
        jtps.doTransaction();
        markWorkAsEdited();
    }

    /**
     * This helper method should be called every time an edit happens.
     */
    private void markWorkAsEdited() {
        // MARK WORK AS EDITED
        AppGUI gui = app.getGUI();
        gui.getFileController().markAsEdited(gui);
    }

    /**
     * This method responds to when the user requests to add a new TA via the
     * UI. Note that it must first do some validation to make sure a unique name
     * and email address has been provided.
     */
    public void handleAddTA() {
        // WE'LL NEED THE WORKSPACE TO RETRIEVE THE USER INPUT VALUES
        GeneratorWorkspace workspace = (GeneratorWorkspace) app.getWorkspaceComponent();
        TextField nameTextField = workspace.getNameTextField();
        TextField emailTextField = workspace.getEmailTextField();
        String name = nameTextField.getText();
        String email = emailTextField.getText();
        Boolean under = workspace.getUnderGradTABox().isSelected();

        // WE'LL NEED TO ASK THE DATA SOME QUESTIONS TOO
        CourseSiteData data = (CourseSiteData) app.getDataComponent();

        // WE'LL NEED THIS IN CASE WE NEED TO DISPLAY ANY ERROR MESSAGES
        PropertiesManager props = PropertiesManager.getPropertiesManager();

        // DID THE USER NEGLECT TO PROVIDE A TA NAME?
        if (name.isEmpty()) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(MISSING_TA_NAME_TITLE), props.getProperty(MISSING_TA_NAME_MESSAGE));
        } // DID THE USER NEGLECT TO PROVIDE A TA EMAIL?
        else if (email.isEmpty()) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(MISSING_TA_EMAIL_TITLE), props.getProperty(MISSING_TA_EMAIL_MESSAGE));
        } else if (!validate(email)) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(EMAIL_ADDRESS_ERROR), props.getProperty(EMAIL_ADDRESS_ERROR));
        } // DOES A TA ALREADY HAVE THE SAME NAME OR EMAIL?
        else if (data.containsTA(name, email)) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(TA_NAME_AND_EMAIL_NOT_UNIQUE_TITLE), props.getProperty(TA_NAME_AND_EMAIL_NOT_UNIQUE_MESSAGE));
        } // EVERYTHING IS FINE, ADD A NEW TA
        else {
            // ADD THE NEW TA TO THE DATA
            data.addTA(name, email, under);

            // CLEAR THE TEXT FIELDS
            nameTextField.setText("");
            emailTextField.setText("");

            // AND SEND THE CARET BACK TO THE NAME TEXT FIELD FOR EASY DATA ENTRY
            nameTextField.requestFocus();

            //Add_TA_Transaction tran = new Add_TA_Transaction(name, email, this);
            //workspace.jtps.addTransaction(tran);
            // WE'VE CHANGED STUFF
            markWorkAsEdited();
            workspace.reloadRecitationPane(data);
            Add_TA_Transaction t = new Add_TA_Transaction(name, email, under, data, workspace);
            jtps.addTransaction(t);
        }
    }

    /**
     * Validate email with regular expression
     *
     * @param email email address string
     * @return true if it matches, false if it's invalid email address
     */
    public boolean validate(final String email) {
        matcher = pattern.matcher(email);
        return matcher.matches();
    }

    /**
     * This function provides a response for when the user presses a keyboard
     * key. Note that we're only responding to Delete, to remove a TA.
     *
     * @param code The keyboard code pressed.
     */
    public void handleKeyPress(KeyCode code) {
        // DID THE USER PRESS THE DELETE KEY?
        if (code == KeyCode.DELETE) {
            // GET THE TABLE
            GeneratorWorkspace workspace = (GeneratorWorkspace) app.getWorkspaceComponent();
            TableView taTable = workspace.getTaTable();

            // IS A TA SELECTED IN THE TABLE?
            Object selectedItem = taTable.getSelectionModel().getSelectedItem();
            if (selectedItem != null) {
                // GET THE TA AND REMOVE IT
                TeachingAssistant ta = (TeachingAssistant) selectedItem;
                String taName = ta.getName();
                String email = ta.getEmail();
                CourseSiteData data = (CourseSiteData) app.getDataComponent();
                data.removeTA(taName);

                int numRow = data.getNumRows();
                int[][] cont = new int[8][numRow + 1];
                String[][] infor = new String[8][numRow + 1];
                for (int i = 0; i < 8; i++) {
                    for (int j = 0; j <= numRow; j++) {
                        infor[i][j] = null;
                        cont[i][j] = 0;
                    }
                }
                String a;
                for (int row = 1; row < numRow; row++) {
                    for (int col = 2; col < 7; col++) {
                        StringProperty prop = data.getCellTextProperty(col, row);
                        String cellText = prop.getValue();
                        if (cellText.contains(taName)) {
                            cont[col][row] = 1;
                            a = prop.getValue();
                            infor[col][row] = prop.getValue();
                        }
                    }
                }
                // AND BE SURE TO REMOVE ALL THE TA'S OFFICE HOURS
                HashMap<String, Label> labels = workspace.getOfficeHoursGridTACellLabels();

                for (Label label : labels.values()) {
                    if (label.getText().equals(taName)
                            || (label.getText().contains(taName + "\n"))
                            || (label.getText().contains("\n" + taName))) {
                        data.removeTAFromCell(label.textProperty(), taName);
                    }
                }
                // WE'VE CHANGED STUFF
                markWorkAsEdited();
            }
        }
    }

    /**
     * This function provides a response for when the user clicks on the office
     * hours grid to add or remove a TA to a time slot.
     *
     * @param pane The pane that was toggled.
     */
    public void handleCellToggle(Pane pane) {
        // GET THE TABLE
        GeneratorWorkspace workspace = (GeneratorWorkspace) app.getWorkspaceComponent();
        TableView taTable = workspace.getTaTable();

        // IS A TA SELECTED IN THE TABLE?
        Object selectedItem = taTable.getSelectionModel().getSelectedItem();
        boolean exist = false;
        if (selectedItem != null) {
            // GET THE TA
            TeachingAssistant ta = (TeachingAssistant) selectedItem;
            String taName = ta.getName();
            CourseSiteData data = (CourseSiteData) app.getDataComponent();
            String cellKey = pane.getId();
            StringProperty cellProp = data.getOfficeHours().get(cellKey);
            String cellText = cellProp.getValue();
            if (cellText.contains(taName)) {
                exist = false;
            } else {
                exist = true;
            }
            // AND TOGGLE THE OFFICE HOURS IN THE CLICKED CELL
            Toggle_TA_Transaction tran = new Toggle_TA_Transaction(taName, pane, data, exist);
            jtps.addTransaction(tran);
            //data.toggleTAOfficeHours(cellKey, taName);

            // WE'VE CHANGED STUFF
            markWorkAsEdited();
        }

    }

    void handleGridCellMouseExited(Pane pane) {
        String cellKey = pane.getId();
        CourseSiteData data = (CourseSiteData) app.getDataComponent();
        int column = Integer.parseInt(cellKey.substring(0, cellKey.indexOf("_")));
        int row = Integer.parseInt(cellKey.substring(cellKey.indexOf("_") + 1));
        GeneratorWorkspace workspace = (GeneratorWorkspace) app.getWorkspaceComponent();

        Pane mousedOverPane = workspace.getTACellPane(data.getCellKey(column, row));
        mousedOverPane.getStyleClass().clear();
        mousedOverPane.getStyleClass().add(CLASS_OFFICE_HOURS_GRID_TA_CELL_PANE);

        // THE MOUSED OVER COLUMN HEADER
        Pane headerPane = workspace.getOfficeHoursGridDayHeaderPanes().get(data.getCellKey(column, 0));
        headerPane.getStyleClass().remove(CLASS_HIGHLIGHTED_GRID_ROW_OR_COLUMN);

        // THE MOUSED OVER ROW HEADERS
        headerPane = workspace.getOfficeHoursGridTimeCellPanes().get(data.getCellKey(0, row));
        headerPane.getStyleClass().remove(CLASS_HIGHLIGHTED_GRID_ROW_OR_COLUMN);
        headerPane = workspace.getOfficeHoursGridTimeCellPanes().get(data.getCellKey(1, row));
        headerPane.getStyleClass().remove(CLASS_HIGHLIGHTED_GRID_ROW_OR_COLUMN);

        // AND NOW UPDATE ALL THE CELLS IN THE SAME ROW TO THE LEFT
        for (int i = 2; i < column; i++) {
            cellKey = data.getCellKey(i, row);
            Pane cell = workspace.getTACellPane(cellKey);
            cell.getStyleClass().remove(CLASS_HIGHLIGHTED_GRID_ROW_OR_COLUMN);
            cell.getStyleClass().add(CLASS_OFFICE_HOURS_GRID_TA_CELL_PANE);
        }

        // AND THE CELLS IN THE SAME COLUMN ABOVE
        for (int i = 1; i < row; i++) {
            cellKey = data.getCellKey(column, i);
            Pane cell = workspace.getTACellPane(cellKey);
            cell.getStyleClass().remove(CLASS_HIGHLIGHTED_GRID_ROW_OR_COLUMN);
            cell.getStyleClass().add(CLASS_OFFICE_HOURS_GRID_TA_CELL_PANE);
        }
    }

    void handleGridCellMouseEntered(Pane pane) {
        String cellKey = pane.getId();
        CourseSiteData data = (CourseSiteData) app.getDataComponent();
        int column = Integer.parseInt(cellKey.substring(0, cellKey.indexOf("_")));
        int row = Integer.parseInt(cellKey.substring(cellKey.indexOf("_") + 1));
        GeneratorWorkspace workspace = (GeneratorWorkspace) app.getWorkspaceComponent();

        // THE MOUSED OVER PANE
        Pane mousedOverPane = workspace.getTACellPane(data.getCellKey(column, row));
        mousedOverPane.getStyleClass().clear();
        mousedOverPane.getStyleClass().add(CLASS_HIGHLIGHTED_GRID_CELL);

        // THE MOUSED OVER COLUMN HEADER
        Pane headerPane = workspace.getOfficeHoursGridDayHeaderPanes().get(data.getCellKey(column, 0));
        headerPane.getStyleClass().add(CLASS_HIGHLIGHTED_GRID_ROW_OR_COLUMN);

        // THE MOUSED OVER ROW HEADERS
        headerPane = workspace.getOfficeHoursGridTimeCellPanes().get(data.getCellKey(0, row));
        headerPane.getStyleClass().add(CLASS_HIGHLIGHTED_GRID_ROW_OR_COLUMN);
        headerPane = workspace.getOfficeHoursGridTimeCellPanes().get(data.getCellKey(1, row));
        headerPane.getStyleClass().add(CLASS_HIGHLIGHTED_GRID_ROW_OR_COLUMN);

        // AND NOW UPDATE ALL THE CELLS IN THE SAME ROW TO THE LEFT
        for (int i = 2; i < column; i++) {
            cellKey = data.getCellKey(i, row);
            Pane cell = workspace.getTACellPane(cellKey);
            cell.getStyleClass().add(CLASS_HIGHLIGHTED_GRID_ROW_OR_COLUMN);
        }

        // AND THE CELLS IN THE SAME COLUMN ABOVE
        for (int i = 1; i < row; i++) {
            cellKey = data.getCellKey(column, i);
            Pane cell = workspace.getTACellPane(cellKey);
            cell.getStyleClass().add(CLASS_HIGHLIGHTED_GRID_ROW_OR_COLUMN);
        }
    }

    /**
     *
     */
    public void handleDeleteTA() {
        // GET THE TABLE
        GeneratorWorkspace workspace = (GeneratorWorkspace) app.getWorkspaceComponent();
        TableView taTable = workspace.getTaTable();

        // IS A TA SELECTED IN THE TABLE?
        Object selectedItem = taTable.getSelectionModel().getSelectedItem();
        if (selectedItem != null) {
            // GET THE TA AND REMOVE IT
            TeachingAssistant ta = (TeachingAssistant) selectedItem;
            String taName = ta.getName();
            String email = ta.getEmail();
            CourseSiteData data = (CourseSiteData) app.getDataComponent();
            //data.removeTA(taName);
            int numRow = data.getNumRows();
            int[][] cont = new int[8][numRow + 1];
            String[][] infor = new String[8][numRow + 1];
            for (int i = 0; i < 8; i++) {
                for (int j = 0; j <= numRow; j++) {
                    infor[i][j] = null;
                    cont[i][j] = 0;
                }
            }
            String a;
            for (int row = 1; row < numRow; row++) {
                for (int col = 2; col < 7; col++) {
                    StringProperty prop = data.getCellTextProperty(col, row);
                    String cellText = prop.getValue();
                    if (cellText.contains(taName)) {
                        cont[col][row] = 1;
                        a = prop.getValue();
                        infor[col][row] = prop.getValue();
                    }
                }
            }
            HashMap<String, StringProperty> officeHours = data.getOfficeHours();
            // AND BE SURE TO REMOVE ALL THE TA'S OFFICE HOURS
            HashMap<String, Label> labels = workspace.getOfficeHoursGridTACellLabels();

            /**
             * for (Label label : labels.values()) { if
             * (label.getText().equals(taName) ||
             * (label.getText().contains(taName + "\n")) ||
             * (label.getText().contains("\n" + taName))) {
             * data.removeTAFromCell(label.textProperty(), taName); } }*
             */
            // WE'VE CHANGED STUFF
            markWorkAsEdited();
            workspace.resetAddBox();

            ObservableList<Recitation> editedRecitation = FXCollections.observableArrayList();

            ObservableList<Recitation> recitations = data.getRecitations();
            for (int i = 0; i < recitations.size(); i++) {
                if (recitations.get(i).getTa1().equals(taName)) {
                    recitations.get(i).setTA1("none");
                    editedRecitation.add(recitations.get(i));
                } else if (recitations.get(i).getTa2().equals(taName)) {
                    recitations.get(i).setTA2("none");
                    editedRecitation.add(recitations.get(i));
                }
            }
            Delete_TA_Transaction tran = new Delete_TA_Transaction(taName, email, ta.getUnderGrad(), officeHours, data, workspace, editedRecitation);
            jtps.addTransaction(tran);
            workspace.reloadRecitationPane(data);
        }
    }

    public void handleEditTA(String oldTAName) {
        GeneratorWorkspace workspace = (GeneratorWorkspace) app.getWorkspaceComponent();
        TableView taTable = workspace.getTaTable();
        Object selectedItem = taTable.getSelectionModel().getSelectedItem();
        TeachingAssistant ta = (TeachingAssistant) selectedItem;

        TextField nameTextField = workspace.getNameTextField();
        TextField emailTextField = workspace.getEmailTextField();
        String name = nameTextField.getText();
        String email = emailTextField.getText();
        Boolean underGrad = (workspace.getUnderGradTABox()).isSelected();

        TeachingAssistant newTA = new TeachingAssistant(name, email);
        newTA.setUnderGrad(underGrad);
        // WE'LL NEED TO ASK THE DATA SOME QUESTIONS TOO
        CourseSiteData data = (CourseSiteData) app.getDataComponent();

        // WE'LL NEED THIS IN CASE WE NEED TO DISPLAY ANY ERROR MESSAGES
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        if (!validate(email)) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(EMAIL_ADDRESS_ERROR), props.getProperty(EMAIL_ADDRESS_ERROR));
        } else {
            // ADD THE NEW TA TO THE DATA
            data.removeTA(oldTAName);
            data.addTA(name, email, underGrad);
            if (!ta.getName().equals(newTA.getName())) {
                HashMap<String, Label> labels = workspace.getOfficeHoursGridTACellLabels();
                for (Label label : labels.values()) {
                    if (label.getText().equals(ta.getName())
                            || (label.getText().contains(ta.getName() + "\n"))
                            || (label.getText().contains("\n" + ta.getName()))) {
                        data.editTAFromCell(label.textProperty(), ta.getName(), newTA.getName());
                    }
                }
            }
            // CLEAR THE TEXT FIELDS
            ObservableList<Recitation> editedRecitation = FXCollections.observableArrayList();

            ObservableList<Recitation> recitations = data.getRecitations();
            for (int i = 0; i < recitations.size(); i++) {
                if (recitations.get(i).getTa1().equals(oldTAName)) {
                    recitations.get(i).setTA1(name);
                    editedRecitation.add(recitations.get(i));
                } else if (recitations.get(i).getTa2().equals(oldTAName)) {
                    recitations.get(i).setTA2(name);
                    editedRecitation.add(recitations.get(i));
                }
            }
            Edit_TA_Transaction tran = new Edit_TA_Transaction(ta, newTA, data, workspace, editedRecitation);
            jtps.addTransaction(tran);
            nameTextField.setText("");
            emailTextField.setText("");

            // WE'VE CHANGED STUFF
            markWorkAsEdited();
            workspace.reloadRecitationPane(data);

        }
    }

    public String loadTAToTextField() {
        GeneratorWorkspace workspace = (GeneratorWorkspace) app.getWorkspaceComponent();
        CourseSiteData data = (CourseSiteData) app.getDataComponent();
        PropertiesManager props = PropertiesManager.getPropertiesManager();

        TableView taTable = (workspace.getTaTable());
        Object selectedItem = taTable.getSelectionModel().getSelectedItem();
        if (selectedItem != null) {
            TeachingAssistant ta = (TeachingAssistant) selectedItem;
            (workspace.getUnderGradTABox()).setSelected(ta.getUnderGrad());
            (workspace.getNameTextField()).setText(ta.getName());
            (workspace.getEmailTextField()).setText(ta.getEmail());

            return ta.getName();
        }
        return null;
    }

    public void handleChangedStartHour(String soldTime, String snewTime) {
        GeneratorWorkspace workspace = (GeneratorWorkspace) app.getWorkspaceComponent();
        CourseSiteData data = (CourseSiteData) app.getDataComponent();
        ArrayList<TimeSlot> offHours = TimeSlot.buildOfficeHoursList(data);
        boolean confirm = false;
        if (!confirm) {
            for (TimeSlot t : offHours) {
                int time = 0;
                char a = t.getTime().charAt(1);
                if (!Character.isDigit(a)) {
                    time = Integer.parseInt(t.getTime().substring(0, 1));
                } else {
                    time = Integer.parseInt(t.getTime().substring(0, 2));
                }
                if (t.getTime().contains("pm")) {
                    if (time != 12) {
                        time += 12;
                    }
                }
                if (!confirm && time < Integer.parseInt(snewTime) && !t.getName().equals("")) {
                    PropertiesManager props = PropertiesManager.getPropertiesManager();
                    AppYesNoCancelDialogSingleton dialog = AppYesNoCancelDialogSingleton.getSingleton();
                    dialog.show(props.getProperty(CHANGE_TIME_TITLE), props.getProperty(CHANGE_TIME_MESSAGE));
                    String selection = dialog.getSelection();
                    if ((selection.equals(AppYesNoCancelDialogSingleton.NO) || (selection.equals(AppYesNoCancelDialogSingleton.CANCEL)))) {
                        return;
                    }
                    confirm = true;
                }
            }
        }
        if (Integer.parseInt(snewTime) < data.getEndHour()) {
            workspace.officeHoursGridPane.getChildren().clear();
            data.setStartHour(Integer.parseInt(snewTime));
            data.initHours(snewTime, String.valueOf(data.getEndHour()));
            for (TimeSlot ts : offHours) {
                String day = ts.getDay();
                String time1 = ts.getTime();
                String name = ts.getName();
                int time = 0;
                char a = ts.getTime().charAt(1);
                if (!Character.isDigit(a)) {
                    time = Integer.parseInt(ts.getTime().substring(0, 1));
                } else {
                    time = Integer.parseInt(ts.getTime().substring(0, 2));
                }
                if (ts.getTime().contains("pm")) {
                    if (time != 12) {
                        time += 12;
                    }
                }
                if (time >= Integer.parseInt(snewTime)) {
                    data.addOfficeHoursReservation(day, time1, name);
                }
            }
            markWorkAsEdited();

        } else {
            PropertiesManager props = PropertiesManager.getPropertiesManager();
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(START_HOUR_TITLE), props.getProperty(START_HOUR_ERROR));
        }
    }

    public void handleChangedStartHourP(String soldTime, String snewTime) {
        Change_Office_Start_Hour_Transaction tran = new Change_Office_Start_Hour_Transaction(soldTime, snewTime, this);
        jtps.addTransaction(tran);
    }

    public void handleChangedEndHourP(String soldTime, String snewTime) {
        Change_Office_End_Hour_Transaction tran = new Change_Office_End_Hour_Transaction(snewTime, soldTime, this);
        jtps.addTransaction(tran);
    }

    public void handleChangedEndHour(String soldTime, String snewTime) {
        GeneratorWorkspace workspace = (GeneratorWorkspace) app.getWorkspaceComponent();
        CourseSiteData data = (CourseSiteData) app.getDataComponent();
        ArrayList<TimeSlot> offHours = TimeSlot.buildOfficeHoursList(data);
        boolean confirm = false;
        if (!confirm) {
            for (TimeSlot t : offHours) {
                int time = 0;
                char a = t.getTime().charAt(1);
                if (!Character.isDigit(a)) {
                    time = Integer.parseInt(t.getTime().substring(0, 1));
                } else {
                    time = Integer.parseInt(t.getTime().substring(0, 2));
                }
                if (t.getTime().contains("pm")) {
                    if (time != 12) {
                        time += 12;
                    }
                }
                if (!confirm && time >= Integer.parseInt(snewTime) && !t.getName().equals("")) {
                    PropertiesManager props = PropertiesManager.getPropertiesManager();
                    AppYesNoCancelDialogSingleton dialog = AppYesNoCancelDialogSingleton.getSingleton();
                    dialog.show(props.getProperty(CHANGE_TIME_TITLE), props.getProperty(CHANGE_TIME_MESSAGE));
                    String selection = dialog.getSelection();
                    if ((selection.equals(AppYesNoCancelDialogSingleton.NO) || (selection.equals(AppYesNoCancelDialogSingleton.CANCEL)))) {
                        return;
                    }
                    confirm = true;
                }
            }
        }
        if (data.getStartHour() < Integer.parseInt(snewTime)) {
            workspace.officeHoursGridPane.getChildren().clear();
            data.setEndHour(Integer.parseInt(snewTime));
            data.initHours(String.valueOf(data.getStartHour()), snewTime);
            for (TimeSlot ts : offHours) {
                String day = ts.getDay();
                String time1 = ts.getTime();
                String name = ts.getName();
                int time = 0;
                char a = ts.getTime().charAt(1);
                if (!Character.isDigit(a)) {
                    time = Integer.parseInt(ts.getTime().substring(0, 1));
                } else {
                    time = Integer.parseInt(ts.getTime().substring(0, 2));
                }
                if (ts.getTime().contains("pm")) {
                    if (time != 12) {
                        time += 12;
                    }
                }
                if (time < Integer.parseInt(snewTime)) {
                    data.addOfficeHoursReservation(day, time1, name);
                }
            }
            //End_Hour_Transaction trans = new End_Hour_Transaction(soldTime, snewTime, offHours, this);
            //addTran(trans);
            markWorkAsEdited();
        } else {
            PropertiesManager props = PropertiesManager.getPropertiesManager();
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(START_HOUR_TITLE), props.getProperty(START_HOUR_ERROR));
        }
    }

    public void handleAddRecitation() {
        GeneratorWorkspace workspace = (GeneratorWorkspace) app.getWorkspaceComponent();
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        CourseSiteData data = (CourseSiteData) app.getDataComponent();

        TextField recitationSectionTextField = workspace.getRecitationSectionTextField();
        TextField recitationInstructorTextField = workspace.getRecitationInstructorTextField();
        TextField recitationDayTimeTextField = workspace.getRecitationDayTimeTextField();
        TextField recitationLocationTextField = workspace.getRecitationLocationTextField();
        ComboBox ta1Box = workspace.getRecitationSupervisingTA1ComboBox();
        ComboBox ta2Box = workspace.getRecitationSupervisingTA2ComboBox();

        String section = recitationSectionTextField.getText();
        String instructor = recitationInstructorTextField.getText();
        String dayTime = recitationDayTimeTextField.getText();
        String location = recitationLocationTextField.getText();
        String ta1 = (String) ta1Box.getValue();
        String ta2 = (String) ta2Box.getValue();
        if (data.containRecitation(section)) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(RECITATION_ALREADY_EXIST_TITLE), props.getProperty(RECITATION_ALREADY_EXIST_MESSAGE));
        } else if (section.isEmpty()) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(MISSING_RECITATION_SECTION_TITLE), props.getProperty(MISSING_RECITATION_SECTION_MESSAGE));
        } else if (instructor.isEmpty()) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(MISSING_RECITATION_INSTRUCTOR_TITLE), props.getProperty(MISSING_RECITATION_INSTRUCTOR_MESSAGE));
        } else if (dayTime.isEmpty()) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(MISSING_RECITATION_DAYTIME_TITLE), props.getProperty(MISSING_RECITATION_DAYTIME_MESSAGE));
        } else if (location.isEmpty()) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(MISSING_RECITATION_LOCATION_TITLE), props.getProperty(MISSING_RECITATION_LOCATION_MESSAGE));
        } else {
            data.addRecitation(section, instructor, dayTime, location, ta1, ta2);
            Add_Recitation_Transaction tran = new Add_Recitation_Transaction(section, instructor, dayTime, location, ta1, ta2, data);
            jtps.addTransaction(tran);
            markWorkAsEdited();
            recitationSectionTextField.setText("");
            recitationInstructorTextField.setText("");
            recitationDayTimeTextField.setText("");
            recitationLocationTextField.setText("");
        }
    }

    public void handleDeleteRecitation() {
        GeneratorWorkspace workspace = (GeneratorWorkspace) app.getWorkspaceComponent();
        TableView recitationTable = workspace.getRecitationTable();

        CourseSiteData data = (CourseSiteData) app.getDataComponent();
        // IS A TA SELECTED IN THE TABLE?
        Object selectedItem = recitationTable.getSelectionModel().getSelectedItem();

        if (selectedItem != null) {
            Recitation re = (Recitation) selectedItem;
            String section = re.getSection();
            data.removeRecitation(section);
            Delete_Recitation_Transaction tran = new Delete_Recitation_Transaction(re, data);
            jtps.addTransaction(tran);
            (workspace.getRecitationSectionTextField()).clear();
            (workspace.getRecitationInstructorTextField()).clear();
            (workspace.getRecitationDayTimeTextField()).clear();
            (workspace.getRecitationLocationTextField()).clear();
            (workspace.getRecitationSupervisingTA1ComboBox()).setValue("");
            (workspace.getRecitationSupervisingTA2ComboBox()).setValue("");
            (workspace.getRecitationTable()).getSelectionModel().select(null);
            markWorkAsEdited();
        }
    }

    public String loadRecitationToTextField() {
        GeneratorWorkspace workspace = (GeneratorWorkspace) app.getWorkspaceComponent();
        TableView recitationTable = workspace.getRecitationTable();

        CourseSiteData data = (CourseSiteData) app.getDataComponent();

        Object selectedItem = recitationTable.getSelectionModel().getSelectedItem();

        if (selectedItem != null) {
            Recitation re = (Recitation) selectedItem;
            String section = re.getSection();
            String instructor = re.getInstructor();
            String dayTime = re.getDayTime();
            String location = re.getLocation();
            String ta1 = re.getTa1();
            String ta2 = re.getTa2();
            (workspace.getRecitationSectionTextField()).setText(section);
            (workspace.getRecitationDayTimeTextField()).setText(dayTime);
            (workspace.getRecitationInstructorTextField()).setText(instructor);
            (workspace.getRecitationLocationTextField()).setText(location);
            (workspace.getRecitationSupervisingTA1ComboBox()).setValue(ta1);
            (workspace.getRecitationSupervisingTA2ComboBox()).setValue(ta2);
            return section;
        } else {
            return "";
        }
    }

    public void handleEditRecitation(String oldRecitationSection) {
        GeneratorWorkspace workspace = (GeneratorWorkspace) app.getWorkspaceComponent();
        TableView recitationTable = workspace.getRecitationTable();

        CourseSiteData data = (CourseSiteData) app.getDataComponent();

        Object selectedItem = recitationTable.getSelectionModel().getSelectedItem();
        Recitation re = (Recitation) selectedItem;

        String newSection = (workspace.getRecitationSectionTextField()).getText();
        String newInstructor = (workspace.getRecitationInstructorTextField()).getText();
        String newDayTime = (workspace.getRecitationDayTimeTextField()).getText();
        String newLocation = (workspace.getRecitationLocationTextField()).getText();
        String newTA1 = (String) (workspace.getRecitationSupervisingTA1ComboBox()).getValue();
        String newTA2 = (String) (workspace.getRecitationSupervisingTA2ComboBox()).getValue();
        data.removeRecitation(oldRecitationSection);
        data.addRecitation(newSection, newInstructor, newDayTime, newLocation, newTA1, newTA2);
        Recitation newR = new Recitation(newSection, newInstructor, newDayTime, newLocation, newTA1, newTA2);
        Edit_Recitation_Transaction tran = new Edit_Recitation_Transaction(re, newR, data);
        jtps.addTransaction(tran);
        (workspace.getRecitationSectionTextField()).clear();
        (workspace.getRecitationDayTimeTextField()).clear();
        (workspace.getRecitationInstructorTextField()).clear();
        (workspace.getRecitationLocationTextField()).clear();
        (workspace.getRecitationSupervisingTA1ComboBox()).setValue("");
        (workspace.getRecitationSupervisingTA2ComboBox()).setValue("");

        (workspace.getRecitationTable()).getSelectionModel().select(null);
        markWorkAsEdited();
    }

    public void handleChangedStartingMondayDate() {
        GeneratorWorkspace workspace = (GeneratorWorkspace) app.getWorkspaceComponent();
        CourseSiteData data = (CourseSiteData) app.getDataComponent();
        PropertiesManager props = PropertiesManager.getPropertiesManager();

        DatePicker startingMondayDatePicker = workspace.getStartingMondayDatePicker();
        LocalDate startingMonday = startingMondayDatePicker.getValue();
        int month = startingMonday.getMonthValue();
        int day = startingMonday.getDayOfMonth();
        int year = startingMonday.getYear();
        if (month > data.getEndingFridayMonth() || year > ((workspace.getEndingFridayDatePicker()).getValue()).getYear()) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(STARTING_MONDAY_ERROR_TITLE), props.getProperty(STARTING_MONDAY_ERROR_MESSAGE));
            workspace.getStartingMondayDatePicker().setValue(LocalDate.of(2017, data.getStartingMondayMonth(), data.getStartingMondayDay()));
        } else if (month == data.getEndingFridayMonth() && day > data.getEndingFridayDay()) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(STARTING_MONDAY_ERROR_TITLE), props.getProperty(STARTING_MONDAY_ERROR_MESSAGE));
            workspace.getStartingMondayDatePicker().setValue(LocalDate.of(2017, data.getStartingMondayMonth(), data.getStartingMondayDay()));

        } else {
            data.setStartingMondayMonth(startingMonday.getMonthValue());
            data.setStartingMondayDay(startingMonday.getDayOfMonth());
            markWorkAsEdited();
        }
    }

    public void handleChangeEndingFridayDate() {
        GeneratorWorkspace workspace = (GeneratorWorkspace) app.getWorkspaceComponent();
        CourseSiteData data = (CourseSiteData) app.getDataComponent();
        PropertiesManager props = PropertiesManager.getPropertiesManager();

        DatePicker endingFridayDatePicker = workspace.getEndingFridayDatePicker();
        LocalDate endingFriday = endingFridayDatePicker.getValue();
        int month = endingFriday.getMonthValue();
        int day = endingFriday.getDayOfMonth();
        int year = endingFriday.getYear();
        if (month < data.getStartingMondayMonth() || year < ((workspace.getStartingMondayDatePicker()).getValue()).getYear()) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(STARTING_MONDAY_ERROR_TITLE), props.getProperty(STARTING_MONDAY_ERROR_MESSAGE));
            workspace.getEndingFridayDatePicker().setValue(LocalDate.of(2017, data.getEndingFridayMonth(), data.getEndingFridayDay()));
        } else if (month == data.getStartingMondayMonth() && day < data.getEndingFridayDay()) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(STARTING_MONDAY_ERROR_TITLE), props.getProperty(STARTING_MONDAY_ERROR_MESSAGE));
            workspace.getEndingFridayDatePicker().setValue(LocalDate.of(2017, data.getEndingFridayMonth(), data.getEndingFridayDay()));

        } else {
            data.setEndingFridayDay(endingFriday.getDayOfMonth());
            data.setEndingFridayMonth(endingFriday.getMonthValue());
            markWorkAsEdited();
        }
    }

    public void handleEditScheduleIteam(String oldScheduleTitle) {
        GeneratorWorkspace workspace = (GeneratorWorkspace) app.getWorkspaceComponent();
        CourseSiteData data = (CourseSiteData) app.getDataComponent();
        PropertiesManager props = PropertiesManager.getPropertiesManager();

        TableView scheduleTable = workspace.getScheduleTable();
        Object selectedItem = scheduleTable.getSelectionModel().getSelectedItem();
        ScheduleItem si = (ScheduleItem) selectedItem;

        String type = (String) (workspace.getScheduleTypeBox()).getValue();
        DatePicker scheduleDatePicker = workspace.getScheduleDatePicker();
        LocalDate date = scheduleDatePicker.getValue();
        String scheduleDate = date.toString();
        String time = (workspace.getScheduleTimeTextField()).getText();
        String title = (workspace.getScheduleTitleTextField()).getText();
        String topic = (workspace.getScheduleTopicTextField()).getText();
        String link = (workspace.getScheduleLinkTextField()).getText();
        String criteria = (workspace.getScheduleCriteriaTextField()).getText();
        markWorkAsEdited();
        data.removeScheduleItem(oldScheduleTitle, si.getDate());
        if (data.containScheduleItem(title, scheduleDate)) {
            AppYesNoCancelDialogSingleton dialog = AppYesNoCancelDialogSingleton.getSingleton();
            dialog.show(props.getProperty(SCHEDULE_ITEM_ALREADY_EXIST_TITLE), props.getProperty(SCHEDULE_ITEM_ALREADY_EXIST_MESSAGE));
        } else if (type == "Holiday") {
            if (!topic.isEmpty() || !time.isEmpty() || !criteria.isEmpty() || scheduleDate.isEmpty() || title.isEmpty() || link.isEmpty()) {
                AppYesNoCancelDialogSingleton dialog = AppYesNoCancelDialogSingleton.getSingleton();
                dialog.show(props.getProperty(ADD_HOLIDAY_ERROR_TITLE), props.getProperty(ADD_HOLIDAY_ERROR_MESSAGE));
            } else {
                data.removeScheduleItem(oldScheduleTitle, si.getDate());
                data.addHoliday(scheduleDate, title, link);
                Holiday holi = new Holiday("Holiday", scheduleDate, title, "");
                holi.setLink(link);
                Edit_Schedule_Transaction tran = new Edit_Schedule_Transaction(si, holi, data);
                jtps.addTransaction(tran);
                workspace.resetAddSchedule();
            }
        } else if (type == "Lecture") {
            if (scheduleDate.isEmpty() || title.isEmpty() || topic.isEmpty() || link.isEmpty() || !time.isEmpty() || !criteria.isEmpty()) {
                AppYesNoCancelDialogSingleton dialog = AppYesNoCancelDialogSingleton.getSingleton();
                dialog.show(props.getProperty(ADD_LECTURE_ERROR_TITLE), props.getProperty(ADD_LECTURE_ERROR_MESSAGE));
            } else {
                data.removeScheduleItem(oldScheduleTitle, si.getDate());
                data.addLecture(scheduleDate, title, topic, link);
                Lecture lecture = new Lecture("Lecture", scheduleDate, title, topic);
                lecture.setLink(link);
                Edit_Schedule_Transaction tran = new Edit_Schedule_Transaction(si, lecture, data);
                jtps.addTransaction(tran);
                workspace.resetAddSchedule();

            }
        } else if (type == "Reference") {
            if (scheduleDate.isEmpty() || title.isEmpty() || topic.isEmpty() || link.isEmpty() || !time.isEmpty() || !criteria.isEmpty()) {
                AppYesNoCancelDialogSingleton dialog = AppYesNoCancelDialogSingleton.getSingleton();
                dialog.show(props.getProperty(ADD_REFERENCE_ERROR_TITLE), props.getProperty(ADD_REFERENCE_ERROR_MESSAGE));
            } else {
                data.removeScheduleItem(oldScheduleTitle, si.getDate());
                data.addReference(scheduleDate, title, topic, link);
                Reference ref = new Reference("Reference", scheduleDate, title, topic);
                ref.setLink(link);
                Edit_Schedule_Transaction tran = new Edit_Schedule_Transaction(si, ref, data);
                jtps.addTransaction(tran);
                workspace.resetAddSchedule();

            }
        } else if (type == "Recitation") {
            if (scheduleDate.isEmpty() || title.isEmpty() || topic.isEmpty() || !link.isEmpty() || !time.isEmpty() || !criteria.isEmpty()) {
                AppYesNoCancelDialogSingleton dialog = AppYesNoCancelDialogSingleton.getSingleton();
                dialog.show(props.getProperty(ADD_RECITATION_SCHEDULE_ERROR_TITLE), props.getProperty(ADD_RECITATION_SCHEDULE_ERROR_MESSAGE));
            } else {
                data.removeScheduleItem(oldScheduleTitle, si.getDate());
                data.addRecitationSchedule(scheduleDate, title, topic);
                workspace.resetAddSchedule();
                RecitationsSchedule rec = new RecitationsSchedule("Recitation", scheduleDate, title, topic);
                Edit_Schedule_Transaction tran = new Edit_Schedule_Transaction(si, rec, data);
                jtps.addTransaction(tran);
            }
        } else if (type == "HW") {
            if (scheduleDate.isEmpty() || title.isEmpty() || topic.isEmpty() || !link.isEmpty() || !time.isEmpty() || !criteria.isEmpty()) {
                AppYesNoCancelDialogSingleton dialog = AppYesNoCancelDialogSingleton.getSingleton();
                dialog.show(props.getProperty(ADD_HW_ERROR_TITLE), props.getProperty(ADD_HW_ERROR_MESSAGE));
            } else {
                data.removeScheduleItem(oldScheduleTitle, si.getDate());
                data.addHW(scheduleDate, title, topic, link, time, criteria);
                workspace.resetAddSchedule();
                HWs hw = new HWs("HW", scheduleDate, title, topic);
                hw.setLink(link);
                hw.setTime(time);
                hw.setCriteria(criteria);
                Edit_Schedule_Transaction tran = new Edit_Schedule_Transaction(si, hw, data);
                jtps.addTransaction(tran);
            }
        } else {
            AppYesNoCancelDialogSingleton dialog = AppYesNoCancelDialogSingleton.getSingleton();
            dialog.show(props.getProperty(SCHEDULE_TYPE_ERROR_TITLE), props.getProperty(SCHEDULE_TYPE_ERROR_MESSAGE));
        }

    }

    public void handleAddSchedule() {
        GeneratorWorkspace workspace = (GeneratorWorkspace) app.getWorkspaceComponent();
        CourseSiteData data = (CourseSiteData) app.getDataComponent();
        PropertiesManager props = PropertiesManager.getPropertiesManager();

        String type = (String) (workspace.getScheduleTypeBox()).getValue();
        DatePicker scheduleDatePicker = workspace.getScheduleDatePicker();
        LocalDate date = scheduleDatePicker.getValue();
        String scheduleDate = date.toString();
        String time = (workspace.getScheduleTimeTextField()).getText();
        String title = (workspace.getScheduleTitleTextField()).getText();
        String topic = (workspace.getScheduleTopicTextField()).getText();
        String link = (workspace.getScheduleLinkTextField()).getText();
        String criteria = (workspace.getScheduleCriteriaTextField()).getText();
        markWorkAsEdited();
        if (data.containScheduleItem(title, scheduleDate)) {
            AppYesNoCancelDialogSingleton dialog = AppYesNoCancelDialogSingleton.getSingleton();
            dialog.show(props.getProperty(SCHEDULE_ITEM_ALREADY_EXIST_TITLE), props.getProperty(SCHEDULE_ITEM_ALREADY_EXIST_MESSAGE));
        } else if (type == "Holiday") {
            if (!topic.isEmpty() || !time.isEmpty() || !criteria.isEmpty() || scheduleDate.isEmpty() || title.isEmpty() || link.isEmpty()) {
                AppYesNoCancelDialogSingleton dialog = AppYesNoCancelDialogSingleton.getSingleton();
                dialog.show(props.getProperty(ADD_HOLIDAY_ERROR_TITLE), props.getProperty(ADD_HOLIDAY_ERROR_MESSAGE));
            } else {
                data.addHoliday(scheduleDate, title, link);
                Holiday holi = new Holiday("Holiday", scheduleDate, title, "");
                holi.setLink(link);
                Add_Schedule_Transaction tran = new Add_Schedule_Transaction(holi, data);
                jtps.addTransaction(tran);
                workspace.resetAddSchedule();
            }
        } else if (type == "Lecture") {
            if (scheduleDate.isEmpty() || title.isEmpty() || topic.isEmpty() || link.isEmpty() || !time.isEmpty() || !criteria.isEmpty()) {
                AppYesNoCancelDialogSingleton dialog = AppYesNoCancelDialogSingleton.getSingleton();
                dialog.show(props.getProperty(ADD_LECTURE_ERROR_TITLE), props.getProperty(ADD_LECTURE_ERROR_MESSAGE));
            } else {
                data.addLecture(scheduleDate, title, topic, link);
                Lecture lec = new Lecture("Lecture", scheduleDate, title, topic);
                lec.setLink(link);
                Add_Schedule_Transaction tran = new Add_Schedule_Transaction(lec, data);
                jtps.addTransaction(tran);
                workspace.resetAddSchedule();
            }
        } else if (type == "Reference") {
            if (scheduleDate.isEmpty() || title.isEmpty() || topic.isEmpty() || link.isEmpty() || !time.isEmpty() || !criteria.isEmpty()) {
                AppYesNoCancelDialogSingleton dialog = AppYesNoCancelDialogSingleton.getSingleton();
                dialog.show(props.getProperty(ADD_REFERENCE_ERROR_TITLE), props.getProperty(ADD_REFERENCE_ERROR_MESSAGE));
            } else {
                data.addReference(scheduleDate, title, topic, link);
                Reference ref = new Reference("Reference", scheduleDate, title, topic);
                ref.setLink(link);
                Add_Schedule_Transaction tran = new Add_Schedule_Transaction(ref, data);
                jtps.addTransaction(tran);
                workspace.resetAddSchedule();
            }
        } else if (type == "Recitation") {
            if (scheduleDate.isEmpty() || title.isEmpty() || topic.isEmpty() || !link.isEmpty() || !time.isEmpty() || !criteria.isEmpty()) {
                AppYesNoCancelDialogSingleton dialog = AppYesNoCancelDialogSingleton.getSingleton();
                dialog.show(props.getProperty(ADD_RECITATION_SCHEDULE_ERROR_TITLE), props.getProperty(ADD_RECITATION_SCHEDULE_ERROR_MESSAGE));
            } else {
                data.addRecitationSchedule(scheduleDate, title, topic);
                RecitationsSchedule reciS = new RecitationsSchedule("Recitation", scheduleDate, title, topic);
                Add_Schedule_Transaction tran = new Add_Schedule_Transaction(reciS, data);
                jtps.addTransaction(tran);
                workspace.resetAddSchedule();
            }
        } else if (type == "HW") {
            if (scheduleDate.isEmpty() || title.isEmpty() || topic.isEmpty() || link.isEmpty() || time.isEmpty() || criteria.isEmpty()) {
                AppYesNoCancelDialogSingleton dialog = AppYesNoCancelDialogSingleton.getSingleton();
                dialog.show(props.getProperty(ADD_HW_ERROR_TITLE), props.getProperty(ADD_HW_ERROR_MESSAGE));
            } else {
                data.addHW(scheduleDate, title, topic, link, time, criteria);
                HWs hw = new HWs("HW", scheduleDate, title, topic);
                hw.setLink(link);
                hw.setTime(time);
                hw.setCriteria(criteria);
                Add_Schedule_Transaction tran = new Add_Schedule_Transaction(hw, data);
                jtps.addTransaction(tran);
                workspace.resetAddSchedule();
            }
        } else {
            AppYesNoCancelDialogSingleton dialog = AppYesNoCancelDialogSingleton.getSingleton();
            dialog.show(props.getProperty(SCHEDULE_TYPE_ERROR_TITLE), props.getProperty(SCHEDULE_TYPE_ERROR_MESSAGE));
        }

    }

    public String loadScheduleToTextField() {
        GeneratorWorkspace workspace = (GeneratorWorkspace) app.getWorkspaceComponent();
        CourseSiteData data = (CourseSiteData) app.getDataComponent();

        TableView scheduleTable = workspace.getScheduleTable();
        Object selectedItem = scheduleTable.getSelectionModel().getSelectedItem();
        if (selectedItem != null) {
            ScheduleItem si = (ScheduleItem) selectedItem;
            String type = si.getType();
            String date = si.getDate();
            String title = si.getTitle();
            int month = Integer.parseInt(date.substring(5, 7));
            int day = Integer.parseInt(date.substring(8));
            (workspace.getScheduleTypeBox()).setValue(type);
            (workspace.getScheduleDatePicker()).setValue(LocalDate.of(2017, month, day));
            (workspace.getScheduleTitleTextField()).setText(si.getTitle());
            (workspace.getScheduleTopicTextField()).setText(si.getTopic());
            (workspace.getScheduleTimeTextField()).clear();
            (workspace.getScheduleLinkTextField()).clear();
            (workspace.getScheduleCriteriaTextField()).clear();
            if (type == "Holiday") {
                Holiday holi = data.getHoliday(title, date);
                (workspace.getScheduleLinkTextField()).setText(holi.getLink());
            } else if (type == "Lecture") {
                Lecture lec = data.getLecture(title, date);
                (workspace.getScheduleLinkTextField()).setText(lec.getLink());
            } else if (type == "Reference") {
                Reference ref = data.getReference(title, date);
                (workspace.getScheduleLinkTextField()).setText(ref.getLink());
            } else if (type == "Recitation") {
                RecitationsSchedule rs = data.getRecitationScheduleItem(title, date);
            } else if (type == "HW") {
                HWs hw = data.getHWs(title, date);
                (workspace.getScheduleLinkTextField()).setText(hw.getLink());
                (workspace.getScheduleTimeTextField()).setText(hw.getTime());
                (workspace.getScheduleCriteriaTextField()).setText(hw.getCriteria());
            }
            return title;
        }
        return "";
    }

    public void handleDeleteScheduleItem() {
        GeneratorWorkspace workspace = (GeneratorWorkspace) app.getWorkspaceComponent();
        TableView scheduleTable = workspace.getScheduleTable();

        CourseSiteData data = (CourseSiteData) app.getDataComponent();
        // IS A TA SELECTED IN THE TABLE?
        Object selectedItem = scheduleTable.getSelectionModel().getSelectedItem();

        if (selectedItem != null) {
            ScheduleItem si = (ScheduleItem) selectedItem;
            String title = si.getTitle();
            String date = si.getDate();
            data.removeScheduleItem(title, date);
            Delete_Schedule_Transaction tran = new Delete_Schedule_Transaction(si, data);
            jtps.addTransaction(tran);
            markWorkAsEdited();
        }

    }

    public void handleEditTeam(String oldTeamName) {
        GeneratorWorkspace workspace = (GeneratorWorkspace) app.getWorkspaceComponent();
        CourseSiteData data = (CourseSiteData) app.getDataComponent();
        PropertiesManager props = PropertiesManager.getPropertiesManager();

        TableView teamTable = workspace.getTeamTable();
        Object selectedItem = teamTable.getSelectionModel().getSelectedItem();
        Team oldTeam = (Team) selectedItem;
        String name = (workspace.getTeamNameTextField()).getText();
        String link = (workspace.getTeamLinkTextField()).getText();
        Color color = (workspace.getTeamColorPicker()).getValue();
        String colorString = color.toString();
        Color textColor = (workspace.getTeamTextColorPicker()).getValue();
        String textColorString = textColor.toString();
        data.removeTeam(oldTeamName);
        data.addTeam(name, colorString, textColorString, link);

        changeTeamName(oldTeamName, name);
        Edit_Team_Transaction tran = new Edit_Team_Transaction(name, colorString, textColorString, link, oldTeam, data, workspace, this);
        jtps.addTransaction(tran);
        workspace.resetAddTeam();
        workspace.resetTeamComboBox();
        markWorkAsEdited();
    }

    public void handleAddTeam() {
        GeneratorWorkspace workspace = (GeneratorWorkspace) app.getWorkspaceComponent();
        CourseSiteData data = (CourseSiteData) app.getDataComponent();
        PropertiesManager props = PropertiesManager.getPropertiesManager();

        String name = (workspace.getTeamNameTextField()).getText();
        String link = (workspace.getTeamLinkTextField()).getText();
        Color color = (workspace.getTeamColorPicker()).getValue();
        String colorString = color.toString();
        Color textColor = (workspace.getTeamTextColorPicker()).getValue();
        String textColorString = textColor.toString();
        if (data.containTeam(name) || name.isEmpty() || link.isEmpty()) {
            AppYesNoCancelDialogSingleton dialog = AppYesNoCancelDialogSingleton.getSingleton();
            dialog.show(props.getProperty(ADD_TEAM_ERROR_TITLE), props.getProperty(ADD_TEAM_ERROR_MESSAGE));
        } else {
            data.addTeam(name, colorString, textColorString, link);
            Add_Team_Transaction tran = new Add_Team_Transaction(name, link, colorString, textColorString, data, workspace);
            jtps.addTransaction(tran);
            workspace.resetAddTeam();
            workspace.resetTeamComboBox();
            markWorkAsEdited();
        }

    }

    public String loadTeamToTextField() {
        GeneratorWorkspace workspace = (GeneratorWorkspace) app.getWorkspaceComponent();
        CourseSiteData data = (CourseSiteData) app.getDataComponent();

        TableView teamTable = workspace.getTeamTable();
        Object selectedItem = teamTable.getSelectionModel().getSelectedItem();
        if (selectedItem != null) {
            Team te = (Team) selectedItem;
            workspace.getTeamNameTextField().setText(te.getName());
            workspace.getTeamLinkTextField().setText(te.getLink());
            Color color = Color.web(te.getColor());
            workspace.getTeamColorPicker().setValue(color);
            Color textColor = Color.web(te.getTextColor());
            workspace.getTeamTextColorPicker().setValue(textColor);

            return te.getName();
        } else {
            return null;
        }
    }

    public void handleDeleteTeam() {
        GeneratorWorkspace workspace = (GeneratorWorkspace) app.getWorkspaceComponent();
        CourseSiteData data = (CourseSiteData) app.getDataComponent();

        TableView teamTable = workspace.getTeamTable();
        Object selectedItem = teamTable.getSelectionModel().getSelectedItem();
        if (selectedItem != null) {
            Team te = (Team) selectedItem;
            String name = te.getName();
            data.removeTeam(name);

            markWorkAsEdited();
            workspace.resetAddTeam();
            workspace.resetTeamComboBox();
            ObservableList<Student> students = data.getStudents();
            ObservableList<Student> deletedStudents = FXCollections.observableArrayList();
            for (int i = 0; i < students.size(); i++) {
                if (students.get(i).getTeam() == name) {
                    deletedStudents.add(students.get(i));
                    data.removeStudent(students.get(i).getLastName(), students.get(i).getFirstName());
                    i--;
                }
            }

            Delete_Team_Transaction tran = new Delete_Team_Transaction(deletedStudents, te, data, workspace);
            jtps.addTransaction(tran);
        }

    }

    public void handleEditStudent(String oldStudentFirstName, String oldStudentLastName) {
        GeneratorWorkspace workspace = (GeneratorWorkspace) app.getWorkspaceComponent();
        CourseSiteData data = (CourseSiteData) app.getDataComponent();
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        TableView studentTable = workspace.getStudentTable();
        Object selectedItem = studentTable.getSelectionModel().getSelectedItem();
        Student oldStu = (Student) selectedItem;

        String lastName = (workspace.getStudentLastNameTextField()).getText();
        String firstName = (workspace.getStudentFirstNameTextField()).getText();
        String role = (String) (workspace.getStudentRoleComboBox()).getValue();
        String team = (String) (workspace.getStudentTeamComboBox()).getValue();
        data.removeStudent(oldStudentLastName, oldStudentFirstName);
        data.addStudent(lastName, firstName, team, role);
        Edit_Student_Transaction tran = new Edit_Student_Transaction(oldStu, lastName, firstName, role, team, data);
        jtps.addTransaction(tran);
        markWorkAsEdited();

    }

    public void handleAddStudent() {
        GeneratorWorkspace workspace = (GeneratorWorkspace) app.getWorkspaceComponent();
        CourseSiteData data = (CourseSiteData) app.getDataComponent();
        PropertiesManager props = PropertiesManager.getPropertiesManager();

        String lastName = (workspace.getStudentLastNameTextField()).getText();
        String firstName = (workspace.getStudentFirstNameTextField()).getText();
        String role = (String) (workspace.getStudentRoleComboBox()).getValue();
        String team = (String) (workspace.getStudentTeamComboBox()).getValue();
        if (data.containStudent(lastName, firstName) || lastName.isEmpty() || firstName.isEmpty() || role.isEmpty() || team.isEmpty()) {
            AppYesNoCancelDialogSingleton dialog = AppYesNoCancelDialogSingleton.getSingleton();
            dialog.show(props.getProperty(ADD_STUDENT_ERROR_TITLE), props.getProperty(ADD_STUDENT_ERROR_MESSAGE));

        } else {
            data.addStudent(lastName, firstName, team, role);
            Add_Student_Transaction tran = new Add_Student_Transaction(lastName, firstName, role, team, data);
            jtps.addTransaction(tran);
            workspace.resetAddStudent();
            markWorkAsEdited();
        }

    }

    public String[] loadStudentToTextField() {
        GeneratorWorkspace workspace = (GeneratorWorkspace) app.getWorkspaceComponent();
        CourseSiteData data = (CourseSiteData) app.getDataComponent();
        PropertiesManager props = PropertiesManager.getPropertiesManager();

        TableView studentTable = workspace.getStudentTable();
        Object selectedItem = studentTable.getSelectionModel().getSelectedItem();
        String[] result = new String[2];
        if (selectedItem != null) {
            Student stu = (Student) selectedItem;
            (workspace.getStudentFirstNameTextField()).setText(stu.getFirstName());
            (workspace.getStudentLastNameTextField()).setText(stu.getLastName());
            (workspace.getStudentTeamComboBox()).setValue(stu.getTeam());
            (workspace.getStudentRoleComboBox()).setValue(stu.getRole());

            result[0] = stu.getFirstName();
            result[1] = stu.getLastName();
            return result;
        }
        return result;
    }

    public void handleDeleteStudent() {
        GeneratorWorkspace workspace = (GeneratorWorkspace) app.getWorkspaceComponent();
        CourseSiteData data = (CourseSiteData) app.getDataComponent();
        PropertiesManager props = PropertiesManager.getPropertiesManager();

        TableView studentTable = workspace.getStudentTable();
        Object selectedItem = studentTable.getSelectionModel().getSelectedItem();
        if (selectedItem != null) {
            Student student = (Student) selectedItem;
            String lastName = student.getLastName();
            String firstName = student.getFirstName();
            data.removeStudent(lastName, firstName);
            Delete_Student_Transaction tran = new Delete_Student_Transaction(lastName, firstName, student.getRole(), student.getTeam(), data);
            jtps.addTransaction(tran);
            markWorkAsEdited();
            workspace.resetAddStudent();
        }
    }

    public int convertToMilitaryTime(String time) {
        int milTime = 0;
        if (time == null) {
            PropertiesManager props = PropertiesManager.getPropertiesManager();
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(CHANGE_TIME_TITLE), props.getProperty(CHANGE_TIME_MESSAGE));       //REMEMBER TO CHANGE TO PROPER ERROR MESSAGE                              
        } else if (time.equalsIgnoreCase("12:00pm")) {
            milTime = 12;
        } else {
            int index = time.indexOf(":");
            String subStringTime = time.substring(0, index);
            milTime = Integer.parseInt(subStringTime);
            if (time.contains("p")) {
                milTime += 12;
            }
        }
        return milTime;
    }

    public void changeTeamName(String oldName, String newName) {
        GeneratorWorkspace workspace = (GeneratorWorkspace) app.getWorkspaceComponent();
        CourseSiteData data = (CourseSiteData) app.getDataComponent();

        ObservableList<Student> students = data.getStudents();
        for (Student stu : students) {
            if (stu.getTeam() == oldName) {
                stu.setTeam(newName);
            }
        }
        (workspace.getStudentTable()).refresh();
    }

    void handleChangeSubject() {
        GeneratorWorkspace workspace = (GeneratorWorkspace) app.getWorkspaceComponent();
        CourseSiteData data = (CourseSiteData) app.getDataComponent();

        data.setSubject(workspace.getSubjectTextField().getText());
        markWorkAsEdited();
    }

    void handleChangeNumber() {
        GeneratorWorkspace workspace = (GeneratorWorkspace) app.getWorkspaceComponent();
        CourseSiteData data = (CourseSiteData) app.getDataComponent();

        data.setNumber(workspace.getNumberTextField().getText());
        markWorkAsEdited();
    }

    void handleChangeSemester() {
        GeneratorWorkspace workspace = (GeneratorWorkspace) app.getWorkspaceComponent();
        CourseSiteData data = (CourseSiteData) app.getDataComponent();

        data.setCourseInfoSemester(workspace.getSemesterTextField().getText());
        data.setSemester(workspace.getSemesterTextField().getText());
        markWorkAsEdited();
    }

    void handleChangeTitle() {
        GeneratorWorkspace workspace = (GeneratorWorkspace) app.getWorkspaceComponent();
        CourseSiteData data = (CourseSiteData) app.getDataComponent();

        data.setCourseTitle(workspace.getTitleTextField().getText());
        markWorkAsEdited();
    }

    void handleChangeInstructorName() {
        GeneratorWorkspace workspace = (GeneratorWorkspace) app.getWorkspaceComponent();
        CourseSiteData data = (CourseSiteData) app.getDataComponent();

        data.setCourseInstructorName(workspace.getInstructorNameTextField().getText());
   markWorkAsEdited();
    }

    void handleChangeInstructorHome() {
        GeneratorWorkspace workspace = (GeneratorWorkspace) app.getWorkspaceComponent();
        CourseSiteData data = (CourseSiteData) app.getDataComponent();

        data.setCourseInstructorHome(workspace.getInstructorHomeTextField().getText());
        markWorkAsEdited();
    }

    void handleChangeYear() {
        GeneratorWorkspace workspace = (GeneratorWorkspace) app.getWorkspaceComponent();
        CourseSiteData data = (CourseSiteData) app.getDataComponent();

        data.setYear(workspace.getYearTextField().getText());
        markWorkAsEdited();
    }

    void handleChangeExportDirectory() {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        GeneratorWorkspace workspace = (GeneratorWorkspace) app.getWorkspaceComponent();
        CourseSiteData data = (CourseSiteData) app.getDataComponent();

        DirectoryChooser fc = new DirectoryChooser();
        fc.setInitialDirectory(new File(PATH_WORK));
        fc.setTitle(props.getProperty(""));

        File selectedFile = fc.showDialog(app.getGUI().getWindow());
        String filePath = selectedFile.getPath();
        workspace.setExportDirectory(filePath);
        workspace.resetExportDir();
    }

    void handleChangeBannerSchoolImage() {
        FileChooser fileChooser = new FileChooser();
         CourseSiteData data = (CourseSiteData) app.getDataComponent();

        GeneratorWorkspace workspace = (GeneratorWorkspace) app.getWorkspaceComponent();
        //Set extension filter
        FileChooser.ExtensionFilter extFilterJPG = new FileChooser.ExtensionFilter("JPG files (*.jpg)", "*.JPG");
        FileChooser.ExtensionFilter extFilterPNG = new FileChooser.ExtensionFilter("PNG files (*.png)", "*.PNG");
        fileChooser.getExtensionFilters().addAll(extFilterJPG, extFilterPNG);
        fileChooser.setInitialDirectory(new File(PATH_IMAGES));
        File file = fileChooser.showOpenDialog(null);
        try {
                BufferedImage bufferedImage = ImageIO.read(file);
                Image image = SwingFXUtils.toFXImage(bufferedImage, null);
                String path = file.getAbsolutePath();
                data.setSchoolBannerImage(path);
                workspace.getBannerSchool().setImage(image);
                workspace.getBannerSchool().setFitHeight(40);
                workspace.getBannerSchool().setFitWidth(100);
            } catch (IOException ex) {
                Logger.getLogger(GeneratorController.class.getName()).log(Level.SEVERE, null, ex);
            }

    }

    void handleChangeLeftFooterImage() {
        FileChooser fileChooser = new FileChooser();
        GeneratorWorkspace workspace = (GeneratorWorkspace) app.getWorkspaceComponent();
        //Set extension filter
        FileChooser.ExtensionFilter extFilterJPG = new FileChooser.ExtensionFilter("JPG files (*.jpg)", "*.JPG");
        FileChooser.ExtensionFilter extFilterPNG = new FileChooser.ExtensionFilter("PNG files (*.png)", "*.PNG");
        fileChooser.getExtensionFilters().addAll(extFilterJPG, extFilterPNG);
        File file = fileChooser.showOpenDialog(null);
        try {
                BufferedImage bufferedImage = ImageIO.read(file);
                Image image = SwingFXUtils.toFXImage(bufferedImage, null);
                workspace.getLeftFooter().setImage(image);
                workspace.getLeftFooter().setFitHeight(40);
                workspace.getLeftFooter().setFitWidth(100);
            } catch (IOException ex) {
                Logger.getLogger(GeneratorController.class.getName()).log(Level.SEVERE, null, ex);
            }
    }

    void handleChangeRightFooterImage() {
        FileChooser fileChooser = new FileChooser();
        GeneratorWorkspace workspace = (GeneratorWorkspace) app.getWorkspaceComponent();
        //Set extension filter
        FileChooser.ExtensionFilter extFilterJPG = new FileChooser.ExtensionFilter("JPG files (*.jpg)", "*.JPG");
        FileChooser.ExtensionFilter extFilterPNG = new FileChooser.ExtensionFilter("PNG files (*.png)", "*.PNG");
        fileChooser.getExtensionFilters().addAll(extFilterJPG, extFilterPNG);
        File file = fileChooser.showOpenDialog(null);
        try {
                BufferedImage bufferedImage = ImageIO.read(file);
                Image image = SwingFXUtils.toFXImage(bufferedImage, null);
                workspace.getRightFooter().setImage(image);
                String path =file.getAbsolutePath();
                workspace.getRightFooter().setFitHeight(40);
                workspace.getRightFooter().setFitWidth(100);
            } catch (IOException ex) {
                Logger.getLogger(GeneratorController.class.getName()).log(Level.SEVERE, null, ex);
            }
    }

    void handleChangeTemplateDirectory() {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        GeneratorWorkspace workspace = (GeneratorWorkspace) app.getWorkspaceComponent();
        CourseSiteData data = (CourseSiteData) app.getDataComponent();

        DirectoryChooser fc = new DirectoryChooser();
        fc.setInitialDirectory(new File(PATH_WORK));
        fc.setTitle(props.getProperty(""));

        File selectedFile = fc.showDialog(app.getGUI().getWindow());
        String filePath = selectedFile.getPath();
        workspace.setTemplateDirectory(filePath);
        workspace.resetTemplateDir();
    }

    void handleExport() throws IOException {
          GeneratorWorkspace workspace = (GeneratorWorkspace) app.getWorkspaceComponent();
        CourseSiteData data = (CourseSiteData) app.getDataComponent();
        CourseSiteFile dile = (CourseSiteFile) app.getFileComponent();
        dile.saveData(data, workspace.getTemplateDirectory()+"./js/test");
        
        File file = new File(workspace.getTemplateDirectory());
        File exFile = new File(workspace.getExportDirectory());
        FileUtils.copyDirectory(file, exFile);
    }
}
