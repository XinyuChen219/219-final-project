/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coursesitegenerator.workspace;

import coursesitegenerator.GeneratorApp;
import coursesitegenerator.GeneratorProp;
import coursesitegenerator.data.CourseSiteData;
import coursesitegenerator.data.HWs;
import coursesitegenerator.data.Holiday;
import coursesitegenerator.data.Lecture;
import coursesitegenerator.data.Recitation;
import coursesitegenerator.data.RecitationsSchedule;
import coursesitegenerator.data.Reference;
import coursesitegenerator.data.ScheduleItem;
import coursesitegenerator.data.Student;
import coursesitegenerator.data.TAData;
import coursesitegenerator.data.TeachingAssistant;
import coursesitegenerator.data.Team;
import coursesitegenerator.file.CourseSiteFile;
import coursesitegenerator.style.style;
import djf.components.AppDataComponent;
import djf.components.AppWorkspaceComponent;
import static djf.settings.AppStartupConstants.PATH_CSS;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.embed.swing.SwingFXUtils;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.SplitPane;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javax.imageio.ImageIO;
import jtps.jTPS;
import properties_manager.PropertiesManager;

/**
 *
 * @author xinyu
 */
public class GeneratorWorkspace extends AppWorkspaceComponent {

    GeneratorApp app;
    GeneratorController controller;
    jTPS jtps;
    Button redoButton;
    Button undoButton;
    TabPane tabPane;
    BorderPane courseDetailPane;
    BorderPane TADataPane;
    BorderPane recitationDataPane;
    BorderPane scheduleDataPane;
    BorderPane projectDataPane;
    Tab courseDetail;
    Tab TAData;
    Tab recitationData;
    Tab scheduleData;
    Tab projectData;

    HBox tasHeaderBox;
    Label tasHeaderLabel;

    // FOR THE TA TABLE
    TableView<TeachingAssistant> taTable;
    TableColumn<TeachingAssistant, String> nameColumn;
    TableColumn<TeachingAssistant, String> emailColumn;
    TableColumn<TeachingAssistant, Boolean> underGradColumn;

    // THE TA INPUT
    HBox addBox;
    TextField nameTextField;
    TextField emailTextField;
    Button addButton;
    Button clearButton;
    Button deleteTAButton;
    CheckBox underGradTABox;

    // THE HEADER ON THE RIGHT
    ComboBox<String> startHourBox;
    ComboBox<String> endHourBox;
    HBox officeHoursHeaderBox;
    Label officeHoursHeaderLabel;

    // THE OFFICE HOURS GRID
    GridPane officeHoursGridPane;
    HashMap<String, Pane> officeHoursGridTimeHeaderPanes;
    HashMap<String, Label> officeHoursGridTimeHeaderLabels;
    HashMap<String, Pane> officeHoursGridDayHeaderPanes;
    HashMap<String, Label> officeHoursGridDayHeaderLabels;
    HashMap<String, Pane> officeHoursGridTimeCellPanes;
    HashMap<String, Label> officeHoursGridTimeCellLabels;
    HashMap<String, Pane> officeHoursGridTACellPanes;
    HashMap<String, Label> officeHoursGridTACellLabels;

    //FOR THE COURSE DETAIL PANE
    GridPane courseInfo; // Course Info Pane
    Label courseInfoBoxLabel;
    Label subjectLabel;
    TextField subjectBox;
    Label numberLabel;
    TextField numberBox;
    Label semesterLabel;;

    TextField semesterBox;
    Label yearLabel;
    TextField yearBox;
    Label titleLabel;
    TextField titleTextField;
    Label instructorNameLabel;
    TextField instructorNameTextField;
    Label instructorHomeLabel;
    TextField instructorHomeTextField;
    Label exportDirLabel;
    String exportDirectory;
    Button changeExportDirButton;
    TextField subjectTextField;
    TextField numberTextField;
    TextField semesterTextField;
    TextField yearTextField;
    Label exportDir;

    //Site Template
    GridPane siteTemplatePane;
    Label siteTemplateLabel;
    Label directoryLabel;
    Label diL;
    Button selectTemplateDirButton;
    Label sitePagesLabel;
    GridPane sitePagePane;
    Label useLabel;
    Label navbarTitleLabel;
    Label fileNameLabel;
    Label scriptLabel;
    CheckBox checkHomeBox;
    CheckBox checkSyllabusBox;
    CheckBox checkScheduleBox;
    CheckBox checkHWsBox;
    CheckBox checkProjectBox;
    String templateDirectory;

    //Page Style
    GridPane pageStylePane;
    GridPane pageStyleGridPane;
    Label bannerSchoolLabel;
    Label leftFooterLabel;
    Label rightFooterLabel;
    Label pageStyleLabel;
    Button changeImageButton;
    Button changeImageButton1;
    Button changeImageButton2;
    Label styleSheetLabel;
    Label styleSheetNoteLabel;
    ComboBox styleSheetBox;
    Image bannerSchoolImage;
    Image leftFooterImage;
    Image rightFooterImage;
    ImageView bannerSchool;
    ImageView leftFooter;
    ImageView rightFooter;

    //Recitation Data
    Boolean addRecitation;
    Label recitationLabel;
    Button removeRecitationButton;
    GridPane recitationGridPane;
    Label sectionLabel;
    Label instructorLabel;
    Label dayTimeLabel;
    Label locationLabel;
    Label ta1Label;
    Label ta2Label;
    TextField recitationSectionTextField;
    TextField recitationInstructorTextField;
    TextField recitationDayTimeTextField;
    TextField recitationLocationTextField;
    ComboBox<String> recitationSupervisingTA1ComboBox;
    ComboBox<String> recitationSupervisingTA2ComboBox;
    GridPane addRecitationPane;
    Button addRecitationButton;
    Button clearRecitationButton;
    HBox rB;//recitation header box
    TableView<Recitation> recitationTable;
    TableColumn<Recitation, String> recitationSectionColumn;
    TableColumn<Recitation, String> recitationInstructorColumn;
    TableColumn<Recitation, String> recitationDayTimeColumn;
    TableColumn<Recitation, String> recitationLocationColumn;
    TableColumn<Recitation, String> recitationTA1Column;
    TableColumn<Recitation, String> recitationTA2Column;

    //Schedule Pane
    DatePicker startingMondayDatePicker;
    DatePicker endingFridayDatePicker;
    Label scheduleLabel;
    GridPane calendarBoundaries;
    VBox scheduleItems;
    GridPane scheduleItemsGrid;
    GridPane addSchedulePane;
    TableView<ScheduleItem> scheduleTable;
    TableColumn<ScheduleItem, String> scheduleTypeColumn;
    TableColumn<ScheduleItem, String> scheduleDateColumn;
    TableColumn<ScheduleItem, String> scheduleTitleColumn;
    TableColumn<ScheduleItem, String> scheduleTopicColumn;
    Button deleteScheduleItemButton;
    ComboBox<String> scheduleTypeBox;
    DatePicker scheduleDatePicker;
    TextField scheduleTimeTextField;
    TextField scheduleTitleTextField;
    TextField scheduleTopicTextField;
    TextField scheduleLinkTextField;
    TextField scheduleCriteriaTextField;
    Button addUpdateScheduleItemButton;
    Button clearScheduleItemButton;

    //Project
    GridPane teamPane;
    TableView<Team> teamTable;
    TableColumn<Team, String> teamNameColumn;
    TableColumn<Team, String> teamColorColumn;
    TableColumn<Team, String> teamTextColorColumn;
    TableColumn<Team, String> teamLinkColumn;
    TableView<Student> studentTable;
    TableColumn<Student, String> studentFirstNameColumn;
    TableColumn<Student, String> studentLastNameColumn;
    TableColumn<Student, String> studentTeamColumn;
    TableColumn<Student, String> studentRoleColumn;
    Button addUpdateTeamButton;
    Button clearTeamButton;
    TextField teamNameTextField;
    ColorPicker teamColorPicker;
    ColorPicker teamTextColorPicker;
    TextField teamLinkTextField;
    Button deleteTeamButton;
    Button deleteStudentButton;
    TextField studentFirstNameTextField;
    TextField studentLastNameTextField;
    ComboBox<String> studentTeamComboBox;
    ComboBox<String> studentRoleComboBox;
    Button addUpdateStudentButton;
    Button clearStudentButton;
    GridPane studentPane;
    VBox teamBox;
    VBox studentBox;
    HBox teamDeleteBox;
    HBox studentDeleteBox;

    String oldRecitationSection;
    String oldScheduleTitle;
    String oldTeamName;
    String[] oldStudentName;
    String oldStudentFirstName;
    String oldStudentLastName;
    Boolean addSchedule;
    Boolean addTeam;
    Boolean addStudent;
    Boolean addTA;
    String oldTAName;
    
    Button exportButton;

    public GeneratorWorkspace(GeneratorApp initApp) {

        oldRecitationSection = "";
        oldScheduleTitle = "";
        oldStudentName = new String[2];
        app = initApp;
        exportButton = app.getGUI().getExportButton();
        redoButton = app.getGUI().getRedoButton();
        undoButton = app.getGUI().getUndoButton();
        jtps = new jTPS();
        addRecitation = true;
        addSchedule = true;
        addTeam = true;
        addStudent = true;
        addTA = true;
        PropertiesManager props = PropertiesManager.getPropertiesManager();

        initializeCourseDetailPane();
        initializeTADataPane();
        initializeRecitationDataPane();
        initializeScheduleDataPane();
        initializeProjectDataPane();
        loadStyleSheet();

        //ADD THE COURSE DETAIL PANE
        tabPane = new TabPane();
        courseDetail = new Tab();
        String courseDetailHeaderText = props.getProperty(GeneratorProp.COURSE_DETAIL_HEADER_TEXT.toString());
        courseDetail.setText(courseDetailHeaderText);
        courseDetail.setContent(courseDetailPane);
        tabPane.getTabs().add(courseDetail);
        //TA DATA PANE
        TAData = new Tab();
        String TADataHeaderText = props.getProperty(GeneratorProp.TA_DATA_HEADER_TEXT.toString());
        TAData.setText(TADataHeaderText);
        TAData.setContent(TADataPane);
        tabPane.getTabs().add(TAData);
        //RECITATION DATA PANE
        recitationData = new Tab();
        String recitationDataHeaderText = props.getProperty(GeneratorProp.RECITATION_DATA_HEADER_TEXT.toString());
        recitationData.setText(recitationDataHeaderText);
        recitationData.setContent(recitationDataPane);
        tabPane.getTabs().add(recitationData);
        //SCHEDULE DATA PANE
        scheduleData = new Tab();
        String scheduleDataHeaderText = props.getProperty(GeneratorProp.SCHEDULE_DATA_HEADER_TEXT.toString());
        scheduleData.setText(scheduleDataHeaderText);
        scheduleData.setContent(scheduleDataPane);
        tabPane.getTabs().add(scheduleData);
        //PROJECT DATA PANE
        projectData = new Tab();
        String projectDataHeaderText = props.getProperty(GeneratorProp.PROJECT_DATA_HEADER_TEXT.toString());
        projectData.setText(projectDataHeaderText);
        projectData.setContent(projectDataPane);
        tabPane.getTabs().add(projectData);

        workspace = new BorderPane();
        ((BorderPane) workspace).setCenter(tabPane);

        // NOW LET'S SETUP THE EVENT HANDLING
        controller = new GeneratorController(app);

        
        subjectTextField.textProperty().addListener(e -> {
            controller.handleChangeSubject();
        });
        numberTextField.textProperty().addListener(e -> {
            controller.handleChangeNumber();
        });
        semesterTextField.textProperty().addListener(e -> {
            controller.handleChangeSemester();
        });
        yearTextField.textProperty().addListener(e -> {
            controller.handleChangeYear();
        });
        titleTextField.textProperty().addListener(e -> {
            controller.handleChangeTitle();
        });
        instructorNameTextField.textProperty().addListener(e -> {
            controller.handleChangeInstructorName();
        });
        instructorHomeTextField.textProperty().addListener(e -> {
            controller.handleChangeInstructorHome();
        });
        changeExportDirButton.setOnAction(e -> {
            controller.handleChangeExportDirectory();
        });
        
        selectTemplateDirButton.setOnAction(e ->{
            controller.handleChangeTemplateDirectory();
        });
        
        //undo/redo
        redoButton.setOnAction(e -> {
            controller.handleRedoTransaction();
        });
        undoButton.setOnAction(e -> {
            controller.handleUndoTransaction();
        });
        
        exportButton.setOnAction(e ->{
            try {
                controller.handleExport();
            } catch (IOException ex) {
                Logger.getLogger(GeneratorWorkspace.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
        //control for adding recitation
        recitationSectionTextField.setOnAction(e -> {
            if (!addRecitation) {
                controller.handleEditRecitation(oldRecitationSection);
            } else {
                controller.handleAddRecitation();
            }
        });
        recitationInstructorTextField.setOnAction(e -> {
            if (!addRecitation) {
                controller.handleEditRecitation(oldRecitationSection);
            } else {
                controller.handleAddRecitation();
            }
        });
        recitationDayTimeTextField.setOnAction(e -> {
            if (!addRecitation) {
                controller.handleEditRecitation(oldRecitationSection);
            } else {
                controller.handleAddRecitation();
            }
        });
        recitationLocationTextField.setOnAction(e -> {
            if (!addRecitation) {
                controller.handleEditRecitation(oldRecitationSection);
            } else {
                controller.handleAddRecitation();
            }
        });
        addRecitationButton.setOnAction(e -> {
            if (!addRecitation) {
                controller.handleEditRecitation(oldRecitationSection);
            } else {
                controller.handleAddRecitation();
            }
        });

        //ADD OR UDPATE THE SCHEDULE
        scheduleTimeTextField.setOnAction(e -> {
            if (!addSchedule) {
                controller.handleEditScheduleIteam(oldScheduleTitle);
            } else {
                controller.handleAddSchedule();
            }
        });
        scheduleTitleTextField.setOnAction(e -> {
            if (!addSchedule) {
                controller.handleEditScheduleIteam(oldScheduleTitle);
            } else {
                controller.handleAddSchedule();
            }
        });
        scheduleTopicTextField.setOnAction(e -> {
            if (!addSchedule) {
                controller.handleEditScheduleIteam(oldScheduleTitle);
            } else {
                controller.handleAddSchedule();
            }
        });
        scheduleLinkTextField.setOnAction(e -> {
            if (!addSchedule) {
                controller.handleEditScheduleIteam(oldScheduleTitle);
            } else {
                controller.handleAddSchedule();
            }
        });
        addUpdateScheduleItemButton.setOnAction(e -> {
            if (!addSchedule) {
                controller.handleEditScheduleIteam(oldScheduleTitle);
            } else {
                controller.handleAddSchedule();
            }
        });
        scheduleTable.setFocusTraversable(true);
        scheduleTable.setOnMouseClicked(e -> {
            addSchedule = false;
            oldScheduleTitle = controller.loadScheduleToTextField();
        });
        deleteScheduleItemButton.setOnAction(e -> {
            controller.handleDeleteScheduleItem();
        });

        clearScheduleItemButton.setOnAction(e -> {
            addSchedule = true;
            resetAddSchedule();
            scheduleTable.getSelectionModel().select(null);
        });

        //Project data pane
        teamNameTextField.setOnAction(e -> {
            if (!addTeam) {
                controller.handleEditTeam(oldTeamName);
            } else {
                controller.handleAddTeam();
            }
        });
        teamLinkTextField.setOnAction(e -> {
            if (!addTeam) {
                controller.handleEditTeam(oldTeamName);
            } else {
                controller.handleAddTeam();
            }
        });
        addUpdateTeamButton.setOnAction(e -> {
            if (!addTeam) {
                controller.handleEditTeam(oldTeamName);
            } else {
                controller.handleAddTeam();
            }
        });
        teamTable.setFocusTraversable(true);
        teamTable.setOnMouseClicked(e -> {
            addTeam = false;
            oldTeamName = controller.loadTeamToTextField();
        });
        deleteTeamButton.setOnAction(e -> {
            controller.handleDeleteTeam();
        });
        clearTeamButton.setOnAction(e -> {
            addTeam = true;
            resetAddTeam();
            teamTable.getSelectionModel().select(null);
        });

        studentFirstNameTextField.setOnAction(e -> {
            if (!addStudent) {
                controller.handleEditStudent(oldStudentFirstName, oldStudentLastName);
            } else {
                controller.handleAddStudent();
            }
        });
        studentLastNameTextField.setOnAction(e -> {
            if (!addStudent) {
                controller.handleEditStudent(oldStudentFirstName, oldStudentLastName);
            } else {
                controller.handleAddStudent();
            }
        });
        
        addUpdateStudentButton.setOnAction(e -> {
            if (!addStudent) {
                controller.handleEditStudent(oldStudentFirstName, oldStudentLastName);
            } else {
                controller.handleAddStudent();
            }
        });
        studentTable.setFocusTraversable(true);
        studentTable.setOnMouseClicked(e -> {
            addStudent = false;
            oldStudentName = controller.loadStudentToTextField();
            oldStudentFirstName = oldStudentName[0];
            oldStudentLastName = oldStudentName[1];
        });
        deleteStudentButton.setOnAction(e -> {
            controller.handleDeleteStudent();
        });
        clearStudentButton.setOnAction(e -> {
            addStudent = true;
            resetAddStudent();
            studentTable.getSelectionModel().select(null);
        });

        //REMOVE RECITATION
        removeRecitationButton.setOnAction(e -> {
            controller.handleDeleteRecitation();
        });

        clearRecitationButton.setOnAction(e -> {
            addRecitation = true;
            recitationSectionTextField.clear();
            recitationInstructorTextField.clear();
            recitationDayTimeTextField.clear();
            recitationLocationTextField.clear();
            recitationSupervisingTA1ComboBox.setValue("none");
            recitationSupervisingTA2ComboBox.setValue("none");
            recitationTable.getSelectionModel().select(null);
        });

        recitationTable.setFocusTraversable(true);
        recitationTable.setOnMouseClicked(e -> {
            addRecitation = false;
            oldRecitationSection = controller.loadRecitationToTextField();
        });

        // CONTROLS FOR ADDING TAs
        nameTextField.setOnAction(e -> {
            if (!addTA) {
                controller.handleEditTA(oldTAName);
            } else {
                controller.handleAddTA();
            }
        });
        emailTextField.setOnAction(e -> {
            if (!addTA) {
                controller.handleEditTA(oldTAName);
            } else {
                controller.handleAddTA();
            }
        });
        addButton.setOnAction(e -> {
            if (!addTA) {
                controller.handleEditTA(oldTAName);
            } else {
                controller.handleAddTA();
            }
        });
        clearButton.setOnAction(e -> {
            addTA = true;
            resetAddBox();
            taTable.getSelectionModel().select(null);
        });
        taTable.setFocusTraversable(true);
        taTable.setOnMouseClicked(e -> {
            addTA = false;
            oldTAName = controller.loadTAToTextField();
        });

        deleteTAButton.setOnAction(e -> {
            controller.handleDeleteTA();
        });

        //SCHEDULE DATA PANE
        startingMondayDatePicker.setOnAction(e -> {
            controller.handleChangedStartingMondayDate();
        });
        endingFridayDatePicker.setOnAction(e -> {
            controller.handleChangeEndingFridayDate();
        });
        startHourBox.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> ov, String oldStartTime, String newStartTime) {
                controller.handleChangedStartHourP(oldStartTime, newStartTime);
                resetComboBoxes();
            }
        });

        endHourBox.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> ov, String oldEndTime, String newEndTime) {
                controller.handleChangedEndHourP(oldEndTime, newEndTime);
                resetComboBoxes();
            }

        });

        changeImageButton.setOnAction(e -> {
            controller.handleChangeBannerSchoolImage();
        });
        changeImageButton1.setOnAction(e -> {
            controller.handleChangeLeftFooterImage();
        });
        changeImageButton2.setOnAction(e -> {
            controller.handleChangeRightFooterImage();
        });

    }

    public void initializeCourseDetailPane() {
        PropertiesManager props = PropertiesManager.getPropertiesManager();

        courseDetailPane = new BorderPane();

        //Course Info Pane
        //get "Course Info" Label
        String courseInfoBoxHeaderText = props.getProperty(GeneratorProp.COURSE_INFO_BOX_HEADER_TEXT.toString());
        courseInfoBoxLabel = new Label(courseInfoBoxHeaderText);
        courseInfo = new GridPane();
        courseInfo.add(courseInfoBoxLabel, 0, 0);

        //First line in courseInfo pane
        String subjectLabelText = props.getProperty(GeneratorProp.SUBJECT_LABEL_TEXT.toString());
        subjectLabel = new Label(subjectLabelText);
        subjectTextField = new TextField();
        String numberLabelText = props.getProperty(GeneratorProp.NUMBER_LABEL_TEXT.toString());
        numberLabel = new Label(numberLabelText);
        numberTextField = new TextField();
        courseInfo.addRow(1, subjectLabel, subjectTextField, numberLabel, numberTextField);
        //Second line in courseInfo pane
        String semesterLabelText = props.getProperty(GeneratorProp.SEMESTER_LABEL_TEXT.toString());
        semesterLabel = new Label(semesterLabelText);
        semesterTextField = new TextField();
        String yearLabelText = props.getProperty(GeneratorProp.YEAR_LABEL_TEXT.toString());
        yearLabel = new Label(yearLabelText);
        yearTextField = new TextField();
        courseInfo.addRow(2, semesterLabel, semesterTextField, yearLabel, yearTextField);
        //Third line in courseInfo pane( title: textField)
        String titleLabelText = props.getProperty(GeneratorProp.TITLE_LABEL_TEXT.toString());
        titleLabel = new Label(titleLabelText);
        titleTextField = new TextField();
        courseInfo.addRow(3, titleLabel, titleTextField);
        //Fourth line in courseInfo pane( instructor name : textField )
        String instructorNameLabelText = props.getProperty(GeneratorProp.INSTRUTOR_NAME_LABEL_TEXT.toString());
        instructorNameLabel = new Label(instructorNameLabelText);
        instructorNameTextField = new TextField();
        courseInfo.addRow(4, instructorNameLabel, instructorNameTextField);
        //Fifth line in courseInfo pane(instructor home : textField)
        String instructorHomeLabelText = props.getProperty(GeneratorProp.INSTRUTOR_HOME_LABEL_TEXT.toString());
        instructorHomeLabel = new Label(instructorHomeLabelText);
        instructorHomeTextField = new TextField();
        courseInfo.addRow(5, instructorHomeLabel, instructorHomeTextField);
        //6th line in courseInfo pane
        String exportDirLabelText = props.getProperty(GeneratorProp.EXPORT_DIR_LABEL_TEXT.toString());
        exportDirLabel = new Label(exportDirLabelText);
        String changeButtonText = props.getProperty(GeneratorProp.CHANGE_BUTTON_TEXT.toString());
        changeExportDirButton = new Button(changeButtonText);

        exportDir = new Label(exportDirectory);
        courseInfo.addRow(6, exportDirLabel, exportDir, changeExportDirButton);

        //Site Template Pane
        // Add the header of site template
        siteTemplatePane = new GridPane();
        String siteTemplateLabelText = props.getProperty(GeneratorProp.SITE_TEMPLATE_LABEL_TEXT.toString());
        siteTemplateLabel = new Label(siteTemplateLabelText);
        siteTemplatePane.add(siteTemplateLabel, 0, 0);

        String directoryLabelText = props.getProperty(GeneratorProp.DIRECTORY_LABEL_TEXT.toString());
        directoryLabel = new Label(directoryLabelText);
        templateDirectory = "no directory is selected";
        diL = new Label(templateDirectory);
        siteTemplatePane.add(directoryLabel, 0, 1);
        siteTemplatePane.add(diL, 0, 2);
        String selectTemplateDirButtonText = props.getProperty(GeneratorProp.SELECT_TEMPLATE_DIR_BUTTON_TEXT.toString());
        selectTemplateDirButton = new Button(selectTemplateDirButtonText);
        siteTemplatePane.add(selectTemplateDirButton, 0, 3);

        String sitePagesLabelText = props.getProperty(GeneratorProp.SITE_PAGES_LABEL_TEXT.toString());
        sitePagesLabel = new Label(sitePagesLabelText);
        siteTemplatePane.add(sitePagesLabel, 0, 4);

        sitePagePane = new GridPane();
        String useLabelText = props.getProperty(GeneratorProp.USE_LABEL_TEXT.toString());
        String navbarTitleLabelText = props.getProperty(GeneratorProp.NAVBAR_TITLE_LABEL_TEXT.toString());
        String fileNameLabelText = props.getProperty(GeneratorProp.FILE_NAME_LABEL_TEXT.toString());
        String scriptLabelText = props.getProperty(GeneratorProp.SCRIPT_LABEL_TEXT.toString());
        useLabel = new Label(useLabelText);
        navbarTitleLabel = new Label(navbarTitleLabelText);
        fileNameLabel = new Label(fileNameLabelText);
        scriptLabel = new Label(scriptLabelText);
        sitePagePane.addRow(0, useLabel, navbarTitleLabel, fileNameLabel, scriptLabel);
        checkHomeBox = new CheckBox();
        Label home = new Label("Home");
        Label index = new Label("index.html");
        Label homeBuilder = new Label("HomeBuilder.jx");
        sitePagePane.addRow(1, checkHomeBox, home, index, homeBuilder);
        checkSyllabusBox = new CheckBox();
        Label syllabus = new Label("Syllabus");
        Label s1 = new Label("syllabus.html");
        Label s2 = new Label("SyllabusBuidler.js");
        sitePagePane.addRow(2, checkSyllabusBox, syllabus, s1, s2);
        checkScheduleBox = new CheckBox();
        Label a1 = new Label("Schedule");
        Label a2 = new Label("schedule.html");
        Label a3 = new Label("ScheduleBuilder.js");
        sitePagePane.addRow(3, checkScheduleBox, a1, a2, a3);
        checkHWsBox = new CheckBox();
        Label h1 = new Label("HWs");
        Label h2 = new Label("hws.html");
        Label h3 = new Label("HWsBuilder.js");
        sitePagePane.addRow(4, checkHWsBox, h1, h2, h3);
        checkProjectBox = new CheckBox();
        Label p1 = new Label("Projects");
        Label p2 = new Label("projects.html");
        Label p3 = new Label("ProjectsBuilder.js");
        sitePagePane.addRow(5, checkProjectBox, p1, p2, p3);
        ColumnConstraints column3 = new ColumnConstraints();
        column3.setPercentWidth(10);
        ColumnConstraints column4 = new ColumnConstraints();
        column4.setPercentWidth(30);
        sitePagePane.getColumnConstraints().addAll(column3, column4, column4, column4);

        siteTemplatePane.add(sitePagePane, 0, 5);

        //Page Style
        pageStylePane = new GridPane();
        String pageStyleLabelText = props.getProperty(GeneratorProp.PAGE_STYLE_LABEL_TEXT.toString());
        pageStyleLabel = new Label(pageStyleLabelText);
        pageStylePane.add(pageStyleLabel, 0, 0);

        String bannerSchoolImageLabelText = props.getProperty(GeneratorProp.BANNER_SCHOOL_IMAGE_LABEL_TEXT.toString());
        String leftFooterImageLabelText = props.getProperty(GeneratorProp.LEFT_FOOTER_IMAGE_LABEL_TEXT.toString());
        String rightFooterImageLabelText = props.getProperty(GeneratorProp.RIGHT_FOOTER_IMAGE_LABEL_TEXT.toString());
        bannerSchoolLabel = new Label(bannerSchoolImageLabelText);
        leftFooterLabel = new Label(leftFooterImageLabelText);
        rightFooterLabel = new Label(rightFooterImageLabelText);

        bannerSchool = new ImageView();
        leftFooter = new ImageView();
        rightFooter = new ImageView();
        String changeImageButtonText = props.getProperty(GeneratorProp.CHANGE_BUTTON_TEXT.toString());
        changeImageButton = new Button(changeImageButtonText);
        changeImageButton1 = new Button(changeImageButtonText);
        changeImageButton2 = new Button(changeImageButtonText);
        pageStylePane.addRow(1, bannerSchoolLabel, bannerSchool, changeImageButton);
        pageStylePane.addRow(2, leftFooterLabel, leftFooter, changeImageButton1);
        pageStylePane.addRow(3, rightFooterLabel, rightFooter, changeImageButton2);

        String styleSheetLabelText = props.getProperty(GeneratorProp.STYLESHEET_LABEL_TEXT.toString());
        styleSheetLabel = new Label(styleSheetLabelText);
        styleSheetBox = new ComboBox<String>();
        pageStylePane.addRow(4, styleSheetLabel, styleSheetBox);

        String pageStyleNoteLabelText = props.getProperty(GeneratorProp.PAGE_STYLE_NOTE_TEXT.toString());
        styleSheetNoteLabel = new Label(pageStyleNoteLabelText);
        pageStylePane.addRow(5, styleSheetNoteLabel);

        ColumnConstraints column1 = new ColumnConstraints();

        courseDetailPane.setTop(courseInfo);
        courseDetailPane.setCenter(siteTemplatePane);
        courseDetailPane.setBottom(pageStylePane);
    }

    public void reloadCourseInfo(CourseSiteData dataComponent) {
        subjectTextField.setText(dataComponent.getSubject());
        numberTextField.setText(dataComponent.getNumber());
        semesterTextField.setText(dataComponent.getCourseInfoSemester());
        yearTextField.setText(dataComponent.getYear());
        titleTextField.setText(dataComponent.getCourseTitle());
        instructorHomeTextField.setText(dataComponent.getCourseInstructorHome());
        instructorNameTextField.setText(dataComponent.getCourseInstructorName());

    }

    public void initializeTADataPane() {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        TADataPane = new BorderPane();

        // INIT THE HEADER ON THE LEFT
        tasHeaderBox = new HBox();
        String tasHeaderText = props.getProperty(GeneratorProp.TAS_HEADER_TEXT.toString());
        tasHeaderLabel = new Label(tasHeaderText);
        String deleteText = props.getProperty(GeneratorProp.DELETE_BUTTON_TEXT.toString());
        deleteTAButton = new Button(deleteText);
        tasHeaderBox.getChildren().add(tasHeaderLabel);
        tasHeaderBox.getChildren().add(deleteTAButton);
        tasHeaderBox.setAlignment(Pos.CENTER_LEFT);

        // MAKE THE TABLE AND SETUP THE DATA MODEL
        taTable = new TableView();
        taTable.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        CourseSiteData data = (CourseSiteData) app.getDataComponent();
        ObservableList<TeachingAssistant> tableData = data.getTeachingAssistants();
        taTable.setItems(tableData);
        String nameColumnText = props.getProperty(GeneratorProp.NAME_COLUMN_TEXT.toString());
        String emailColumnText = props.getProperty(GeneratorProp.EMAIL_COLUMN_TEXT.toString());
        String underGradColumnText = props.getProperty(GeneratorProp.UNDERGRAD_TEXT.toString());

        nameColumn = new TableColumn(nameColumnText);
        emailColumn = new TableColumn(emailColumnText);
        underGradColumn = new TableColumn(underGradColumnText);
        nameColumn.setCellValueFactory(
                new PropertyValueFactory<TeachingAssistant, String>("name")
        );
        emailColumn.setCellValueFactory(
                new PropertyValueFactory<TeachingAssistant, String>("email")
        );
        underGradColumn.setCellValueFactory(
                new PropertyValueFactory<TeachingAssistant, Boolean>("underGrad")
        );
        taTable.getColumns().add(underGradColumn);
        taTable.getColumns().add(nameColumn);
        taTable.getColumns().add(emailColumn);

        // ADD BOX FOR ADDING A TA
        String namePromptText = props.getProperty(GeneratorProp.NAME_PROMPT_TEXT.toString());
        String emailPromptText = props.getProperty(GeneratorProp.EMAIL_PROMPT_TEXT.toString());
        String addButtonText = props.getProperty(GeneratorProp.ADD_BUTTON_TEXT.toString());
        nameTextField = new TextField();
        emailTextField = new TextField();
        nameTextField.setPromptText(namePromptText);
        emailTextField.setPromptText(emailPromptText);
        underGradTABox = new CheckBox();
        addButton = new Button(addButtonText);
        clearButton = new Button("Clear");
        addBox = new HBox();
        underGradTABox.prefWidthProperty().bind(addBox.widthProperty().multiply(.04));
        nameTextField.prefWidthProperty().bind(addBox.widthProperty().multiply(.33));
        emailTextField.prefWidthProperty().bind(addBox.widthProperty().multiply(.33));
        addButton.prefWidthProperty().bind(addBox.widthProperty().multiply(.15));
        clearButton.prefWidthProperty().bind(addBox.widthProperty().multiply(.15));
        addBox.getChildren().add(underGradTABox);
        addBox.getChildren().add(nameTextField);
        addBox.getChildren().add(emailTextField);
        addBox.getChildren().add(addButton);
        addBox.getChildren().add(clearButton);
        addBox.setAlignment(Pos.CENTER_LEFT);

        String[] hours = {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24"};
        startHourBox = new ComboBox<String>();
        startHourBox.getItems().addAll(hours);
        startHourBox.setValue(String.valueOf(data.getStartHour()));
        endHourBox = new ComboBox<String>();
        endHourBox.getItems().addAll(hours);
        endHourBox.setValue(String.valueOf(data.getEndHour()));

        officeHoursHeaderBox = new HBox();
        String officeHoursGridText = props.getProperty(GeneratorProp.OFFICE_HOURS_SUBHEADER.toString());
        officeHoursHeaderLabel = new Label(officeHoursGridText);
        officeHoursHeaderBox.getChildren().add(startHourBox);
        officeHoursHeaderBox.getChildren().add(endHourBox);
        officeHoursHeaderBox.getChildren().add(officeHoursHeaderLabel);

        // THESE WILL STORE PANES AND LABELS FOR OUR OFFICE HOURS GRID
        officeHoursGridPane = new GridPane();
        officeHoursGridTimeHeaderPanes = new HashMap();
        officeHoursGridTimeHeaderLabels = new HashMap();
        officeHoursGridDayHeaderPanes = new HashMap();
        officeHoursGridDayHeaderLabels = new HashMap();
        officeHoursGridTimeCellPanes = new HashMap();
        officeHoursGridTimeCellLabels = new HashMap();
        officeHoursGridTACellPanes = new HashMap();
        officeHoursGridTACellLabels = new HashMap();

        // ORGANIZE THE LEFT AND RIGHT PANES
        VBox leftPane = new VBox();
        leftPane.getChildren().add(tasHeaderBox);
        leftPane.getChildren().add(taTable);
        leftPane.getChildren().add(addBox);
        VBox rightPane = new VBox();
        rightPane.getChildren().add(officeHoursHeaderBox);
        rightPane.getChildren().add(officeHoursGridPane);

        SplitPane sPane = new SplitPane(leftPane, new ScrollPane(rightPane));
        ((BorderPane) TADataPane).setCenter(sPane);
        taTable.prefHeightProperty().bind(TADataPane.heightProperty().multiply(1.9));
    }

    public void initializeRecitationDataPane() {
        PropertiesManager props = PropertiesManager.getPropertiesManager();

        recitationDataPane = new BorderPane();

        String recitationLabelText = props.getProperty(GeneratorProp.RECITATION_LABEL_TEXT.toString());
        recitationLabel = new Label(recitationLabelText);
        String removeButtonText = props.getProperty(GeneratorProp.REMOVE_BUTTON_TEXT.toString());
        removeRecitationButton = new Button(removeButtonText);
        rB = new HBox();
        rB.getChildren().addAll(recitationLabel, removeRecitationButton);
        recitationDataPane.setTop(rB);
        rB.setAlignment(Pos.CENTER_LEFT);
        String sectionLabelText = props.getProperty(GeneratorProp.SECTION_LABEL_TEXT.toString());
        String instructorLabelText = props.getProperty(GeneratorProp.INSTRUCTOR_LABEL_TEXT.toString());
        String dayTimeLabelText = props.getProperty(GeneratorProp.DAY_TIME_LABEL_TEXT.toString());
        String locationLabelText = props.getProperty(GeneratorProp.LOCATION_LABEL_TEXT.toString());
        String taLabelText = props.getProperty(GeneratorProp.TA_LABEL_TEXT.toString());
        String addRecitationButtonText = props.getProperty(GeneratorProp.ADD_RECITATION_BUTTON_TEXT.toString());
        String supervisingTALabelText = props.getProperty(GeneratorProp.SUPERVISING_TA_LABEL_TEXT.toString());
        String clearRecitationButtonText = props.getProperty(GeneratorProp.CLEAR_BUTTON_TEXT.toString());

        addRecitationButton = new Button(addRecitationButtonText);
        clearRecitationButton = new Button(clearRecitationButtonText);

        recitationTable = new TableView();
        recitationTable.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        CourseSiteData data = (CourseSiteData) app.getDataComponent();
        ObservableList<Recitation> recitationsData = data.getRecitations();
        recitationTable.setItems(recitationsData);
        recitationSectionColumn = new TableColumn(sectionLabelText);
        recitationInstructorColumn = new TableColumn(instructorLabelText);
        recitationDayTimeColumn = new TableColumn(dayTimeLabelText);
        recitationLocationColumn = new TableColumn(locationLabelText);
        recitationTA1Column = new TableColumn(taLabelText);
        recitationTA2Column = new TableColumn(taLabelText);
        recitationSectionColumn.setCellValueFactory(
                new PropertyValueFactory<Recitation, String>("section")
        );
        recitationInstructorColumn.setCellValueFactory(
                new PropertyValueFactory<Recitation, String>("instructor")
        );
        recitationDayTimeColumn.setCellValueFactory(
                new PropertyValueFactory<Recitation, String>("dayTime")
        );
        recitationLocationColumn.setCellValueFactory(
                new PropertyValueFactory<Recitation, String>("location")
        );
        recitationTA1Column.setCellValueFactory(
                new PropertyValueFactory<Recitation, String>("ta1")
        );
        recitationTA2Column.setCellValueFactory(
                new PropertyValueFactory<Recitation, String>("ta2")
        );
        recitationTable.getColumns().addAll(recitationSectionColumn, recitationInstructorColumn, recitationDayTimeColumn, recitationLocationColumn, recitationTA1Column, recitationTA2Column);

        recitationDataPane.setCenter(recitationTable);

        addRecitationPane = new GridPane();
        String addEditString = props.getProperty(GeneratorProp.ADD_EDIT_LABEL_TEXT);
        Label addEditLabel = new Label(addEditString);
        addRecitationPane.add(addEditLabel, 0, 0);
        recitationSectionTextField = new TextField();
        sectionLabel = new Label(sectionLabelText + ":");
        addRecitationPane.addRow(1, sectionLabel, recitationSectionTextField);
        recitationInstructorTextField = new TextField();
        recitationDayTimeTextField = new TextField();
        recitationLocationTextField = new TextField();
        instructorLabel = new Label(instructorLabelText + ":");
        dayTimeLabel = new Label(dayTimeLabelText + ":");
        locationLabel = new Label(locationLabelText + ":");
        addRecitationPane.addRow(2, instructorLabel, recitationInstructorTextField);
        addRecitationPane.addRow(3, dayTimeLabel, recitationDayTimeTextField);
        addRecitationPane.addRow(4, locationLabel, recitationLocationTextField);
        recitationSupervisingTA1ComboBox = new ComboBox<String>();
        recitationSupervisingTA2ComboBox = new ComboBox<String>();
        ObservableList<TeachingAssistant> TAsData = data.getTeachingAssistants();
        String[] taNames = new String[TAsData.size()];
        for (int i = 0; i < TAsData.size(); i++) {
            taNames[i] = (TAsData.get(i)).getName();
        }
        recitationSupervisingTA1ComboBox.getItems().add("none");
        recitationSupervisingTA2ComboBox.getItems().add("none");
        recitationSupervisingTA1ComboBox.getItems().addAll(taNames);
        recitationSupervisingTA2ComboBox.getItems().addAll(taNames);
        Label supervisingTALabel = new Label(supervisingTALabelText + ":");
        Label supervisingTALabel2 = new Label(supervisingTALabelText + ":");
        addRecitationPane.addRow(5, supervisingTALabel, recitationSupervisingTA1ComboBox);
        addRecitationPane.addRow(6, supervisingTALabel2, recitationSupervisingTA2ComboBox);

        addRecitationPane.addRow(7, addRecitationButton, clearRecitationButton);

        ColumnConstraints column2 = new ColumnConstraints();
        column2.setPercentWidth(15);
        addRecitationPane.getColumnConstraints().addAll(column2, column2);

        recitationDataPane.setBottom(addRecitationPane);
    }

    public void reloadRecitationPane(CourseSiteData dataComponent) {
        recitationTable.setItems(dataComponent.getRecitations());
        recitationTable.refresh();
        recitationSupervisingTA1ComboBox.getItems().clear();
        recitationSupervisingTA2ComboBox.getItems().clear();
        ObservableList<TeachingAssistant> TAsData = dataComponent.getTeachingAssistants();
        String[] taNames = new String[TAsData.size()];
        for (int i = 0; i < TAsData.size(); i++) {
            taNames[i] = (TAsData.get(i)).getName();
        }
        recitationSupervisingTA1ComboBox.getItems().add("none");
        recitationSupervisingTA2ComboBox.getItems().add("none");
        recitationSupervisingTA1ComboBox.getItems().addAll(taNames);
        recitationSupervisingTA2ComboBox.getItems().addAll(taNames);
    }

    public void initializeScheduleDataPane() {
        PropertiesManager props = PropertiesManager.getPropertiesManager();

        String scheduleLabelText = props.getProperty(GeneratorProp.Schedule_LABEL_TEXT.toString());
        scheduleLabel = new Label(scheduleLabelText);

        String calendarBText = props.getProperty(GeneratorProp.CALENDAR_BOUNDARIES_LABEL_TEXT.toString());
        Label calendarLabel = new Label(calendarBText);
        String startingMonday = props.getProperty(GeneratorProp.STARTING_MONDAY_LABEL_TEXT.toString());
        String endingFriday = props.getProperty(GeneratorProp.ENDING_FRIDAY_LABEL_TEXT.toString());
        Label startingMondayLabel = new Label(startingMonday);
        Label endingFridayLabel = new Label(endingFriday);
        calendarBoundaries = new GridPane();
        startingMondayDatePicker = new DatePicker();
        endingFridayDatePicker = new DatePicker();
        calendarBoundaries.add(scheduleLabel, 0, 0);
        calendarBoundaries.add(calendarLabel, 0, 1);
        calendarBoundaries.addRow(2, startingMondayLabel, startingMondayDatePicker, endingFridayLabel, endingFridayDatePicker);

        ColumnConstraints column1 = new ColumnConstraints();
        column1.setPercentWidth(25);
        calendarBoundaries.getColumnConstraints().addAll(column1, column1, column1, column1);

        scheduleTable = new TableView();
        scheduleTable.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        CourseSiteData data = (CourseSiteData) app.getDataComponent();
        ObservableList<ScheduleItem> scheduleItemData = data.getScheduleItems();
        scheduleTable.setItems(scheduleItemData);

        String scheduleItem = props.getProperty(GeneratorProp.SCHEDULE_ITEM_LABEL_TEXT.toString());
        String deleteText = props.getProperty(GeneratorProp.DELETE_BUTTON_TEXT.toString());
        Label scheduleItemLabel = new Label(scheduleItem);
        scheduleItems = new VBox();
        HBox tBox = new HBox();
        tBox.setAlignment(Pos.CENTER_LEFT);
        tBox.getChildren().add(scheduleItemLabel);
        deleteScheduleItemButton = new Button(deleteText);
        tBox.getChildren().add(deleteScheduleItemButton);
        scheduleItems.getChildren().add(tBox);

        String type = props.getProperty(GeneratorProp.TYPE_LABEL_TEXT.toString());
        String date = props.getProperty(GeneratorProp.DATE_LABEL_TEXT.toString());
        String title = props.getProperty(GeneratorProp.TITLE_LABEL_TEXT.toString());
        String topic = props.getProperty(GeneratorProp.TOPIC_LABEL_TEXT.toString());
        scheduleTypeColumn = new TableColumn(type);
        scheduleDateColumn = new TableColumn(date);
        scheduleTitleColumn = new TableColumn(title);
        scheduleTopicColumn = new TableColumn(topic);
        scheduleTypeColumn.setCellValueFactory(
                new PropertyValueFactory<ScheduleItem, String>("type")
        );
        scheduleDateColumn.setCellValueFactory(
                new PropertyValueFactory<ScheduleItem, String>("date")
        );
        scheduleTitleColumn.setCellValueFactory(
                new PropertyValueFactory<ScheduleItem, String>("title")
        );
        scheduleTopicColumn.setCellValueFactory(
                new PropertyValueFactory<ScheduleItem, String>("topic")
        );
        scheduleTable.getColumns().addAll(scheduleTypeColumn, scheduleDateColumn, scheduleTitleColumn, scheduleTopicColumn);
        scheduleItems.getChildren().add(scheduleTable);

        addSchedulePane = new GridPane();
        String addEdit = props.getProperty(GeneratorProp.ADD_EDIT_LABEL_TEXT.toString());
        String time = props.getProperty(GeneratorProp.TIME_LABEL_TEXT.toString());
        String link = props.getProperty(GeneratorProp.LINK_LABEL_TEXT.toString());
        String criteria = props.getProperty(GeneratorProp.CRITERIA_LABEL_TEXT.toString());
        Label addEditLabel = new Label(addEdit);
        Label typeLabel1 = new Label(type + ":");
        Label dateLabel1 = new Label(date + ":");
        Label timeLabel = new Label(time + ":");
        Label titleLabel1 = new Label(title);
        Label topicLabel1 = new Label(topic + ":");
        Label linkLabel = new Label(link + ":");
        Label criteriaLabel = new Label(criteria + ":");
        String addUpdateButtonText = props.getProperty(GeneratorProp.ADD_UPDATE_BUTTON_TEXT.toString());
        addUpdateScheduleItemButton = new Button(addUpdateButtonText);
        String clear = props.getProperty(GeneratorProp.CLEAR_BUTTON_TEXT.toString());
        clearScheduleItemButton = new Button(clear);
        Label empty = new Label("");
        scheduleTypeBox = new ComboBox<String>();
        scheduleTypeBox.getItems().addAll("Holiday", "Lecture", "Reference", "Recitation", "HW");
        scheduleDatePicker = new DatePicker();
        scheduleTimeTextField = new TextField();
        scheduleTitleTextField = new TextField();
        scheduleTopicTextField = new TextField();
        scheduleLinkTextField = new TextField();
        scheduleCriteriaTextField = new TextField();
        addSchedulePane.addColumn(0, addEditLabel, typeLabel1, dateLabel1, timeLabel, titleLabel1, topicLabel1, linkLabel, criteriaLabel, addUpdateScheduleItemButton);
        addSchedulePane.addColumn(1, empty, scheduleTypeBox, scheduleDatePicker, scheduleTimeTextField, scheduleTitleTextField, scheduleTopicTextField, scheduleLinkTextField, scheduleCriteriaTextField, clearScheduleItemButton);

        scheduleDataPane = new BorderPane();
        scheduleDataPane.setTop(calendarBoundaries);
        scheduleDataPane.setCenter(scheduleItems);
        scheduleDataPane.setBottom(addSchedulePane);
    }

    public void reloadScheduleDataPane(CourseSiteData dataComponent) {
        startingMondayDatePicker.setValue(LocalDate.of(2017, dataComponent.getStartingMondayMonth(), dataComponent.getStartingMondayDay()));
        endingFridayDatePicker.setValue(LocalDate.of(2017, dataComponent.getEndingFridayMonth(), dataComponent.getEndingFridayDay()));
        /**
         * ObservableList<Holiday> holidays = dataComponent.getHolidays();
         * ObservableList<HWs> hws = dataComponent.getHws();
         * ObservableList<Lecture> lectures = dataComponent.getLectures();
         * ObservableList<Reference> references = dataComponent.getReferences();
         * ObservableList<RecitationsSchedule> recSche =
         * dataComponent.getRecitationsSchedule();
         *
         */
    }

    public Label getCourseInfoBoxLabel() {
        return courseInfoBoxLabel;
    }

    public Label getDirectoryLabel() {
        return directoryLabel;
    }

    public Label getDayTimeLabel() {
        return dayTimeLabel;
    }

    public Button getClearRecitationButton() {
        return clearRecitationButton;
    }

    public DatePicker getEndingFridayDatePicker() {
        return endingFridayDatePicker;
    }

    public Button getChangeImageButton() {
        return changeImageButton;
    }

    public Button getChangeImageButton1() {
        return changeImageButton1;
    }

    public Button getChangeImageButton2() {
        return changeImageButton2;
    }

    public TableView<Recitation> getRecitationTable() {
        return recitationTable;
    }

    public VBox getScheduleItems() {
        return scheduleItems;
    }

    public GridPane getCalendarBoundaries() {
        return calendarBoundaries;
    }

    public GridPane getScheduleItemsGrid() {
        return scheduleItemsGrid;
    }

    public GridPane getAddSchedulePane() {
        return addSchedulePane;
    }

    public void initializeProjectDataPane() {
        PropertiesManager props = PropertiesManager.getPropertiesManager();

        String project = props.getProperty(GeneratorProp.PROJECT_LABEL_TEXT.toString());
        String teams = props.getProperty(GeneratorProp.TEAMS_LABEL_TEXT.toString());
        String name = props.getProperty(GeneratorProp.NAME_LABEL_TEXT.toString());
        String color = props.getProperty(GeneratorProp.COLOR_LABEL_TEXT.toString());
        String textColor = props.getProperty(GeneratorProp.TEXT_COLOR_LABEL_TEXT.toString());
        String link = props.getProperty(GeneratorProp.LINK_LABEL_TEXT.toString());

        Label projectLabel = new Label(project);
        Label teamsLabel = new Label(teams);

        teamTable = new TableView();
        teamTable.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        CourseSiteData data = (CourseSiteData) app.getDataComponent();
        ObservableList<Team> tableData = data.getTeams();
        teamTable.setItems(tableData);
        teamNameColumn = new TableColumn(name);
        teamColorColumn = new TableColumn(color);
        teamTextColorColumn = new TableColumn(textColor);
        teamLinkColumn = new TableColumn(link);
        teamNameColumn.setCellValueFactory(
                new PropertyValueFactory<Team, String>("name")
        );
        teamColorColumn.setCellValueFactory(
                new PropertyValueFactory<Team, String>("color")
        );
        teamTextColorColumn.setCellValueFactory(
                new PropertyValueFactory<Team, String>("textColor")
        );
        teamLinkColumn.setCellValueFactory(
                new PropertyValueFactory<Team, String>("link")
        );
        teamTable.getColumns().addAll(teamNameColumn, teamColorColumn, teamTextColorColumn, teamLinkColumn);

        teamBox = new VBox();
        teamDeleteBox = new HBox();
        studentBox = new VBox();
        studentDeleteBox = new HBox();

        String addEdit = props.getProperty(GeneratorProp.ADD_EDIT_LABEL_TEXT.toString());
        String addUpdate = props.getProperty(GeneratorProp.ADD_UPDATE_BUTTON_TEXT.toString());
        String deleteText = props.getProperty(GeneratorProp.DELETE_BUTTON_TEXT.toString());
        deleteTeamButton = new Button(deleteText);
        addUpdateTeamButton = new Button(addUpdate);
        String clearText = props.getProperty(GeneratorProp.CLEAR_BUTTON_TEXT.toString());
        clearTeamButton = new Button(clearText);
        Label addEditLabel = new Label(addEdit);
        teamPane = new GridPane();
        teamDeleteBox.getChildren().addAll(teamsLabel, deleteTeamButton);
        teamBox.getChildren().add(teamDeleteBox);
        teamBox.getChildren().add(teamTable);
        teamDeleteBox.setAlignment(Pos.CENTER_LEFT);

        teamPane.add(addEditLabel, 0, 0);
        Label linkLabel = new Label(link);
        Label nameLabel = new Label(name);
        Label colorLabel = new Label(color);
        Label textColorLabel = new Label(textColor);
        teamNameTextField = new TextField();
        teamColorPicker = new ColorPicker();
        teamTextColorPicker = new ColorPicker();
        teamLinkTextField = new TextField();
        teamPane.addRow(1, nameLabel, teamNameTextField);
        teamPane.addRow(2, colorLabel, teamColorPicker, textColorLabel, teamTextColorPicker);
        teamPane.addRow(3, linkLabel, teamLinkTextField);
        teamPane.addRow(4, addUpdateTeamButton, clearTeamButton);
        teamBox.getChildren().add(teamPane);

        String students = props.getProperty(GeneratorProp.STUDENTS_LABEL_TEXT.toString());
        Label studentsLabel = new Label(students);
        String firstName = props.getProperty(GeneratorProp.FIRST_NAME_LABEL_TEXT.toString());
        String lastName = props.getProperty(GeneratorProp.LAST_NAME_LABEL_TEXT.toString());
        String team = props.getProperty(GeneratorProp.TEAM_LABEL_TEXT.toString());
        String role = props.getProperty(GeneratorProp.ROLE_LABEL_TEXT.toString());

        studentPane = new GridPane();

        studentTable = new TableView();
        studentTable.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        ObservableList<Student> studentData = data.getStudents();
        studentTable.setItems(studentData);
        studentFirstNameColumn = new TableColumn(firstName);
        studentLastNameColumn = new TableColumn(lastName);
        studentTeamColumn = new TableColumn(team);
        studentRoleColumn = new TableColumn(role);
        studentFirstNameColumn.setCellValueFactory(
                new PropertyValueFactory<Student, String>("firstName")
        );
        studentLastNameColumn.setCellValueFactory(
                new PropertyValueFactory<Student, String>("lastName")
        );
        studentTeamColumn.setCellValueFactory(
                new PropertyValueFactory<Student, String>("team")
        );
        studentRoleColumn.setCellValueFactory(
                new PropertyValueFactory<Student, String>("role")
        );
        studentTable.getColumns().addAll(studentFirstNameColumn, studentLastNameColumn, studentTeamColumn, studentRoleColumn);
        studentPane.add(studentsLabel, 0, 0);
        deleteStudentButton = new Button(deleteText);
        studentDeleteBox.getChildren().addAll(studentsLabel, deleteStudentButton);
        studentDeleteBox.setAlignment(Pos.CENTER_LEFT);
        studentBox.getChildren().add(studentDeleteBox);
        studentBox.getChildren().add(studentTable);

        Label firstNameLabel = new Label(firstName);
        Label lastNameLabel = new Label(lastName);
        Label teamLabel = new Label(team);
        Label roleLabel = new Label(role);
        Label addEditLabel1 = new Label(addEdit);
        studentPane.add(addEditLabel1, 0, 0);
        studentFirstNameTextField = new TextField();
        studentLastNameTextField = new TextField();
        studentTeamComboBox = new ComboBox();
        studentRoleComboBox = new ComboBox();
        studentRoleComboBox.getItems().addAll("Data Designer", "Lead Designer", "Project Manager", "Lead Programmer");
        addUpdateStudentButton = new Button(addUpdate);
        clearStudentButton = new Button(clearText);
        studentPane.addRow(1, firstNameLabel, studentFirstNameTextField);
        studentPane.addRow(2, lastNameLabel, studentLastNameTextField);
        studentPane.addRow(3, teamLabel, studentTeamComboBox);
        studentPane.addRow(4, roleLabel, studentRoleComboBox);
        studentPane.addRow(5, addUpdateStudentButton, clearStudentButton);
        studentBox.getChildren().add(studentPane);

        VBox vb = new VBox();
        vb.getChildren().addAll(teamBox, studentBox);

        projectDataPane = new BorderPane();
        projectDataPane.setTop(projectLabel);
        projectDataPane.setCenter(vb);
    }

    public void reloadProjectDataPane(CourseSiteData dataComponent) {

    }

    @Override
    public void resetWorkspace() {
        officeHoursGridPane.getChildren().clear();

        // AND THEN ALL THE GRID PANES AND LABELS
        officeHoursGridTimeHeaderPanes.clear();
        officeHoursGridTimeHeaderLabels.clear();
        officeHoursGridDayHeaderPanes.clear();
        officeHoursGridDayHeaderLabels.clear();
        officeHoursGridTimeCellPanes.clear();
        officeHoursGridTimeCellLabels.clear();
        officeHoursGridTACellPanes.clear();
        officeHoursGridTACellLabels.clear();
    }

    @Override
    public void reloadWorkspace(AppDataComponent dataComponent) {
        CourseSiteData data = (CourseSiteData) dataComponent;
        resetComboBoxes();
        resetAddBox();
        reloadOfficeHoursGrid(data);
        reloadCourseInfo(data);
        reloadRecitationPane(data);
        reloadScheduleDataPane(data);
        reloadProjectDataPane(data);
        resetAddStudent();
        resetAddTeam();
        resetAddSchedule();
        recitationSectionTextField.setText("");
        recitationInstructorTextField.setText("");
        recitationDayTimeTextField.setText("");
        recitationLocationTextField.setText("");
        bannerSchool.setImage(null);
        leftFooter.setImage(null);
        rightFooter.setImage(null);
    }

    public void reloadOfficeHoursGrid(CourseSiteData dataComponent) {

        ArrayList<String> gridHeaders = dataComponent.getGridHeaders();

        // ADD THE TIME HEADERS
        for (int i = 0; i < 2; i++) {
            addCellToGrid(dataComponent, officeHoursGridTimeHeaderPanes, officeHoursGridTimeHeaderLabels, i, 0);
            dataComponent.getCellTextProperty(i, 0).set(gridHeaders.get(i));
        }

        // THEN THE DAY OF WEEK HEADERS
        for (int i = 2; i < 7; i++) {
            addCellToGrid(dataComponent, officeHoursGridDayHeaderPanes, officeHoursGridDayHeaderLabels, i, 0);
            dataComponent.getCellTextProperty(i, 0).set(gridHeaders.get(i));
        }

        // THEN THE TIME AND TA CELLS
        int row = 1;
        for (int i = dataComponent.getStartHour(); i < dataComponent.getEndHour(); i++) {
            // START TIME COLUMN
            int col = 0;
            addCellToGrid(dataComponent, officeHoursGridTimeCellPanes, officeHoursGridTimeCellLabels, col, row);
            dataComponent.getCellTextProperty(col, row).set(buildCellText(i, "00"));
            addCellToGrid(dataComponent, officeHoursGridTimeCellPanes, officeHoursGridTimeCellLabels, col, row + 1);
            dataComponent.getCellTextProperty(col, row + 1).set(buildCellText(i, "30"));

            // END TIME COLUMN
            col++;
            int endHour = i;
            addCellToGrid(dataComponent, officeHoursGridTimeCellPanes, officeHoursGridTimeCellLabels, col, row);
            dataComponent.getCellTextProperty(col, row).set(buildCellText(endHour, "30"));
            addCellToGrid(dataComponent, officeHoursGridTimeCellPanes, officeHoursGridTimeCellLabels, col, row + 1);
            dataComponent.getCellTextProperty(col, row + 1).set(buildCellText(endHour + 1, "00"));
            col++;

            // AND NOW ALL THE TA TOGGLE CELLS
            while (col < 7) {
                addCellToGrid(dataComponent, officeHoursGridTACellPanes, officeHoursGridTACellLabels, col, row);
                addCellToGrid(dataComponent, officeHoursGridTACellPanes, officeHoursGridTACellLabels, col, row + 1);
                col++;
            }
            row += 2;
        }

        // CONTROLS FOR TOGGLING TA OFFICE HOURS
        for (Pane p : officeHoursGridTACellPanes.values()) {
            p.setFocusTraversable(true);
            p.setOnKeyPressed(e -> {
                controller.handleKeyPress(e.getCode());
            });
            p.setOnMouseClicked(e -> {
                controller.handleCellToggle((Pane) e.getSource());
            });
            p.setOnMouseExited(e -> {
                controller.handleGridCellMouseExited((Pane) e.getSource());
            });
            p.setOnMouseEntered(e -> {
                controller.handleGridCellMouseEntered((Pane) e.getSource());
            });
        }

        // AND MAKE SURE ALL THE COMPONENTS HAVE THE PROPER STYLE
        style taStyle = (style) app.getStyleComponent();
        taStyle.initOfficeHoursGridStyle();
    }

    public GeneratorApp getApp() {
        return app;
    }

    public GeneratorController getController() {
        return controller;
    }

    public GridPane getTeamPane() {
        return teamPane;
    }

    public GridPane getStudentPane() {
        return studentPane;
    }

    public VBox getTeamBox() {
        return teamBox;
    }

    public VBox getStudentBox() {
        return studentBox;
    }

    public HBox getTeamDeleteBox() {
        return teamDeleteBox;
    }

    public HBox getStudentDeleteBox() {
        return studentDeleteBox;
    }

    public GridPane getPageStylePane() {
        return pageStylePane;
    }

    public GridPane getSiteTemplatePane() {
        return siteTemplatePane;
    }

    public Button getChangeExportDirButton() {
        return changeExportDirButton;
    }

    public Label getBannerSchoolLabel() {
        return bannerSchoolLabel;
    }

    public ImageView getBannerSchool() {
        return bannerSchool;
    }

    public Button getAddRecitationButton() {
        return addRecitationButton;
    }

    public TabPane getTabPane() {
        return tabPane;
    }

    public Pane getCourseDetailPane() {
        return courseDetailPane;
    }

    public Pane getTADataPane() {
        return TADataPane;
    }

    public Pane getRecitationDataPane() {
        return recitationDataPane;
    }

    public Pane getScheduleDataPane() {
        return scheduleDataPane;
    }

    public Pane getProjectDataPane() {
        return projectDataPane;
    }

    public Tab getCourseDetail() {
        return courseDetail;
    }

    public Tab getTAData() {
        return TAData;
    }

    public Tab getRecitationData() {
        return recitationData;
    }

    public Tab getScheduleData() {
        return scheduleData;
    }

    public Tab getProjectData() {
        return projectData;
    }

    public HBox getTasHeaderBox() {
        return tasHeaderBox;
    }

    public Label getTasHeaderLabel() {
        return tasHeaderLabel;
    }

    public TableView<TeachingAssistant> getTaTable() {
        return taTable;
    }

    public TableColumn<TeachingAssistant, String> getNameColumn() {
        return nameColumn;
    }

    public TableColumn<TeachingAssistant, String> getEmailColumn() {
        return emailColumn;
    }

    public HBox getAddBox() {
        return addBox;
    }

    public TextField getNameTextField() {
        return nameTextField;
    }

    public Label getScheduleLabel() {
        return scheduleLabel;
    }

    public TextField getEmailTextField() {
        return emailTextField;
    }

    public Button getAddButton() {
        return addButton;
    }

    public Button getClearButton() {
        return clearButton;
    }

    public HBox getOfficeHoursSubheaderBox() {
        return officeHoursHeaderBox;
    }

    public Label getOfficeHoursSubheaderLabel() {
        return officeHoursHeaderLabel;
    }

    public ComboBox<String> getStartHourBox() {
        return startHourBox;
    }

    public ComboBox<String> getEndHourBox() {
        return endHourBox;
    }

    public HBox getOfficeHoursHeaderBox() {
        return officeHoursHeaderBox;
    }

    public Label getOfficeHoursHeaderLabel() {
        return officeHoursHeaderLabel;
    }

    public GridPane getOfficeHoursGridPane() {
        return officeHoursGridPane;
    }

    public HashMap<String, Pane> getOfficeHoursGridTimeHeaderPanes() {
        return officeHoursGridTimeHeaderPanes;
    }

    public HashMap<String, Label> getOfficeHoursGridTimeHeaderLabels() {
        return officeHoursGridTimeHeaderLabels;
    }

    public HashMap<String, Pane> getOfficeHoursGridDayHeaderPanes() {
        return officeHoursGridDayHeaderPanes;
    }

    public HashMap<String, Label> getOfficeHoursGridDayHeaderLabels() {
        return officeHoursGridDayHeaderLabels;
    }

    public HashMap<String, Pane> getOfficeHoursGridTimeCellPanes() {
        return officeHoursGridTimeCellPanes;
    }

    public HashMap<String, Label> getOfficeHoursGridTimeCellLabels() {
        return officeHoursGridTimeCellLabels;
    }

    public HashMap<String, Pane> getOfficeHoursGridTACellPanes() {
        return officeHoursGridTACellPanes;
    }

    public HashMap<String, Label> getOfficeHoursGridTACellLabels() {
        return officeHoursGridTACellLabels;
    }

    public GridPane getSitePagePane() {
        return sitePagePane;
    }

    public GridPane getCourseInfo() {
        return courseInfo;
    }

    public HBox getrB() {
        return rB;
    }

    public GridPane getAddRecitationPane() {
        return addRecitationPane;
    }

    public GridPane getRecitationGridPane() {
        return recitationGridPane;
    }

    public CheckBox getCheckHomeBox() {
        return checkHomeBox;
    }

    public CheckBox getCheckSyllabusBox() {
        return checkSyllabusBox;
    }

    public CheckBox getCheckScheduleBox() {
        return checkScheduleBox;
    }

    public CheckBox getCheckHWsBox() {
        return checkHWsBox;
    }

    public CheckBox getCheckProjectBox() {
        return checkProjectBox;
    }

    public Button getAddUpdateScheduleItemButton() {
        return addUpdateScheduleItemButton;
    }

    public Button getClearScheduleItemButton() {
        return clearScheduleItemButton;
    }

    public Button getAddUpdateTeamButton() {
        return addUpdateTeamButton;
    }

    public Button getAddUpdateStudentButton() {
        return addUpdateStudentButton;
    }

    public Button getClearStudentButton() {
        return clearStudentButton;
    }

    public Label getSubjectLabel() {
        return subjectLabel;
    }

    public TextField getSubjectBox() {
        return subjectBox;
    }

    public Label getNumberLabel() {
        return numberLabel;
    }

    public TextField getNumberBox() {
        return numberBox;
    }

    public Label getSemesterLabel() {
        return semesterLabel;
    }

    public TextField getSemesterBox() {
        return semesterBox;
    }

    public Label getYearLabel() {
        return yearLabel;
    }

    public TextField getYearBox() {
        return yearBox;
    }

    public Label getTitleLabel() {
        return titleLabel;
    }

    public TextField getTitleTextField() {
        return titleTextField;
    }

    public TextField getSubjectTextField() {
        return subjectTextField;
    }

    public TextField getNumberTextField() {
        return numberTextField;
    }

    public TextField getSemesterTextField() {
        return semesterTextField;
    }

    public TextField getYearTextField() {
        return yearTextField;
    }

    public Label getSiteTemplateLabel() {
        return siteTemplateLabel;
    }

    public Button getSelectTemplateDirButton() {
        return selectTemplateDirButton;
    }

    public Label getSitePagesLabel() {
        return sitePagesLabel;
    }

    public Label getUseLabel() {
        return useLabel;
    }

    public Label getNavbarTitleLabel() {
        return navbarTitleLabel;
    }

    public Label getScriptLabel() {
        return scriptLabel;
    }

    public String getTemplateDirectory() {
        return templateDirectory;
    }

    public GridPane getPageStyleGridPane() {
        return pageStyleGridPane;
    }

    public Label getRightFooterLabel() {
        return rightFooterLabel;
    }

    public Label getPageStyleLabel() {
        return pageStyleLabel;
    }

    public Label getStyleSheetLabel() {
        return styleSheetLabel;
    }

    public Label getStyleSheetNoteLabel() {
        return styleSheetNoteLabel;
    }

    public ComboBox getStyleSheetBox() {
        return styleSheetBox;
    }

    public ImageView getRightFooter() {
        return rightFooter;
    }

    public Label getRecitationLabel() {
        return recitationLabel;
    }

    public Button getRemoveRecitationButton() {
        return removeRecitationButton;
    }

    public Label getSectionLabel() {
        return sectionLabel;
    }

    public Label getLocationLabel() {
        return locationLabel;
    }

    public Label getTa1Label() {
        return ta1Label;
    }

    public Label getTa2Label() {
        return ta2Label;
    }

    public TextField getRecitationSectionTextField() {
        return recitationSectionTextField;
    }

    public TextField getRecitationInstructorTextField() {
        return recitationInstructorTextField;
    }

    public TextField getRecitationDayTimeTextField() {
        return recitationDayTimeTextField;
    }

    public TextField getRecitationLocationTextField() {
        return recitationLocationTextField;
    }

    public ComboBox getRecitationSupervisingTA1ComboBox() {
        return recitationSupervisingTA1ComboBox;
    }

    public ComboBox getRecitationSupervisingTA2ComboBox() {
        return recitationSupervisingTA2ComboBox;
    }

    public TableColumn<Recitation, String> getRecitationSectionColumn() {
        return recitationSectionColumn;
    }

    public TableColumn<Recitation, String> getRecitationInstructorColumn() {
        return recitationInstructorColumn;
    }

    public TableColumn<Recitation, String> getRecitationDayTimeColumn() {
        return recitationDayTimeColumn;
    }

    public TableColumn<Recitation, String> getRecitationLocationColumn() {
        return recitationLocationColumn;
    }

    public TableColumn<Recitation, String> getRecitationTA1Column() {
        return recitationTA1Column;
    }

    public TableColumn<Recitation, String> getRecitationTA2Column() {
        return recitationTA2Column;
    }

    public DatePicker getStartingMondayDatePicker() {
        return startingMondayDatePicker;
    }

    public TableView<ScheduleItem> getScheduleTable() {
        return scheduleTable;
    }

    public TableColumn<ScheduleItem, String> getScheduleTypeColumn() {
        return scheduleTypeColumn;
    }

    public TableColumn<ScheduleItem, String> getScheduleDateColumn() {
        return scheduleDateColumn;
    }

    public TableColumn<ScheduleItem, String> getScheduleTitleColumn() {
        return scheduleTitleColumn;
    }

    public TableColumn<ScheduleItem, String> getScheduleTopicColumn() {
        return scheduleTopicColumn;
    }

    public ComboBox getScheduleTypeBox() {
        return scheduleTypeBox;
    }

    public DatePicker getScheduleDatePicker() {
        return scheduleDatePicker;
    }

    public TextField getScheduleTimeTextField() {
        return scheduleTimeTextField;
    }

    public TextField getScheduleTitleTextField() {
        return scheduleTitleTextField;
    }

    public TextField getScheduleTopicTextField() {
        return scheduleTopicTextField;
    }

    public TextField getScheduleLinkTextField() {
        return scheduleLinkTextField;
    }

    public TextField getScheduleCriteriaTextField() {
        return scheduleCriteriaTextField;
    }

    public TableView<Team> getTeamTable() {
        return teamTable;
    }

    public TableColumn<Team, String> getTeamNameColumn() {
        return teamNameColumn;
    }

    public TableColumn<Team, String> getTeamColorColumn() {
        return teamColorColumn;
    }

    public TableColumn<Team, String> getTeamTextColorColumn() {
        return teamTextColorColumn;
    }

    public TableColumn<Team, String> getTeamLinkColumn() {
        return teamLinkColumn;
    }

    public TableView<Student> getStudentTable() {
        return studentTable;
    }

    public TableColumn<Student, String> getStudentFirstNameColumn() {
        return studentFirstNameColumn;
    }

    public TableColumn<Student, String> getStudentLastNameColumn() {
        return studentLastNameColumn;
    }

    public TableColumn<Student, String> getStudentTeamColumn() {
        return studentTeamColumn;
    }

    public TableColumn<Student, String> getStudentRoleColumn() {
        return studentRoleColumn;
    }

    public TextField getTeamNameTextField() {
        return teamNameTextField;
    }

    public ColorPicker getTeamColorPicker() {
        return teamColorPicker;
    }

    public ColorPicker getTeamTextColorPicker() {
        return teamTextColorPicker;
    }

    public TextField getTeamLinkTextField() {
        return teamLinkTextField;
    }

    public TextField getStudentFirstNameTextField() {
        return studentFirstNameTextField;
    }

    public TextField getStudentLastNameTextField() {
        return studentLastNameTextField;
    }

    public ComboBox getStudentTeamComboBox() {
        return studentTeamComboBox;
    }

    public ComboBox getStudentRoleComboBox() {
        return studentRoleComboBox;
    }

    public Pane getWorkspace() {
        return workspace;
    }

    public boolean isWorkspaceActivated() {
        return workspaceActivated;
    }

    public Label getInstructorNameLabel() {
        return instructorNameLabel;
    }

    public TextField getInstructorNameTextField() {
        return instructorNameTextField;
    }

    public Label getInstructorHomeLabel() {
        return instructorHomeLabel;
    }

    public TextField getInstructorHomeTextField() {
        return instructorHomeTextField;
    }

    public Label getExportDirLabel() {
        return exportDirLabel;
    }

    public Label getFileNameLabel() {
        return fileNameLabel;
    }

    public Label getLeftFooterLabel() {
        return leftFooterLabel;
    }

    public ImageView getLeftFooter() {
        return leftFooter;
    }

    public Label getInstructorLabel() {
        return instructorLabel;
    }

    public Button getDeleteScheduleItemButton() {
        return deleteScheduleItemButton;
    }

    public Button getClearTeamButton() {
        return clearTeamButton;
    }

    public Button getDeleteTeamButton() {
        return deleteTeamButton;
    }

    public Button getDeleteStudentButton() {
        return deleteStudentButton;
    }

    public String getCellKey(Pane testPane) {
        for (String key : officeHoursGridTACellLabels.keySet()) {
            if (officeHoursGridTACellPanes.get(key) == testPane) {
                return key;
            }
        }
        return null;
    }

    public Label getTACellLabel(String cellKey) {
        return officeHoursGridTACellLabels.get(cellKey);
    }

    public Pane getTACellPane(String cellPane) {
        return officeHoursGridTACellPanes.get(cellPane);
    }

    public String buildCellKey(int col, int row) {
        return "" + col + "_" + row;
    }

    public String buildCellText(int militaryHour, String minutes) {
        // FIRST THE START AND END CELLS
        int hour = militaryHour;
        if (hour > 12) {
            hour -= 12;
        }
        String cellText = "" + hour + ":" + minutes;
        if (militaryHour < 12) {
            cellText += "am";
        } else {
            cellText += "pm";
        }
        return cellText;
    }

    public void addCellToGrid(CourseSiteData dataComponent, HashMap<String, Pane> panes, HashMap<String, Label> labels, int col, int row) {
        // MAKE THE LABEL IN A PANE
        Label cellLabel = new Label("");
        HBox cellPane = new HBox();
        cellPane.setAlignment(Pos.CENTER);
        cellPane.getChildren().add(cellLabel);

        // BUILD A KEY TO EASILY UNIQUELY IDENTIFY THE CELL
        String cellKey = dataComponent.getCellKey(col, row);
        cellPane.setId(cellKey);
        cellLabel.setId(cellKey);

        // NOW PUT THE CELL IN THE WORKSPACE GRID
        officeHoursGridPane.add(cellPane, col, row);

        // AND ALSO KEEP IN IN CASE WE NEED TO STYLIZE IT
        panes.put(cellKey, cellPane);
        labels.put(cellKey, cellLabel);

        // AND FINALLY, GIVE THE TEXT PROPERTY TO THE DATA MANAGER
        // SO IT CAN MANAGE ALL CHANGES
        dataComponent.setCellProperty(col, row, cellLabel.textProperty());
    }

    public void resetAddBox() {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        String namePromptText = props.getProperty(GeneratorProp.NAME_PROMPT_TEXT.toString());
        String emailPromptText = props.getProperty(GeneratorProp.EMAIL_PROMPT_TEXT.toString());
        String addButtonText = props.getProperty(GeneratorProp.ADD_BUTTON_TEXT.toString());
        emailTextField.setText("");
        nameTextField.setText("");
        nameTextField.requestFocus();
        addButton.setText(addButtonText);
        underGradTABox.setSelected(false);
    }

    public void resetComboBoxes() {
        CourseSiteData data = (CourseSiteData) app.getDataComponent();
        startHourBox.setValue(String.valueOf(data.getStartHour()));
        endHourBox.setValue(String.valueOf(data.getEndHour()));
    }

    public void setExportDirectory(String dir) {
        this.exportDirectory = dir;
    }

    public String getExportDirectory() {
        return exportDirectory;
    }

    public void resetAddSchedule() {
        scheduleTypeBox.setValue("");
        scheduleDatePicker.setValue(LocalDate.now());
        scheduleTitleTextField.clear();
        scheduleTopicTextField.clear();
        scheduleTimeTextField.clear();
        scheduleLinkTextField.clear();
        scheduleCriteriaTextField.clear();
    }

    public void resetAddTeam() {
        teamNameTextField.clear();
        teamLinkTextField.clear();
        Color color = Color.WHITE;
        teamColorPicker.setValue(color);
        teamTextColorPicker.setValue(color);
        teamTable.getSelectionModel().select(null);
    }

    public void resetTeamComboBox() {
        CourseSiteData data = (CourseSiteData) app.getDataComponent();
        studentTeamComboBox.getItems().clear();
        ObservableList<Team> teams = data.getTeams();
        String[] teamName = new String[teams.size()];
        for (int i = 0; i < teams.size(); i++) {
            teamName[i] = teams.get(i).getName();
        }

        studentTeamComboBox.getItems().addAll(teamName);
    }

    public void resetAddStudent() {
        studentFirstNameTextField.clear();
        studentLastNameTextField.clear();
        studentRoleComboBox.setValue("");
        studentTeamComboBox.setValue("");
        studentTable.getSelectionModel().select(null);
    }

    public CheckBox getUnderGradTABox() {
        return underGradTABox;
    }

    public Image getBannerSchoolImage() {
        return bannerSchoolImage;
    }

    public void setBannerSchoolImage(Image bannerSchoolImage) {
        this.bannerSchoolImage = bannerSchoolImage;
    }

    public Image getLeftFooterImage() {
        return leftFooterImage;
    }

    public void setLeftFooterImage(Image leftFooterImage) {
        this.leftFooterImage = leftFooterImage;
    }

    public Image getRightFooterImage() {
        return rightFooterImage;
        
    }

    public void setRightFooterImage(Image rightFooterImage) {
        this.rightFooterImage = rightFooterImage;
    }

    public void resetExportDir() {
        exportDir.setText(exportDirectory);

    }
    
    public void resetTemplateDir(){
        diL.setText(templateDirectory);
    }

    public void setTemplateDirectory(String templateDirectory) {
        this.templateDirectory = templateDirectory;
    }

    public void loadStyleSheet() {
        File folder = new File(PATH_CSS);
        File[] listOfFiles = folder.listFiles();
        String[] results = new String[listOfFiles.length];
        int i = 0;
        for (File file : listOfFiles) {
            if (file.isFile()) {
                results[i] = file.getName();
                i++;
            }
        }
        styleSheetBox.getItems().addAll(results);
    }
    
    
}

