/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coursesitegenerator.jTPS;

import coursesitegenerator.data.CourseSiteData;
import coursesitegenerator.data.Student;
import coursesitegenerator.data.Team;
import coursesitegenerator.workspace.GeneratorWorkspace;
import javafx.collections.ObservableList;

/**
 *
 * @author xinyu
 */
public class Delete_Team_Transaction implements jTPS_Transaction {

    ObservableList<Student> students;
    Team team;
    CourseSiteData data;
    GeneratorWorkspace workspace;

    public Delete_Team_Transaction(ObservableList<Student> students, Team team, CourseSiteData data, GeneratorWorkspace workspace) {
        this.students = students;
        this.team = team;
        this.data = data;
        this.workspace = workspace;
    }

    @Override
    public void doTransaction() {
        data.removeTeam(team.getName());
        for (Student stu : students) {
            data.removeStudent(stu.getLastName(), stu.getFirstName());
        }
        workspace.resetTeamComboBox();
    }

    @Override
    public void undoTransaction() {
        data.addTeam(team.getName(), team.getColor(), team.getTextColor(), team.getLink());
        for (Student stu : students) {
            data.addStudent(stu.getLastName(), stu.getFirstName(), stu.getTeam(), stu.getRole());
        }
        workspace.resetTeamComboBox();
    }

}
