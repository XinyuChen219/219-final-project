/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coursesitegenerator.jTPS;

import coursesitegenerator.data.CourseSiteData;
import coursesitegenerator.workspace.GeneratorWorkspace;

/**
 *
 * @author xinyu
 */
public class Add_TA_Transaction implements jTPS_Transaction{
    private String taEmail;
    private String taName;
    private Boolean underGrad;
    private CourseSiteData data;
    private GeneratorWorkspace workspace;
    
    public Add_TA_Transaction(){}
    public Add_TA_Transaction(String name, String email, Boolean underGrad, CourseSiteData data, GeneratorWorkspace workspace){
        taEmail = email;
        taName = name;
        this.underGrad = underGrad;
        this.data = data;
        this.workspace = workspace;
    }

    @Override
    public void doTransaction() {
        data.addTA(taName, taEmail, underGrad);
        workspace.reloadRecitationPane(data);
    }

    @Override
    public void undoTransaction() {
        data.removeTA(taName);
        workspace.reloadRecitationPane(data);
    }
    
}
