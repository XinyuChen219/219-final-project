/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coursesitegenerator.jTPS;

import coursesitegenerator.workspace.GeneratorController;

/**
 *
 * @author xinyu
 */
public class Change_Office_End_Hour_Transaction implements jTPS_Transaction{
    String newTime;
    String oldTime;
    GeneratorController controller;

    public Change_Office_End_Hour_Transaction(String newTime, String oldTime, GeneratorController controller) {
        this.newTime = newTime;
        this.oldTime = oldTime;
        this.controller = controller;
    }
    
    
    @Override
    public void doTransaction() {
        controller.handleChangedEndHour(oldTime, newTime);
    }

    @Override
    public void undoTransaction() {
        controller.handleChangedEndHour(newTime, oldTime);
    }
    
}
