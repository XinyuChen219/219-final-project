
package coursesitegenerator.jTPS;

import coursesitegenerator.data.CourseSiteData;

/**
 *
 * @author xinyu
 */
public class Add_Recitation_Transaction implements jTPS_Transaction{
    String section;
    String instructor;
    String dayTime;
    String location;
    String ta1;
    String ta2;
    CourseSiteData data;

    public Add_Recitation_Transaction(String section, String instructor, String dayTime, String location, String ta1, String ta2, CourseSiteData data) {
        this.section = section;
        this.instructor = instructor;
        this.dayTime = dayTime;
        this.location = location;
        this.ta1 = ta1;
        this.ta2 = ta2;
        this.data = data;
    }
    
    
    @Override
    public void doTransaction() {
        data.addRecitation(section, instructor, dayTime, location, ta1, ta2);
    }

    @Override
    public void undoTransaction() {
       data.removeRecitation(section);
    }
    
}