/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coursesitegenerator.jTPS;

import coursesitegenerator.data.CourseSiteData;
import coursesitegenerator.data.Recitation;

/**
 *
 * @author xinyu
 */
public class Delete_Recitation_Transaction implements jTPS_Transaction{
    Recitation res;
    CourseSiteData data;

    public Delete_Recitation_Transaction(Recitation res, CourseSiteData data) {
        this.res = res;
        this.data = data;
    }
    
    
    @Override
    public void doTransaction() {
        data.removeRecitation(res.getSection());
    }

    @Override
    public void undoTransaction() {
        data.addRecitation(res.getSection(), res.getInstructor(), res.getDayTime(), res.getLocation(), res.getTa1(), res.getTa2());
    }
    
}
