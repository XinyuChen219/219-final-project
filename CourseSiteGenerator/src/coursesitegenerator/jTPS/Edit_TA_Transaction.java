/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coursesitegenerator.jTPS;

import coursesitegenerator.data.CourseSiteData;
import coursesitegenerator.data.Recitation;
import coursesitegenerator.data.TeachingAssistant;
import coursesitegenerator.workspace.GeneratorWorkspace;
import java.util.HashMap;
import javafx.collections.ObservableList;
import javafx.scene.control.Label;

/**
 *
 * @author xinyu
 */
public class Edit_TA_Transaction implements jTPS_Transaction {

    TeachingAssistant oldTA;
    TeachingAssistant newTA;
    CourseSiteData data;
    GeneratorWorkspace workspace;
    ObservableList<Recitation> editedRecitations;

    public Edit_TA_Transaction(TeachingAssistant oldTA, TeachingAssistant newTA, CourseSiteData data, GeneratorWorkspace workspace, ObservableList<Recitation> editedRecitations) {
        this.oldTA = oldTA;
        this.newTA = newTA;
        this.data = data;
        this.workspace = workspace;
        this.editedRecitations = editedRecitations;
    }

    @Override
    public void doTransaction() {
        data.removeTA(oldTA.getName());
        data.addTA(newTA.getName(), newTA.getEmail(), newTA.getUnderGrad());
        if (!oldTA.getName().equals(newTA.getName())) {
            HashMap<String, Label> labels = workspace.getOfficeHoursGridTACellLabels();
            for (Label label : labels.values()) {
                if (label.getText().equals(oldTA.getName())
                        || (label.getText().contains(oldTA.getName() + "\n"))
                        || (label.getText().contains("\n" + oldTA.getName()))) {
                    data.editTAFromCell(label.textProperty(), oldTA.getName(), newTA.getName());
                }
            }
        }
        ObservableList<Recitation> recitations = data.getRecitations();
        for (int i = 0; i < recitations.size(); i++) {
            if (recitations.get(i).getTa1().equals(oldTA.getName())) {
                recitations.get(i).setTA1(newTA.getName());
            } else if (recitations.get(i).getTa2().equals(oldTA.getName())) {
                recitations.get(i).setTA2(newTA.getName());
            }
        }
        workspace.reloadRecitationPane(data);
    }

    @Override
    public void undoTransaction() {
        data.removeTA(newTA.getName());
        data.addTA(oldTA.getName(), oldTA.getEmail(), oldTA.getUnderGrad());
        if (!oldTA.getName().equals(newTA.getName())) {
            HashMap<String, Label> labels = workspace.getOfficeHoursGridTACellLabels();
            for (Label label : labels.values()) {
                if (label.getText().equals(newTA.getName())
                        || (label.getText().contains(newTA.getName() + "\n"))
                        || (label.getText().contains("\n" + newTA.getName()))) {
                    data.editTAFromCell(label.textProperty(), newTA.getName(), oldTA.getName());
                }
            }
        }
        
        for(Recitation res : editedRecitations){
            if(res.getTa1().equals(newTA.getName()))
                res.setTA1(oldTA.getName());
            else
                res.setTA2(oldTA.getName());
        }
        workspace.getRecitationTable().refresh();
        workspace.reloadRecitationPane(data);
    }

}
