/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coursesitegenerator.jTPS;

import coursesitegenerator.data.CourseSiteData;
import coursesitegenerator.data.Team;
import coursesitegenerator.workspace.GeneratorController;
import coursesitegenerator.workspace.GeneratorWorkspace;

/**
 *
 * @author xinyu
 */
public class Edit_Team_Transaction implements jTPS_Transaction{
    String name;
    String color;
    String textColor;
    String link;
    Team oldTeam;
    CourseSiteData data;
    GeneratorWorkspace workspace;
    GeneratorController controller;

    public Edit_Team_Transaction(String name, String color, String textColor, String link, Team oldTeam, CourseSiteData data, GeneratorWorkspace workspace, GeneratorController controller) {
        this.name = name;
        this.color = color;
        this.textColor = textColor;
        this.link = link;
        this.oldTeam = oldTeam;
        this.data = data;
        this.workspace = workspace;
        this.controller = controller;
    }
    
    @Override
    public void doTransaction() {
                data.removeTeam(oldTeam.getName());
        data.addTeam(name, color, textColor, link);

        controller.changeTeamName( oldTeam.getName(), name);
        workspace.resetTeamComboBox();
    }

    @Override
    public void undoTransaction() {
        data.removeTeam(name);
        data.addTeam(oldTeam.getName(), oldTeam.getColor(), oldTeam.getTextColor(), oldTeam.getLink());
        
        controller.changeTeamName(name, oldTeam.getName());
        workspace.resetTeamComboBox();
    }
    
}
