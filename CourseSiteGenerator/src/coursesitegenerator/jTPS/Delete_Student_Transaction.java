/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coursesitegenerator.jTPS;

import coursesitegenerator.data.CourseSiteData;

/**
 *
 * @author xinyu
 */
public class Delete_Student_Transaction implements jTPS_Transaction{
    String lastName;
    String firstName;
    String role;
    String team;
    CourseSiteData data;

    public Delete_Student_Transaction(String lastName, String firstName, String role, String team, CourseSiteData data) {
        this.lastName = lastName;
        this.firstName = firstName;
        this.role = role;
        this.team = team;
        this.data = data;
    }
    
    
    @Override
    public void doTransaction() {
        data.removeStudent(lastName, firstName);
    }

    @Override
    public void undoTransaction() {
        data.addStudent(lastName, firstName, team, role);
    }
    
}
