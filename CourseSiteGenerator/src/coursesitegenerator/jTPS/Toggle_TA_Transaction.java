/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coursesitegenerator.jTPS;

import coursesitegenerator.data.CourseSiteData;
import javafx.scene.layout.Pane;

/**
 *
 * @author xinyu
 */
public class Toggle_TA_Transaction implements jTPS_Transaction{
    String name;
    CourseSiteData data;
    Pane pane;
    Boolean exist;
    
    public Toggle_TA_Transaction(){}
    
    public Toggle_TA_Transaction(String name, Pane pane, CourseSiteData data, Boolean exist){
        this.name = name;
        this.data = data;
        this.pane = pane;
        this.exist = exist;
    }
    
    @Override
    public void doTransaction() {
        String cellKey = pane.getId();
        data.toggleTAOfficeHours(cellKey, name);
    }

    @Override
    public void undoTransaction() {
        String cellKey = pane.getId();
        data.toggleTAOfficeHours(cellKey, name);
    }
    
}
