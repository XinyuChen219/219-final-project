/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coursesitegenerator.jTPS;

import coursesitegenerator.data.CourseSiteData;
import coursesitegenerator.data.Recitation;

/**
 *
 * @author xinyu
 */
public class Edit_Recitation_Transaction implements jTPS_Transaction{
    Recitation oldR;
    Recitation newR;
    CourseSiteData data;

    public Edit_Recitation_Transaction(Recitation oldR, Recitation newR, CourseSiteData data) {
        this.oldR = oldR;
        this.newR = newR;
        this.data = data;
    }
    
    
    
    @Override
    public void doTransaction() {
        data.removeRecitation(oldR.getSection());
        data.addRecitation(newR.getSection(), newR.getInstructor(), newR.getDayTime(), newR.getLocation(), newR.getTa1(), newR.getTa2());
    }

    @Override
    public void undoTransaction() {
        data.removeRecitation(newR.getSection());
        data.addRecitation(oldR.getSection(), oldR.getInstructor(), oldR.getDayTime(), oldR.getLocation(), oldR.getTa1(), oldR.getTa2());
    
    }
    
}
