/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coursesitegenerator.jTPS;

import coursesitegenerator.data.CourseSiteData;
import coursesitegenerator.workspace.GeneratorController;
import java.util.HashMap;
import javafx.beans.property.StringProperty;

/**
 *
 * @author xinyu
 */
public class Change_Office_Start_Hour_Transaction implements jTPS_Transaction{
    String oldTime;
    String newTime;
    GeneratorController controller;

    public Change_Office_Start_Hour_Transaction(String oldTime, String newTime, GeneratorController controller) {
        this.oldTime = oldTime;
        this.newTime = newTime;
        this.controller = controller;
    }
    
    
    
    public Change_Office_Start_Hour_Transaction() {
    }

    @Override
    public void doTransaction() {
        controller.handleChangedStartHour(oldTime, newTime);
    }

    @Override
    public void undoTransaction() {
        controller.handleChangedStartHour(newTime, oldTime);
    }
    
}
