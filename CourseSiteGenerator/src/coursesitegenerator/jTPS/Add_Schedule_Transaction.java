/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coursesitegenerator.jTPS;

import coursesitegenerator.data.CourseSiteData;
import coursesitegenerator.data.HWs;
import coursesitegenerator.data.Holiday;
import coursesitegenerator.data.Lecture;
import coursesitegenerator.data.RecitationsSchedule;
import coursesitegenerator.data.Reference;
import coursesitegenerator.data.ScheduleItem;

/**
 *
 * @author xinyu
 */
public class Add_Schedule_Transaction implements jTPS_Transaction{
    ScheduleItem schedule;
    CourseSiteData data;

    public Add_Schedule_Transaction(ScheduleItem schedule, CourseSiteData data) {
        this.schedule = schedule;
        this.data = data;
    }
    
    
    
    @Override
    public void doTransaction() {
        if(schedule.getType().equals("Recitation")){
            RecitationsSchedule re = (RecitationsSchedule) schedule;
            data.addRecitationSchedule(re.getDate(), re.getTitle(), re.getTopic());
        }
        else if(schedule.getType().equals("Holiday")){
            Holiday ho = (Holiday) schedule;
            data.addHoliday(ho.getDate(), ho.getTitle(), ho.getLink());
        }
        else if(schedule.getType().equals("Lecture")){
            Lecture le = (Lecture) schedule;
            data.addLecture(le.getDate(), le.getTitle(), le.getTopic(), le.getLink());
        }
        else if(schedule.getType().equals("Reference")){
            Reference ref = (Reference) schedule;
            data.addReference(ref.getDate(), ref.getTitle(), ref.getTopic(), ref.getLink());
        }
        else{
            HWs hw = (HWs) schedule;
            data.addHW(hw.getDate(), hw.getTitle(), hw.getTopic(), hw.getLink(), hw.getTime(), hw.getCriteria());
        }
            
    }

    @Override
    public void undoTransaction() {
        data.removeScheduleItem(schedule.getTitle(), schedule.getDate());
    }
    
}
