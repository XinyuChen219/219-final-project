/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coursesitegenerator.jTPS;

import coursesitegenerator.data.CourseSiteData;
import coursesitegenerator.data.Holiday;
import coursesitegenerator.data.ScheduleItem;
import java.util.logging.Logger;

/**
 *
 * @author xinyu
 */
public class Edit_Schedule_Transaction implements jTPS_Transaction{
    ScheduleItem oldS;
    ScheduleItem newS;
    CourseSiteData data;

    public Edit_Schedule_Transaction(ScheduleItem oldS, ScheduleItem newS, CourseSiteData data) {
        this.oldS = oldS;
        this.newS = newS;
        this.data = data;
    }

    
    
    @Override
    public void doTransaction() {
        data.removeScheduleItem(oldS.getTitle(), oldS.getDate());
        data.addScheduleItem(newS);
    }

    @Override
    public void undoTransaction() {
        data.removeScheduleItem(newS.getTitle(), newS.getDate());
        data.addScheduleItem(oldS);
    }
    
}
