/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coursesitegenerator.jTPS;

import coursesitegenerator.data.CourseSiteData;
import coursesitegenerator.workspace.GeneratorWorkspace;

/**
 *
 * @author xinyu
 */
public class Add_Team_Transaction implements jTPS_Transaction{

    private String name;
    private String link;
    private String color;
    private String textColor;
    CourseSiteData data;
    GeneratorWorkspace workspace;

    public Add_Team_Transaction(String name, String link, String color, String textColor,CourseSiteData data,GeneratorWorkspace workspace) {
        this.name = name;
        this.link = link;
        this.color = color;
        this.textColor = textColor;
        this.data = data;
        this.workspace = workspace;
    }
    
    @Override
    public void doTransaction() {
        data.addTeam(name, color, textColor, link);
        workspace.resetTeamComboBox();
    }

    @Override
    public void undoTransaction() {
        data.removeTeam(name);
        workspace.resetTeamComboBox();
    }
    
}
