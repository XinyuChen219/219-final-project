/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coursesitegenerator.jTPS;

import coursesitegenerator.data.CourseSiteData;
import coursesitegenerator.data.Student;

/**
 *
 * @author xinyu
 */
public class Edit_Student_Transaction implements jTPS_Transaction {

    Student oldStudent;
    String lastName;
    String firstName;
    String role;
    String team;
    CourseSiteData data;

    public Edit_Student_Transaction(Student oldStudent, String lastName, String firstName, String role, String team, CourseSiteData data) {
        this.oldStudent = oldStudent;
        this.lastName = lastName;
        this.firstName = firstName;
        this.role = role;
        this.team = team;
        this.data = data;
    }

    @Override
    public void doTransaction() {
        data.removeStudent(oldStudent.getLastName(), oldStudent.getFirstName());

        data.addStudent(lastName, firstName, team, role);
    }

    @Override
    public void undoTransaction() {
        data.removeStudent(lastName, firstName);
        data.addStudent(oldStudent.getLastName(), oldStudent.getFirstName(), oldStudent.getTeam(), oldStudent.getRole());
        
    }

}
