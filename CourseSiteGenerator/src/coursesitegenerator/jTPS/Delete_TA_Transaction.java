/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coursesitegenerator.jTPS;

import coursesitegenerator.data.CourseSiteData;
import coursesitegenerator.data.Recitation;
import coursesitegenerator.workspace.GeneratorWorkspace;
import java.util.ArrayList;
import java.util.HashMap;
import javafx.beans.property.StringProperty;
import javafx.collections.ObservableList;

/**
 *
 * @author xinyu
 */
public class Delete_TA_Transaction implements jTPS_Transaction {

    private String email;
    private String name;
    private Boolean under;
    private CourseSiteData data;
    private GeneratorWorkspace workspace;
    private ArrayList<String> keyArray;
    HashMap<String, StringProperty> officeHours;
    ObservableList<Recitation> recitations;

    public Delete_TA_Transaction() {
    }

    public Delete_TA_Transaction(String name, String email, Boolean under, HashMap<String, StringProperty> officeHours, CourseSiteData data, GeneratorWorkspace workspace, ObservableList<Recitation> recitations) {
        this.email = email;
        this.name = name;
        this.under = under;
        this.data = data;
        this.workspace = workspace;
        this.officeHours = officeHours;
        keyArray = new ArrayList<String>();
        this.recitations = recitations;
    }

    @Override
    public void doTransaction() {
        keyArray.clear(); //clear anystored values 
        data.removeTA(name);
        for (HashMap.Entry<String, StringProperty> entry : data.getOfficeHours().entrySet()) {
            String key = entry.getKey();
            StringProperty prop = data.getOfficeHours().get(key);
            if (prop.getValue().equals(name)
                    || (prop.getValue().contains(name + "\n"))
                    || (prop.getValue().contains("\n" + name))) {
                System.out.println(prop.getValue());
                keyArray.add(key);
                data.removeTAFromCell(prop, name);
            }

        }
        ObservableList<Recitation> recitat = data.getRecitations();
        for (int i = 0; i < recitat.size(); i++) {
            if (recitat.get(i).getTa1().equals(name)) {
                recitat.get(i).setTA1("none");
            } else if (recitat.get(i).getTa2().equals(name)) {
                recitat.get(i).setTA2("none");
            }
        }
        workspace.reloadRecitationPane(data);
    }

    @Override
    public void undoTransaction() {
        data.addTA(name, email, under);

        for (String key : keyArray) {
            StringProperty prop = officeHours.get(key);
            String cellText = prop.getValue();
            prop.setValue(cellText + "\n" + name);
        }
        
        for(Recitation res : recitations){
            if(res.getTa1() == "none")
                res.setTA1(name);
            else
                res.setTA2(name);
        }
        workspace.getRecitationTable().refresh();
        workspace.reloadRecitationPane(data);
    }

}
