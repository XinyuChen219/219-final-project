/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coursesitegenerator;

import coursesitegenerator.data.CourseSiteData;
import coursesitegenerator.data.TAData;
import coursesitegenerator.file.CourseSiteFile;
import coursesitegenerator.style.style;
import coursesitegenerator.workspace.GeneratorWorkspace;
import djf.AppTemplate;
import java.util.Locale;
import static javafx.application.Application.launch;

/**
 *
 * @author xinyu
 */
public class GeneratorApp extends AppTemplate{

    @Override
    public void buildAppComponentsHook() {
         dataComponent = new CourseSiteData(this);
        workspaceComponent = new GeneratorWorkspace(this);
        fileComponent = new CourseSiteFile(this);
        styleComponent = new style(this);
    }
    
    public static void main(String[] args) {
	Locale.setDefault(Locale.US);
	launch(args);
    }
}
