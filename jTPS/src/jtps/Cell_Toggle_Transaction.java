/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jtps;

import tam.workspace.TAController;

/**
 *
 * @author xinyu
 */
public class Cell_Toggle_Transaction implements jTPS_Transaction{

    String cellKey;
    String taName;
    TAController controller;
    boolean exist;
    
    public Cell_Toggle_Transaction(){}
    
    public Cell_Toggle_Transaction(String cellKey, String taName, boolean exist, TAController controller){
        this.cellKey = cellKey;
        this.taName = taName;
        this.exist = exist;
        this.controller = controller;
    }
    
    @Override
    public void doTransaction() {
        controller.reToggle(cellKey, taName, exist);
    }

    @Override
    public void undoTransaction() {
        controller.unToggle(cellKey, taName, exist);
    }
    
}
