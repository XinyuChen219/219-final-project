/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jtps;

import java.util.ArrayList;
import tam.file.TimeSlot;
import tam.workspace.TAController;

/**
 *
 * @author xinyu
 */
public class End_Hour_Transaction implements jTPS_Transaction{
    String soldTime, snewTime;
    ArrayList<TimeSlot> offHours;
    TAController controller;
    public End_Hour_Transaction(){}
    
    public End_Hour_Transaction(String soldTime, String snewTime, ArrayList<TimeSlot> offHours, TAController controller){
        this.soldTime = soldTime;
        this.snewTime = snewTime;
        this.offHours = offHours;
        this.controller = controller;
    }
    @Override
    public void doTransaction() {
        controller.reChangeEndHour(soldTime, snewTime, offHours);
    }

    @Override
    public void undoTransaction() {
        controller.unChangeEndHour(soldTime, snewTime, offHours);
    }
    
}
