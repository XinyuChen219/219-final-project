/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jtps;

import javafx.scene.control.TextField;
import coursesitegenerator.GeneratorApp;
import tam.workspace.TAController;
import tam.workspace.TAWorkspace;

/**
 *
 * @author xinyu
 */
public class Add_TA_Transaction implements jTPS_Transaction {

    String name;
    String email;
    TAController controller;

    public Add_TA_Transaction() {
    }

    public Add_TA_Transaction(String name, String email, TAController controller) {
        this.name = name;
        this.email = email;
        this.controller = controller;
    }

    @Override
    public void doTransaction() {
        controller.handleReAddTA(name, email);
    }

    @Override
    public void undoTransaction() {
        controller.handlerDeleteTheAddedTA(name);
    }

}
