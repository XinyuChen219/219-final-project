/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jtps;

import java.util.HashMap;
import javafx.beans.property.StringProperty;
import javafx.scene.control.Label;
import tam.data.TAData;
import tam.workspace.TAController;

/**
 *
 * @author xinyu
 */
public class Delete_TA_Transaction implements jTPS_Transaction{
    String TAname;
    TAData data;
    String email;
    TAController controller;
    String[][] infor;
    HashMap<String, Label> labels;
    int[][] cont;
    public Delete_TA_Transaction(){}
    
    public Delete_TA_Transaction(String TAname, String email, String[][] infor, TAData data, TAController controller, HashMap<String, Label> labels, int[][] cont){
        this.TAname = TAname;
        this.data = data;
        this.email = email;
        this.infor = infor;
        this.controller = controller;
        this.labels = labels;
        this.cont = cont;
    }

    @Override
    public void doTransaction() {
        controller.redoDeleteTA(TAname);
    }

    @Override
    public void undoTransaction() {
        controller.undoDeleteTA(TAname, email, infor, labels, cont);
    }
    
}
