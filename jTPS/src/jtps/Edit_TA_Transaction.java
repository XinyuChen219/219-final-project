/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jtps;

import tam.workspace.TAController;

/**
 *
 * @author xinyu
 */
public class Edit_TA_Transaction implements jTPS_Transaction{
    String oldName, newName, oldEmail, newEmail;
    TAController controller;
    public Edit_TA_Transaction(){}
    
    public Edit_TA_Transaction(String oldName, String newName, String oldEmail, String newEmail, TAController controller){
        this.oldName = oldName;
        this.newName = newName;
        this.oldEmail = oldEmail;
        this.newEmail = newEmail;
        this.controller = controller;
    }
    @Override
    public void doTransaction() {
        controller.reEditTA(oldName, newName, oldEmail, newEmail);
    }

    @Override
    public void undoTransaction() {
        controller.unEditTA(oldName, newName, oldEmail, newEmail);
    }
    
}
