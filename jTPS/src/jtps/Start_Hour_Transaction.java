/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jtps;

import java.util.ArrayList;
import tam.file.TimeSlot;
import tam.workspace.TAController;

/**
 *
 * @author xinyu
 */
public class Start_Hour_Transaction implements jTPS_Transaction{
    String soldTime, snewTime;
    ArrayList<TimeSlot> offHours;
    TAController controller;
    int occ;
    public Start_Hour_Transaction(){}
    
    public Start_Hour_Transaction(String soldTime, String snewTime, ArrayList<TimeSlot> offHours, TAController controller){
        occ = 0;
        this.soldTime = soldTime;
        this.snewTime = snewTime;
        this.offHours = offHours;
        this.controller = controller;
    }
    @Override
    public void doTransaction() {
        if(occ != 0){
            controller.reChangeStartTime(soldTime, snewTime);
        }
        occ++;
    }

    @Override
    public void undoTransaction() {
        controller.unChangeStartTime(soldTime, snewTime, offHours);
    }
    
}
